const Web3 = require('web3')
const web3 = window.web3 ? new Web3(window.web3.currentProvider) : new Web3()

function isEqualAddresses(a, b) {
  return web3.utils.toChecksumAddress(a) === web3.utils.toChecksumAddress(b)
}

function verifyOwner(walletAddress, message, signature) {
  try {
    const signedAddress = web3.eth.accounts.recover(message, signature)
    return isEqualAddresses(signedAddress, walletAddress)
  } catch (e) {
    return false
  }
}

function appendPrefix(input) {
  return `0x${input}`
}

module.exports = {
  web3,
  verifyOwner,
  toChecksumAddress: web3.utils.toChecksumAddress,
  isAddress: web3.utils.isAddress,
  isEqualAddresses,
  appendPrefix,
  ZERO_ADDRESS: '0x0000000000000000000000000000000000000000'
}
