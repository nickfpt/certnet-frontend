import React from 'react'
import PropTypes from 'prop-types'

const Icon = props =>
  // eslint-disable-next-line jsx-a11y/no-static-element-interactions
  (<i
    className={props.name} style={{
      color: props.color,
      fontSize: props.iconSize,
      cursor: props.cursor,
      marginLeft: props.marginLeft,
      marginRight: props.marginRight,
      marginTop: props.marginTop,
      marginBottom: props.marginBottom,
    }}
    onClick={props.onClick}
    id={props.id}
  />)

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string,
  iconSize: PropTypes.string,
  marginLeft: PropTypes.string,
  marginRight: PropTypes.string,
  marginTop: PropTypes.string,
  marginBottom: PropTypes.string,
  onClick: PropTypes.func,
  cursor: PropTypes.string,
  id: PropTypes.string,
}

Icon.defaultProps = {
  color: '',
  iconSize: '1em',
  marginLeft: '0',
  marginRight: '0',
  marginTop: '0',
  marginBottom: '0',
}

export default Icon
