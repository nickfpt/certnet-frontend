import React, { Component } from 'react'
import AuthenticationModal from './../Authentication'
import Cookies from 'universal-cookie'
import { ROLES } from './../../constants/auth'
import { logout } from './../../actions/AuthenAction'
import { connect } from 'react-redux'

import {
  Collapse, Navbar, NavbarToggler,
  Nav, NavItem, UncontrolledDropdown, Label,
  DropdownToggle, DropdownMenu, DropdownItem,
} from 'reactstrap'
import { NavLink } from 'react-router-dom'
import jwt from 'jsonwebtoken'

const cookies = new Cookies()
class Header extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      isOpenLoginForm: false,
    }
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }
  logout = async () => {
    cookies.remove('token', {
      path: '/'
    })
    await this.props.handleLogout()
    this.props.updateToken()
  }

  openLoginForm = () => {
    this.setState({
      isOpenLoginForm: true,
    })
  }
  closeLoginForm = () => {
    this.setState({
      isOpenLoginForm: false,
    })
    this.props.updateToken()
  }

  render() {
    const token = this.props.tokenData
    let decodedToken = null
    try {
      decodedToken = jwt.verify(token.token, process.env.JWT_SECRET)
    } catch (ex) {
      decodedToken = null
    }
    const { pathname } = this.props
    return (
      <div>
        <Navbar dark expand="md" className="header">
          <NavLink activeClassName="" className="navBrand" exact to="/home" >
            <img src="https://storage.googleapis.com/certnet/img/logo.png" height="30" alt="CERTNET">
            </img>
          </NavLink>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav navbar>
              <NavItem className="header-nav">
                <NavLink className={pathname.startsWith('/quickShare') ? 'headerMenu-active' : 'headerMenu'} to="/quickShare" >QuickShare</NavLink>
              </NavItem>
              <NavItem className="header-nav">
                <NavLink className={pathname.startsWith('/partners') ? 'headerMenu-active' : 'headerMenu'} to="/partners">Partners</NavLink>
              </NavItem>
              {
                decodedToken && decodedToken.roles.includes(ROLES.ADMIN) &&
                <NavItem className="header-nav">
                  <NavLink className={pathname.startsWith('/admin') ? 'headerMenu-active' : 'headerMenu'} to="/admin">Admin</NavLink>
                </NavItem>
              }
              {
                decodedToken && decodedToken.roles.includes(ROLES.ISSUER) &&
                <NavItem className="header-nav">
                  <NavLink className={pathname.startsWith('/issuer') ? 'headerMenu-active' : 'headerMenu'} to="/issuer">Issuer</NavLink>
                </NavItem>
              }
              {
                decodedToken && decodedToken.roles.includes(ROLES.RECIPIENT) &&
                <NavItem className="header-nav">
                  <NavLink className={pathname.startsWith('/recipient') ? 'headerMenu-active' : 'headerMenu'} to="/recipient">Recipient</NavLink>
                </NavItem>
              }
            </Nav>

            {!decodedToken ?
              (
                <Nav className="ml-auto" navbar>
                  <Label className="headerMenu-label" onClick={this.openLoginForm}>
                    Login
                  </Label>
                </Nav>)
              : (

                <Nav className="ml-auto" navbar>
                  {
                    decodedToken && (decodedToken.roles.includes(ROLES.RECIPIENT) || decodedToken.roles.includes(ROLES.ADMIN)) &&
                    <NavItem className="header-nav">
                      <NavLink className={pathname.startsWith('/account') ? 'headerMenu-active' : 'headerMenu'} to="/account">{decodedToken.email}</NavLink>
                    </NavItem>
                  }
                  <Label className="headerMenu-label" onClick={this.logout}>
                    Logout
                  </Label>
                </Nav>

              )}

          </Collapse>
        </Navbar>
        <AuthenticationModal
          modal={this.state.isOpenLoginForm}
          toggle={this.closeLoginForm}
        />
      </div >
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleLogout: async () => { await dispatch(logout()) },
  }
}

export default connect(null, mapDispatchToProps)(Header)
