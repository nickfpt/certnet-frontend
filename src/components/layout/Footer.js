import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Row >
          <Col md="12" lg={{ size: 10, offset: 1 }} >
                        Copyright © 2018 CertNet - LauRa Team
          </Col>
        </Row>
      </div>
    )
  }
}
export default Footer
