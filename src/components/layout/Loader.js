import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import React from 'react'
import PropTypes from 'prop-types'

const Loader = ({ height }) => {
  const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 400px; 
  `
  return (
    <BeatLoader
      className={override}
      sizeUnit={'px'}
      size={15}
      color={'#36D7B7'}
    />
  )
}

// Loader.propTypes = {
//   height: PropTypes.number.isRequired,
// }

Loader.defaultProps = {
  height: 500,
}

export default Loader
