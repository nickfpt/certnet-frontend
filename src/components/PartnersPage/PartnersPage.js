import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Row, Col, Input, Card, CardBody, CardHeader,
} from 'reactstrap'
import { getIssuer } from './../../actions/IssuersAction'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import get from 'lodash/get'
import styled from 'styled-components'
import Icon from './../form/icon'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`
const ContentCard = styled.p`
    margin-bottom: 8px;
`

class PartnersPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      partnerName: '',
      cardPartners: [],
    }
  }
  componentWillMount = async () => {
    const issuerId = get(this.props.match, ['params', 'issuerId'], null)
    // if (issuerId !== null) {
    // } else {
    // }
  }
  componentDidMount = async (e) => {
    await this.props.handleGetIssuer()
    this.setState({
      cardPartners: this.props.issuersReducer.issuersArr,
    })
  }
  handleInputChange = async (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    await this.setState({
      [name]: value,
    })
    // filter
    if (this.state.partnerName.trim() !== '') {
      let cardPartnersTemp = [...this.state.cardPartners]
      cardPartnersTemp = this.props.issuersReducer.issuersArr.filter(element => element.fullname.toLowerCase().includes(this.state.partnerName.toLowerCase().trim()))
      await this.setState({
        cardPartners: cardPartnersTemp,
      })
    } else {
      this.setState({
        cardPartners: this.props.issuersReducer.issuersArr,
      })
    }
  }
  render() {
    const { cardPartners } = this.state
    const { issuersReducer } = this.props
    return (
      <div>
        <Row>
          <Col sm="12" style={{ marginTop: '20px' }} >
            <div className="text-center">
              <h4>All Partners</h4>

            </div>
          </Col>
          <Col lg="12" xl={{ size: 8, offset: 2 }} style={{ marginBottom: '10px' }}>
            <Input
              placeholder="Enter partner's name"
              value={this.state.partnerName}
              name="partnerName"
              onChange={this.handleInputChange}
              type="text"
              style={{ marginTop: '5px', paddingRight: '40px' }}
              autoFocus
            />
            <Icon name="fas fa-search icon-search" color="#ADB5BD" iconSize="1.5em" />
          </Col>
        </Row>
        <Row>
          {issuersReducer.isFetching ? (
            <Col sm="12">
              <BeatLoader
                className={override}
                sizeUnit={'px'}
                size={15}
                color={'#36D7B7'}
              />
            </Col>)
            :
            (
              cardPartners.length > 0 ?
                (<Col xs="12" xl={{ size: 10, offset: 1 }}>
                  <Row>
                    {this.displayCardPartner(cardPartners)}
                  </Row>
                </Col>) :
                (<Col lg="12" xl={{ size: 8, offset: 2 }} style={{ marginBottom: '10px' }}>
                  <p>No data found</p>
                </Col>)
            )
          }
        </Row>
      </div >

    )
  }


  displayCardPartner = (arr) => {
    let result = null
    if (arr.length > 0) {
      result = arr.map((element, index) => (
        <Col xs="12" sm="6" lg="4" xl="3" key={index}>
          <Card className="cardStyle" outline color="primary">
            <a href={`/partners/${element._id}`} target="_blank" rel="noopener noreferrer">
              <CardHeader tag="h4" style={{ textAlign: 'center' }} className="cardHeaderStyle">
                {element.fullname}
              </CardHeader>
            </a>
            <div className="text-center" style={{ height: '40%', marginTop: 20 }}>
              <a href={`/partners/${element._id}`} target="_blank" rel="noopener noreferrer">
                <img
                  className="imgCardStyle" src={element.logo} alt={element.fullname}
                />
              </a>
            </div>
            <CardBody className="cardBodyPStyle">
              <ContentCard><b>Contact Email: </b>{element.contactEmail}</ContentCard>
              <ContentCard><b>Phone Number: </b>{element.phone}</ContentCard>
              <ContentCard><b>Website: </b><a href={element.website} target="_blank" rel="noopener noreferrer">{element.website}</a></ContentCard>
            </CardBody>
          </Card>
        </Col >
      ))
    }
    return result
  }

}
function mapStateToProps(state) {
  return {
    issuersReducer: state.issuersReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetIssuer: async () => { await dispatch(getIssuer()) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PartnersPage)

