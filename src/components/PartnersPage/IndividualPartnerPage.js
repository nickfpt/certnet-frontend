import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Row, Col, Input, Card, CardBody, CardHeader,
} from 'reactstrap'
import { getIndividualIssuer } from './../../actions/IssuersAction'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import get from 'lodash/get'
import styled from 'styled-components'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`
const ContentCard = styled.p`
    margin-bottom: 8px;
`

class IndividualPartnerPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      partner: {},
    }
  }
  componentDidMount = async () => {
    const issuerId = get(this.props.match, ['params', 'issuerId'], null)
    console.log(issuerId)
    if (issuerId !== null) {
      await this.props.handleGetIndividualIssuer(issuerId)
      console.log(this.props.issuersReducer)
      await this.setState({
        partner: this.props.issuersReducer.issuerData,
      })
    }
  }
  render() {
    const { partner } = this.state
    const { issuersReducer } = this.props
    return (
      <Row>
        {issuersReducer.isFetchingIndividual ? (
                    // console.log("ssss")
          <Col sm="12">
            <BeatLoader
              className={override}
              sizeUnit={'px'}
              size={15}
              color={'#36D7B7'}
            />
          </Col>
                )
                    :
                    (
                        Object.keys(partner).length > 0 &&
                        <Col xs={{ size: 10, offset: 1 }} lg={{ size: 8, offset: 2 }}>
                          <Card className="cardStyleIndividual" >
                            <CardHeader tag="h4" style={{ textAlign: 'center' }} >
                              {partner.fullname}
                            </CardHeader>
                            <div className="text-center" style={{ marginTop: 20 }}>
                              <img className="imgCardStyleIndividual" src={partner.logo} alt={partner.fullname} />
                            </div>
                            <CardBody style={{ margin: '20px 5% 25px 5%' }} >
                              <ContentCard><b>Owner: </b>{partner.ownerName}</ContentCard>
                              <ContentCard><b>Contact Email: </b>{partner.contactEmail}</ContentCard>
                              <ContentCard><b>Phone Number: </b>{partner.phone}</ContentCard>
                              <ContentCard><b>Address: </b>{partner.address}</ContentCard>
                              <ContentCard>
                                <b>Ethereum Address:</b>
                                <ul>
                                  {this.displayETHaddress(partner.ethereumAccounts)}
                                </ul>

                              </ContentCard>
                              <ContentCard><b>Website: </b><a href={partner.website} target="_blank" rel="noopener noreferrer">{partner.website}</a></ContentCard>
                              <ContentCard><b>Description: </b>{partner.description}</ContentCard>
                            </CardBody>
                          </Card>
                        </Col>
                    )
                }
      </Row>

    )
  }


  displayETHaddress = (arr) => {
    let result = null
    if (arr.length > 0) {
      result = arr.map((element, index) => (
        <li>{element.address}</li>
                ))
    }
    return result
  }

}
function mapStateToProps(state) {
  return {
    issuersReducer: state.issuersReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetIndividualIssuer: async (issuerId) => { await dispatch(getIndividualIssuer(issuerId)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IndividualPartnerPage)

