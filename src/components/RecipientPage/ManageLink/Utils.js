import React from 'react'


const createDataRow = (dataRow, index) => ({
  no: index + 1,
  link: dataRow.token,
  expireTime: dataRow.expiredTime,
  arrCert: dataRow.certNames,
  token: dataRow.token,
  verificationTime: dataRow.verificationTime,
})

export function makeData(arrLink) {
  const result = []
  for (let index = 0; index < arrLink.length; index += 1) {
    const element = createDataRow(arrLink[index], index)
    result.push(element)
  }
  return result
}


export const Tips = () =>
  (<div style={{ textAlign: 'center' }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>)
export function setPageSizeOption(size, arrOption = [10, 20, 25, 50, 100]) {
  const arr = []
  if (arrOption[0] >= size) {
    arr.push(arrOption[0])
  } else {
    for (let index = 0; index < arrOption.length; index += 1) {
      if (arrOption[index] <= size) {
        arr.push(arrOption[index])
      } else {
        arr.push(size)
        break
      }
    }
  }
  return arr
}
