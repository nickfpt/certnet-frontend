import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import ReactTable from 'react-table'
import { AlertList } from 'react-bs-notifier'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import styled from 'styled-components'
import { fetchDataAllLink, deleteLink, fetchDataAllLinkAgain } from './../../../actions/ManageLinkAction'
import { makeData, Tips, setPageSizeOption } from './Utils'

const dateFormat = require('dateformat')

const loader = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`
const actionBtn = {
  width: '100px',
  margin: '0px 10px',
}

const Container = styled(Col)`
  .ReactTable .rt-thead.-header {
    box-shadow: none;
    background: #E9E9E9;
    font-weight: bold;
  }
  .ReactTable.-striped .rt-tr.-odd {
    background: #00000000;
  }
  .ReactTable.-striped .rt-tr.-even {
    background: #00000004;
  }
`

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnRightAlign = {
  style: { textAlign: 'right' },
  headerStyle: { textAlign: 'right' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}

class ManageLink extends Component {
  constructor(props) {
    super(props)

    this.state = {
      modalDelete: false,
      tokenDelete: '',
      alerts: [],

    }
  }
  componentDidMount = () => {
    this.props.handleFetchDataAllLink()
  }
  onAlertDismissed(alert) {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  handleDeleteLink = async () => {
    this.setState({
      modalDelete: !this.state.modalDelete,
    })
    await this.props.handleDeleteLink(this.state.tokenDelete)
    if (this.props.manageLink.resultDelete) {
      this.generate('info', 'Link has been deleted!')
    } else {
      this.generate('danger', this.props.manageLink.errMsgDelete)
    }
    await this.props.handleFetchDataAllLinkAgain()
  }
  handleCopy = () => {
    this.generate('info', 'Link has been copied to clipboard!')
  }
  generate = (type, msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type,
      headline: 'Notification:',
      message: msg,
    }

    this.setState({
      alerts: [newAlert],
    })
  }

  toggleBtnDelete = (token) => {
    this.setState({
      modalDelete: !this.state.modalDelete,
      tokenDelete: token,
    })
  }
  displayCertList = (list) => {
    let result = null
    if (list.length > 0) {
      result = list.map((element, index) => (
        <li key={index} >{element}</li>
      ))
    }
    return result
  }

  render() {
    const columns = [
      {
        Header: '#',
        accessor: 'no',
        maxWidth: 60,
        ...ColumnCenterAlign,
      },
      {
        Header: 'Link',
        accessor: 'link',
        ...ColumnLeftAlign,
        Cell: row => (
          <div>
            <a href={`https://www.certnet.site/home/${row.original.link}`} target={'_blank'}>{`www.certnet.site/home/${row.original.link}`}</a>
          </div>
        ),
        minWidth: 180,
      },
      {
        Header: 'Views',
        accessor: 'verificationTime',
        ...ColumnRightAlign,
        minWidth: 80,
      },
      {
        Header: 'Expire Date',
        accessor: 'expireTime',
        Cell: row => (
          <div>
            {dateFormat(row.value, ' mmmm dS, yyyy')}
          </div>
        ),
        minWidth: 150,
        ...ColumnLeftAlign,
      },
      {
        Header: 'Actions',
        accessor: 'token',
        width: 300,
        Cell: row => (
          <div className="text-center">
            <Button color="danger" onClick={() => this.toggleBtnDelete(row.value)} style={actionBtn}>Delete</Button>
            <CopyToClipboard text={`https://www.certnet.site/home/${row.original.link}`}>
              <Button color="primary" style={actionBtn} onClick={() => this.handleCopy()}>Copy Link</Button>
            </CopyToClipboard>
          </div>
        ),
      },
    ]
    const { manageLink } = this.props
    const data = makeData(this.props.manageLink.allLinks)
    return (
      <div>
        <AlertList
          position="top-right"
          alerts={this.state.alerts}
          timeout={1000}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed.bind(this)}
        />
        <Row>
          <Col xs="12"> <h5>Your Active Links</h5><br /> </Col>
          {manageLink.isFetching ? (
            <Col sm="12">
              <BeatLoader
                className={loader}
                sizeUnit={'px'}
                size={15}
                color={'#36D7B7'}
              />
            </Col>)
            :
            (
              <Container sm="12">
                <ReactTable
                  getTdProps={() => ({
                    style: {
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'center',
                    },
                  })}
                  data={data}
                  columns={columns}
                  defaultPageSize={10}
                  pageSizeOptions={setPageSizeOption(data.length)}
                  className="-striped -highlight"
                  SubComponent={row => (
                    <div style={{ padding: '10px 0px 0px 50px' }}>
                      <ul type="square">
                        {this.displayCertList(row.original.arrCert)}
                      </ul>
                    </div>
                  )}
                />
                <br />
                <Tips />
              </Container>
            )
          }
        </Row>
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleBtnDelete} centered>
          <ModalHeader toggle={this.toggleBtnDelete}>Delete Link</ModalHeader>
          <ModalBody>
            Are you sure to delete this link?
          </ModalBody>
          <ModalFooter>
            <Button color="danger" size="lg" onClick={this.handleDeleteLink}>Delete</Button>{' '}
            <Button color="secondary" size="lg" onClick={this.toggleBtnDelete}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div >

    )
  }
}
function mapStateToProps(state) {
  return {
    manageLink: state.manageLinkReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleFetchDataAllLink: () => { dispatch(fetchDataAllLink()) },
    handleFetchDataAllLinkAgain: () => { dispatch(fetchDataAllLinkAgain()) },
    handleDeleteLink: async (token) => { await dispatch(deleteLink(token)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageLink)

