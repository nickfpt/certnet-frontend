import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { TabContent, Nav, NavItem } from 'reactstrap'
import { Route, Switch, NavLink, Redirect } from 'react-router-dom'
import ManageCertificate from './ManageCertificate/ManageCertificate'
import ManageLink from './ManageLink/ManageLink'
import * as Color from './../../constants/Color'
import arrTabs from './arrTabs'


const HomeWrapper = styled.section`
  padding-left: 1%;
  background: ${Color.LV1};
`

const tabStyle = {
  padding: '30px 15px',
}

class RecipientPage extends Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      activeTab: '1',
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      })
    }
  }

  render() {
    const { match } = this.props
    const { path } = match
    const shouldRedirect = match.url === this.props.location.pathname
    return (
      <HomeWrapper>
        {shouldRedirect && <Redirect to={`${path}/manage-cert`} />}
        <Nav tabs className="mr-auto">
          <div className="mx-auto d-sm-flex d-block flex-sm-nowrap">
            {this.showContentMenu(arrTabs)}
          </div>
        </Nav>
        <TabContent activeTab={this.state.activeTab} style={tabStyle}>

          <Switch>
            <Route path={`${path}/manage-cert`} component={ManageCertificate} />
            <Route path={`${path}/manage-link`} component={ManageLink} />
            <Redirect from={`${path}`} to={`${path}/manage-cert`} />
          </Switch>
        </TabContent>
      </HomeWrapper>
    )
  }
  showContentMenu(arrTabs) {
    const { path } = this.props.match
    let result = null
    if (arrTabs.length > 0) {
      result = arrTabs.map((nav, index) => (
        <NavItem key={index} className="NavItem-Recipient">
          <NavLink
            key={index}
            // className={classnames({ active: this.state.activeTab === nav.id })}
            className="nav-link"
            to={`${path}${nav.to}`}
            onClick={() => { this.toggle(nav.id) }}
          >
            {nav.title}
          </NavLink>
        </NavItem>

      ))
    }
    return result
  }
}
function mapStateToProps() {
  return {
  }
}

function mapDispatchToProps() {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipientPage)

