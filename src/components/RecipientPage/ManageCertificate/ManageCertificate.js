import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Row, Col, Input, Form, Alert, Modal, ModalHeader, ModalBody } from 'reactstrap'
import { fetchDataAllCert, generateShareLink } from './../../../actions/ManageCertificateAction'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import CardCertificate from './CardCertificate'
import { AlertList } from 'react-bs-notifier'
import Loader from '../../layout/Loader'

const dateFormat = require('dateformat')

const inputNumberDay = {
  marginLeft: '10px',
  width: '50px',
  display: 'inline-block',
  padding: '5px',
}
const btnGetLink = {
  marginLeft: '10px',
  marginBottom: '5.5px',
}
const colModalLink = {
  paddingRight: '5px',
  paddingLeft: '0px',
  marginBottom: '5px',
}


class ManageCertificate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      displayAllBatches: true,
      numberDays: 7,
      selectedCertIds: [],
      selectedFlag: true,
      modalGetLink: false,
      alerts: [],
      disableGetLinkBtn: false,
      validCerts: [],
      invalidCerts: [],
    }
  }
  componentDidMount = async () => {
    await this.props.fetchDataAllCert()
    if (this.props.manageCert.allCerts.length === 0) {
      await this.setState({
        disableGetLinkBtn: true
      })
    } else {
      await this.setState({
        disableGetLinkBtn: false,
        validCerts: this.props.manageCert.allCerts.filter(cert => !cert.metadata.revoked),
        invalidCerts: this.props.manageCert.allCerts.filter(cert => cert.metadata.revoked)
      })
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    if ((this.state.selectedCertIds !== nextState.selectedCertIds) && (this.state.selectedFlag === nextState.selectedFlag)) {
      return false
    }
    return true
  }
  onAlertDismissed = (alert) => {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  generate = (msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type: 'info',
      headline: 'Notification:',
      message: msg,
    }

    this.setState({
      alerts: [newAlert],
    })
  }

  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
    })
  }
  handleSelectCard = (certId) => {
    const { selectedFlag } = this.state
    const cloneSelectedCertIds = JSON.parse(JSON.stringify(this.state.selectedCertIds))
    const indexId = cloneSelectedCertIds.indexOf(certId)    // <-- Not supported in <IE9
    if (indexId !== -1) {
      cloneSelectedCertIds.splice(indexId, 1)
    } else {
      cloneSelectedCertIds.push(certId)
    }
    if (!selectedFlag) {
      this.setState({
        selectedCertIds: cloneSelectedCertIds,
        selectedFlag: true,
      })
    } else {
      this.setState({
        selectedCertIds: cloneSelectedCertIds
      })
    }
  }

  handleGetLink = async (e) => {
    e.preventDefault()
    if (this.state.selectedCertIds.length === 0) {
      this.setState({
        selectedFlag: false,
      })
      return
    }
    const dataPost = {
      certList: this.state.selectedCertIds,
      duration: this.state.numberDays,
    }
    await this.props.generateShareLink(dataPost)
    this.setState({
      modalGetLink: !this.state.modalGetLink,
    })
  }
  toggleModalGetLink = () => {
    this.setState({
      modalGetLink: !this.state.modalGetLink,
    })
  }
  focus = () => {
    this.inputLink.focus()
    this.inputLink.select()
    this.generate('Link has been copied to clipboard!')
  }
  displayCertCard = (allCerts, selectable = true) => {
    let result = null
    if (allCerts.length > 0) {
      result = allCerts.map((element, index) => (
        <CardCertificate key={index} data={element} handleSelect={this.handleSelectCard} selectable={selectable} />
      ))
    } else {
      result = (
        <div className="text-center" style={{ width: '100%' }}>
          <p>You don't have any certificate</p>
        </div>
      )
    }
    return result
  }

  render() {
    const { manageCert } = this.props
    const { disableGetLinkBtn, invalidCerts, validCerts } = this.state
    return (
      <div>
        <AlertList
          position="top-right"
          alerts={this.state.alerts}
          timeout={1000}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed}
        />
        <Row>
          <Col xs="12" xl={{ size: 10, offset: 1 }} style={{ textAlign: 'right' }} >
            <h6 style={{ display: 'inline-block' }} >Duration days</h6>
            <Form style={{ display: 'inline-block' }} onSubmit={this.handleGetLink}>
              <Input
                value={this.state.numberDays}
                onChange={this.handleInputChange}
                type="number"
                name="numberDays"
                min="1" max="14"
                style={inputNumberDay}
                disabled={disableGetLinkBtn}
              />
              <Button color="primary" style={btnGetLink} disabled={disableGetLinkBtn}>Get Link</Button>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col lg={{ size: 10, offset: 1 }}>
            {this.state.selectedFlag ? <br /> :
              (
                <Alert color="danger" fade={false}>
                  Please select at least one certificate.
                </Alert>
              )
            }
          </Col>
        </Row>
        <Row>
          {manageCert.isFetching ? (
            <Col sm="12">
              <Loader height={500} />
            </Col>)
            :
            (
              <Col lg={{ size: 10, offset: 1 }}>
                {validCerts.length === 0 && invalidCerts.length === 0 ? (
                  <Row>
                    <Col xs="12">
                      <div className="text-center" style={{ width: '100%' }}>
                        <p>You don't have any certificate.</p>
                      </div>
                    </Col>
                  </Row>
                ) :
                  (
                    <Row>
                      <Col xs="12">
                        <div className="cert-line">
                          <span className="cert-line-text" style={{ color: '#0169D9' }}>
                            Valid Certificates
                          </span>
                        </div>
                      </Col>
                      {this.displayCertCard(validCerts)}
                      <Col xs="12">
                        {/* <h4 style={{ color: '#C82333' }}>Invalid Certificate:</h4> */}
                        <div className="cert-line">
                          <span className="cert-line-text" style={{ color: '#C82333' }}>
                            Invalid Certificates
                          </span>
                        </div>
                      </Col>
                      {this.displayCertCard(invalidCerts, false)}
                    </Row>
                  )}
              </Col>
            )
          }
        </Row>
        <Modal isOpen={this.state.modalGetLink} toggle={this.toggleModalGetLink} keyboard={false} backdrop={'static'} size="lg" centered>
          <ModalHeader toggle={this.toggleModalGetLink}>Share Link</ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12" sm="12" md="10" style={colModalLink}>
                <Input
                  innerRef={(input) => { this.inputLink = input }}
                  readOnly
                  value={`https://www.certnet.site/home/${manageCert.shareLinkData.token}`}
                />
              </Col>
              <Col xs="12" sm="12" md="2" style={colModalLink}>
                <CopyToClipboard text={`https://www.certnet.site/home/${manageCert.shareLinkData.token}`}>
                  <Button color="primary" block onClick={this.focus} >Copy</Button>
                </CopyToClipboard>
              </Col>
            </Row>
            Expire Time: {dateFormat(manageCert.shareLinkData.expiredTime, ' mmmm dS, yyyy, h:MM:ss TT')}
          </ModalBody>
        </Modal>
      </div >

    )
  }

}
function mapStateToProps(state) {
  return {
    manageCert: state.manageCertificateReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchDataAllCert: async () => { await dispatch(fetchDataAllCert()) },
    generateShareLink: (data) => { dispatch(generateShareLink(data)) },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageCertificate)

