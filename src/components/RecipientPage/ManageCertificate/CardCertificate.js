import React from 'react'
import { Card, Button, CardHeader, Col, Modal, ModalHeader, ModalBody, CardBody } from 'reactstrap'
import { Tips, setPageSizeOption } from './Utils'
import { makeDataCert } from './Data'
import ReactTable from 'react-table'
import styled from 'styled-components'
import get from 'lodash/get'

const dateFormat = require('dateformat')

const ContentCard = styled.p`
    margin-bottom: 7px;
`
const CertCartContentSelected = {
  color: '#51708F',
  backgroundColor: '#D9EDF7',
}
const btnSelectCard = {
  borderTopColor: '#007bff',
  borderRadius: '0px',
  boxShadow: '0 0 0 0',
  borderBottom: '0px',
  borderLeft: '0px',
  borderRight: '0px',
  height: '45px',
  // color: 'rgb(0, 123, 255)'
}
const Link = styled.a`
color: #007bff !important;
`

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}


class CardCertificate extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      tooltipOpen: false,
      selected: false,
      colorType: 'secondary',
      btnText: 'Select',
      modal: false,
    }
  }
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    })
  }
  handleClickSelect = () => {
    const certId = this.props.data._id
    this.setState({
      selected: !this.state.selected,
      colorType: this.state.colorType === 'secondary' ? 'primary' : 'secondary',
      btnText: this.state.btnText === 'Select' ? 'Selected' : 'Select',
    })
    this.props.handleSelect(certId)
  }
  handleToggle = () => {
    this.setState({
      modal: !this.state.modal,
    })
  }

  render() {
    const issuerName = get(this.props.data, ['content', 'issuerInfo', 'name'], null)
    const issuerLogo = get(this.props.data, ['content', 'issuerInfo', 'logo'], null)
    const issuerId = get(this.props.data, ['issuerId'], '')

    const { certName } = this.props.data.content
    const { issueTime } = this.props.data.metadata
    const data = makeDataCert(this.props.data)
    return (
      <Col xs="12" sm="6" lg="4" xl="3">
        <Card body outline color="primary" className="card-Certificate">
          <div style={this.state.selected ? CertCartContentSelected : null}>
            <CardHeader tag="h4" className="card-header-Cert" onClick={this.handleToggle}>
              {certName}
            </CardHeader>
            <div style={{ padding: '10px' }}>
              <div className="text-center" style={{ height: '40%', marginTop: 20 }}>
                <img
                  className="imgCardCertStyle" src={issuerLogo} alt={issuerName}
                  onClick={this.handleToggle}
                />
              </div>
              <CardBody style={{ marginBottom: 25 }} className="cardBodyStyle">
                <ContentCard>
                  <Link href={`/partners/${issuerId}`} target="_blank" rel="noopener noreferrer">{issuerName}</Link></ContentCard>
                <ContentCard style={{ fontSize: 15 }}>
                  {dateFormat(issueTime, ' mmmm dS, yyyy')}
                </ContentCard>
                <Link onClick={this.handleToggle}>View Details</Link>
              </CardBody>
            </div>
          </div>
          <Button
            disabled={!this.props.selectable}
            outline={!this.state.selected}
            className={!this.state.selected && this.props.selectable ? 'btn-card-selectable' : ''}
            color={this.state.colorType}
            onClick={this.handleClickSelect}
            style={btnSelectCard}
          >
            {this.state.btnText}
          </Button>
        </Card>
        <br />
        <Modal isOpen={this.state.modal} toggle={this.handleToggle} className={this.props.className} size="lg">
          <ModalHeader toggle={this.handleToggle}>{certName}</ModalHeader>
          <ModalBody>
            <p style={{ marginBottom: 15 }}><b>Issue Date</b>: {dateFormat(issueTime, ' mmmm dS, yyyy')}</p>
            <ReactTable
              data={data}
              columns={[
                {
                  Header: '#',
                  accessor: 'no',
                  maxWidth: 60,
                  ...ColumnCenterAlign,
                },
                {
                  Header: 'Subject',
                  accessor: 'subject',
                  maxWidth: 250,
                  ...ColumnLeftAlign,
                },
                {
                  Header: 'Value',
                  accessor: 'value',
                  ...ColumnLeftAlign,
                },
              ]}
              defaultSorted={[
                {
                  id: 'index',
                  desc: false,
                },
              ]}
              defaultPageSize={10}
              className="-striped -highlight"
              pageSizeOptions={setPageSizeOption(data.length)}
            />
            <Tips />
          </ModalBody>
        </Modal>
      </Col>
    )
  }
}

export default CardCertificate
