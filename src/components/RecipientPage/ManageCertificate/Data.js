// import { DialogContentText } from "@material-ui/core";
import { camelCase } from 'lodash'
import { mapRecipientInfo, mapContent } from './../../../constants/mapCertColumn'
import get from 'lodash/get'

export function makeDataCert(cert) {
  let dataAll = []
  const arrHeader = cert.header
  const { additionInfor } = cert.content
  const mapRecipientInfoKey = Object.keys(mapRecipientInfo)
  const mapRecipientInfoValue = Object.values(mapRecipientInfo)
  const mapContentKey = Object.keys(mapContent)
  const mapContentValue = Object.values(mapContent)
  for (let i = 0; i < arrHeader.length; i += 1) {
    const dataRow = {}
    dataRow.no = i + 2
    dataRow.subject = arrHeader[i]
         // map data RecipientInfo
    for (let y = 0; y < mapRecipientInfoValue.length; y += 1) {
      if (camelCase(mapRecipientInfoValue[y]) === camelCase(arrHeader[i])) {
        dataRow.value = cert.content.recipientInfo[mapRecipientInfoKey[y]]
      }
    }
         // map data CertInfo
    for (let y = 0; y < mapContentValue.length; y += 1) {
      if (camelCase(mapContentValue[y]) === camelCase(arrHeader[i])) {
        dataRow.value = cert.content[mapContentKey[y]]
      }
    }
         // map data additionInfor
    if (additionInfor !== undefined) {
      const mapAdditiontKey = Object.keys(additionInfor)
      const mapAdditiontValue = Object.values(additionInfor)
      for (let y = 0; y < mapAdditiontKey.length; y += 1) {
        if (mapAdditiontKey[y] === camelCase(arrHeader[i])) {
          dataRow.value = mapAdditiontValue[y]
        }
      }
    }
    dataAll.push(dataRow)
  }
  dataAll = [{
    no: 1,
    subject: 'Issuer',
    value: get(cert, ['content', 'issuerInfo', 'name'], null),
  }, ...dataAll]
  return dataAll
}
