const arrTabs = [
  {
    id: '1',
    title: 'Manage Certificates',
    to: '/manage-cert',
  },
  {
    id: '2',
    title: 'Manage Links',
    to: '/manage-link',
  },
]
export default arrTabs
