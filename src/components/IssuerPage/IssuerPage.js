import React, { Component } from 'react'
import styled from 'styled-components'
import * as Color from './../../constants/Color'
// import classnames from 'classnames';
import { connect } from 'react-redux'
import { TabContent, Nav, NavItem, Row, Col } from 'reactstrap'
import ManageBatch from './ManageBatch/ManageBatch'
import ManageTemp from './ManageTemplate/ManageTemp'
import IssueBatch from './IssueBatch/IssueBatch'
// import FieldDn./ManageTemplate/FieldDnDlate/FieldDnD"
import arrTabs from './arrTabs'
import { Route, Switch, NavLink, Redirect } from 'react-router-dom'

const HomeWrapper = styled.section`
  padding-left: 1%;
  background: ${Color.LV1};
`
const rowStyle = {
  minHeight: '750px',
}
const colStyle = {
  background: Color.LV1,
}
const tabStyle = {
  padding: '30px 15px',
}

class IssuerPage extends Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      activeTab: '1',
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      })
    }
  }

  render() {
    const { match } = this.props
    const { path } = match
    const shouldRedirect = match.url === this.props.location.pathname
    return (

      <HomeWrapper>
        {shouldRedirect && <Redirect to={`${path}/batch`} />}
        <Row style={rowStyle}>
          <Col style={colStyle} md="12" lg={{ size: 12, offset: 0 }} >
            <Nav tabs className="mr-auto">
              <div className="mx-auto d-sm-flex d-block flex-sm-nowrap">
                {this.showContentMenu(arrTabs)}
              </div>
            </Nav>
            <TabContent activeTab={this.state.activeTab} style={tabStyle}>

              <Switch>
                <Route path={`${path}/batch`} component={IssueBatch} />
                <Route path={`${path}/manage-cert`} component={ManageBatch} />
                <Route path={`${path}/manage-tem`} component={ManageTemp} />
                <Redirect from={`${path}`} to={`${path}/batch`} />
              </Switch>
            </TabContent>
          </Col>
        </Row>
      </HomeWrapper>
    )
  }
  showContentMenu(arrTabs) {
    const { path } = this.props.match
    let result = null
    if (arrTabs.length > 0) {
      result = arrTabs.map((nav, index) => (
        <NavItem key={index}>
          <NavLink
            key={index}
                            // className={classnames({ active: this.state.activeTab === nav.id })}
            className="nav-link"
            to={`${path}${nav.to}`}
            onClick={() => { this.toggle(nav.id) }}
          >
            {nav.title}
          </NavLink>
        </NavItem>

                ))
    }
    return result
  }
}
function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IssuerPage)

