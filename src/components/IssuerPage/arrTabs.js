const arrTabs = [
  {
    id: '1',
    title: 'Issue Certificate Batch',
    to: '/batch',
  },
  {
    id: '2',
    title: 'Manage Issued Certificates',
    to: '/manage-cert',
  },
  {
    id: '3',
    title: 'Manage Certificate Templates',
    to: '/manage-tem',
  },
]
export default arrTabs
