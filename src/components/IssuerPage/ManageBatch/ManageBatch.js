import React, { Component } from 'react'
import styled from 'styled-components'
import { fetchBatchData, getListCertsByBatchId } from './../../../actions/ManageBatchAction'
import { connect } from 'react-redux'
import { Row, Col, Button, Table, Label, Input, Badge } from 'reactstrap'
import { css } from 'react-emotion'
import { BeatLoader } from 'react-spinners'
import ReactTable from 'react-table'
import { makeData, Tips, makeListTagData } from './Utils'
import { setPageSizeOption } from './../../../Utils'
import Icon from '../../form/icon'
import { makeDataCert } from './Data'
import matchSorter from 'match-sorter'
import TagsInput from 'react-tagsinput'
import Select from 'react-select'
import RevokeBatchConfirmModal from './RevokeBatchModal'
import RevokeCertificateConfirmModal from './RevokeCertificateModal'
import jwt from 'jsonwebtoken'
import Cookies from 'universal-cookie'

const dateFormat = require('dateformat')

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`

const NoData = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 15px;
  height: 150px;
`
const Container = styled.div`
  .ReactTable .rt-thead.-header {
    box-shadow: none;
    background: #E9E9E9;
    font-weight: bold;
  }
  .ReactTable.-striped .rt-tr.-odd {
    background: #00000000;
  }
  .ReactTable.-striped .rt-tr.-even {
    background: #00000004;
  }
`

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnRightAlign = {
  style: { textAlign: 'right' },
  headerStyle: { textAlign: 'right' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}
class ManageBatch extends Component {
  constructor(props) {
    const cookies = new Cookies()
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    super(props)
    this.state = {
      displayAllBatches: true,
      selectBatch: {},
      titleFilter: '',
      descriptionFilter: '',
      tagsFilter: [],
      revokeBatchModal: false,
      revokeCertificateModal: false,
      revokeBatchMerkleRoot: '',
      revokedBatchId: '',
      revokeObjectName: '',
      revokeBatch: false,
      // revoke certificate state:
      revokeRoot: '',
      revokeTargetHash: '',
      revokeProof: '',
      revokedCertId: '',
      recipientName: '',
      certName: '',
      token,
    }
  }
  componentDidMount = async () => {
    await this.reload()
  }
  reload = async () => {
    await this.props.handleFetchBatchData()
    this.setState({
      dataAllBatches: makeData(this.props.manageBatch.batchAll),
    })
  }
  handleInputFilterChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      validateFlag: true,
    })
  }
  handleTagsFilterChange = (tagsFilter) => {
    this.setState({ tagsFilter })
  }
  handleBatchDetail = async (data) => {
    const selectBatch = JSON.parse(data)
    await this.props.handleGetListCertsByBatchId(selectBatch._id)
    this.setState({
      selectBatch,
    })
    this.toggleDetail()
  }
  reloadBatchDetail = async (batchId) => {
    await this.props.handleGetListCertsByBatchId(batchId)
  }
  toggleDetail = () => {
    this.setState({
      displayAllBatches: !this.state.displayAllBatches,
    })
  }
  createTableHeader = (arrHeader) => {
    const { token } = this.state
    const tableHeader = [
      {
        Header: 'Action',
        Cell: row => {
          return row.original.revoked === false &&
            <Button color="danger" size="sm"
              onClick={async () => {
                await this.setState({
                  revokeRoot: row.original.merkleRoot,
                  revokeTargetHash: row.original.targetHash,
                  revokeProof: row.original.proof,
                  revokedCertId: row.original.id,
                  recipientName: row.original.recipientName,
                  certName: row.original.certName,
                })
                this.toggleRevokeCertificateModal()
              }}
              style={{ marginLeft: 10 }}
              disabled={!token.issuableStatus}
            >Revoke</Button>
        },
        filterable: false,
      },
      {
        Header: 'Status',
        accessor: 'revoked',
        Cell: row => (
          <div className="text-center">
            {row.value ? (<b style={{ color: '#C82333' }}>Revoked</b>) : (<b style={{ color: '#0169D9' }}>Valid</b>)}
          </div>
        ),
        minWidth: 110,
        filterable: false,
      }
    ]
    for (let index = 0; index < arrHeader.length; index += 1) {
      const obj = {
        Header: arrHeader[index],
        accessor: `header${index}`,
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, { keys: [`header${index}`] }),
        filterAll: true,
      }
      tableHeader.push(obj)
    }
    return tableHeader
  }
  toggleRevokeBatchModal = () => {
    this.setState({
      revokeBatchModal: !this.state.revokeBatchModal
    })
  }
  toggleRevokeCertificateModal = () => {
    this.setState({
      revokeCertificateModal: !this.state.revokeCertificateModal
    })
  }
  render() {
    const { manageBatch } = this.props
    const { displayAllBatches, selectBatch, dataAllBatches, titleFilter, descriptionFilter,
      tagsFilter, revokeBatchModal, revokeBatch, revokeCertificateModal, token,
      revokeRoot, revokeTargetHash, revokeProof, revokedCertId, recipientName, certName } = this.state
    let result = dataAllBatches
    if (titleFilter.trim() !== '') {
      result = result.filter(batch => batch.title.toLowerCase().includes(titleFilter.trim().toLowerCase()))
    }
    if (descriptionFilter.trim() !== '') {
      result = result.filter(batch => batch.description !== undefined && batch.description.toLowerCase().includes(descriptionFilter.trim().toLowerCase()))
    }
    if (tagsFilter.length > 0) {
      result = result.filter((batch) => {
        let flag = true
        tagsFilter.forEach((element) => {
          if (!batch.tags.includes(element.value)) {
            flag = false
          }
        })
        return flag
      })
    }
    return (
      <Container>
        <Row>
          {displayAllBatches ?
            (
              <Col sm="12">
                <Row>
                  <Col sm="4">
                    <Label>Title</Label>
                    <Input
                      type="text"
                      name="titleFilter"
                      value={this.state.titleFilter}
                      id="title"
                      placeholder="Enter title"
                      onChange={this.handleInputFilterChange}
                    />
                  </Col>
                  <Col sm="4">
                    <Label>Description</Label>
                    <Input
                      type="text"
                      name="descriptionFilter"
                      value={this.state.descriptionFilter}
                      id="title"
                      placeholder="Enter description"
                      onChange={this.handleInputFilterChange}
                    />
                  </Col>
                  <Col sm="4">
                    <Label>Tags</Label>
                    <Select
                      isMulti
                      name="tagsFilter"
                      options={makeListTagData(dataAllBatches)}
                      className="basic-multi-select"
                      classNamePrefix="select"
                      onChange={this.handleTagsFilterChange}
                      value={this.state.tagsFilter}
                    />
                    <br />
                  </Col>
                  <Col sm="12">
                    {
                      manageBatch.isFetching ? (
                        <BeatLoader
                          className={override}
                          sizeUnit={'px'}
                          size={15}
                          color={'#36D7B7'}
                        />) : (
                          <div>
                            <ReactTable
                              getTdProps={() => ({
                                style: {
                                  display: 'flex',
                                  flexDirection: 'column',
                                  justifyContent: 'center',
                                },
                              })}
                              data={result}
                              defaultFilterMethod={(filter, row) =>
                                String(row[filter.id]) === filter.value}
                              columns={[{
                                Header: '#',
                                accessor: 'no',
                                maxWidth: 60,
                                ...ColumnCenterAlign,
                              },
                              {
                                Header: 'Title',
                                accessor: 'title',
                                ...ColumnLeftAlign,
                              },
                              {
                                Header: 'Description',
                                id: 'description',
                                accessor: 'description',
                                ...ColumnLeftAlign,
                              },
                              {
                                Header: 'Tags',
                                accessor: 'tags',
                                Cell: row => (
                                  <TagsInput
                                    className=""
                                    value={row.value}
                                    onChange={this.handleChange}
                                    disabled
                                    inputProps={{ placeholder: '' }}
                                    tagProps={{ className: 'tagsinput-custom' }}
                                  />
                                ),
                              },
                              {
                                Header: 'Quantity',
                                accessor: 'quantity',
                                ...ColumnRightAlign,
                                minWidth: 70
                              },
                              {
                                Header: 'Issue Date',
                                accessor: 'time',
                                Cell: row => (
                                  <div className="text-center">
                                    {dateFormat(row.value, ' mmmm dS, yyyy')}
                                  </div>
                                ),
                                minWidth: 150
                              },
                              {
                                Header: 'Status',
                                accessor: 'revoked',
                                Cell: row => (
                                  <div className="text-center">
                                    {row.value ? (<b style={{ color: '#C82333' }}>Revoked</b>) : (<b style={{ color: '#0169D9' }}>Valid</b>)}
                                  </div>
                                ),
                                minWidth: 150
                              },
                              {
                                Header: 'Actions',
                                accessor: 'id',
                                width: 200,
                                Cell: row => (
                                  <div >
                                    <Button color="primary" style={{ width: '80px' }} outline onClick={() => this.handleBatchDetail(row.value)}>View</Button>
                                    {/* {!(row.original.revoked === true || !token.issuableStatus) &&
                                      <Button color="danger"  style={btnStyle} onClick={() => {
                                        this.setState({
                                          revokeBatchMerkleRoot: row.original.merkleRoot,
                                          revokedBatchId: row.original._id,
                                          revokeObjectName: row.original.title,
                                          revokeBatchModal: true,
                                        })
                                        this.toggleRevokeBatchModal()
                                      }}
                                        style={{ marginLeft: 10 }}
                                        disabled={row.original.revoked === true || !token.issuableStatus}>Revoke</Button>} */}
                                    <Button color="danger" onClick={() => {
                                      this.setState({
                                        revokeBatchMerkleRoot: row.original.merkleRoot,
                                        revokedBatchId: row.original._id,
                                        revokeObjectName: row.original.title,
                                        revokeBatchModal: true,
                                      })
                                      this.toggleRevokeBatchModal()
                                    }}
                                      style={{ marginLeft: 10, width: '80px' }}
                                      disabled={row.original.revoked === true || !token.issuableStatus}>Revoke</Button>
                                  </div>
                                ),
                              },
                              ]}
                              defaultPageSize={10}
                              pageSizeOptions={setPageSizeOption(manageBatch.batchAll.length)}
                              className="-striped -highlight templateTable"
                            />
                            <Tips />
                          </div>
                        )
                    }
                  </Col>
                </Row>
                {revokeBatchModal && <RevokeBatchConfirmModal
                  modal={revokeBatchModal}
                  toggle={this.toggleRevokeBatchModal}
                  revokeBatch={revokeBatch}
                  revokeBatchMerkleRoot={this.state.revokeBatchMerkleRoot}
                  revokeObjectName={this.state.revokeObjectName}
                  revokedBatchId={this.state.revokedBatchId}
                  reload={this.reload}
                />}
              </Col>)
            :
            (
              <Col xs="12">
                <Row>
                  <Col xs="12" sm="1">
                    <Button color="primary" onClick={this.toggleDetail}>
                      <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
                      Back
                    </Button>
                  </Col>
                  <Col xs="12" sm={{ size: 8, offset: 1 }}>
                    <div className="table-responsive">
                      <Table borderless>
                        <tbody>
                          <tr>
                            <th scope="row" width="10%">Title:</th>
                            <td width="40%"> {selectBatch.title}</td>
                            <th scope="row" width="10%">Quantity:</th>
                            <td width="15%">{selectBatch.quantity}</td>
                            <th scope="row" width="5%">Time:</th>
                            <td width="20%">{dateFormat(selectBatch.issueTime, ' mmmm dS, yyyy')}</td>
                          </tr>
                          <tr>
                            <th scope="row" >Description:</th>
                            <td> {selectBatch.description}</td>
                            <th scope="row">Tags</th>
                            <td colSpan="3">
                              <TagsInput
                                className=""
                                value={selectBatch.tags}
                                onChange={this.handleChange}
                                disabled
                                inputProps={{ placeholder: '' }}
                              />
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </Col>
                  <Col xs="12">
                    <ReactTable
                      filterable
                      data={makeDataCert(manageBatch.listCerts.certs, selectBatch.header)}
                      columns={this.createTableHeader(selectBatch.header)}
                      defaultPageSize={10}
                      className="-striped"
                      NoDataComponent={() => (
                        <NoData>
                          {manageBatch.isFetching ? ' ' : 'No rows found'}
                        </NoData>
                      )}
                    />
                  </Col>
                </Row>
                {revokeCertificateModal && <RevokeCertificateConfirmModal
                  modal={revokeCertificateModal}
                  toggle={this.toggleRevokeCertificateModal}
                  reload={this.reloadBatchDetail}
                  revokeRoot={revokeRoot}
                  revokeTargetHash={revokeTargetHash}
                  revokeProof={revokeProof}
                  revokedCertId={revokedCertId}
                  recipientName={recipientName}
                  certName={certName}
                  selectedBatchId={selectBatch._id}
                />}
              </Col>
            )}
        </Row>
      </Container >

    )
  }

}
function mapStateToProps(state) {
  return {
    manageBatch: state.manageBatchReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleFetchBatchData: () => dispatch(fetchBatchData()),
    handleGetListCertsByBatchId: batchId => dispatch(getListCertsByBatchId(batchId)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageBatch)

