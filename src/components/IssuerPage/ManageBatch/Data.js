// import { DialogContentText } from "@material-ui/core";
import { camelCase } from 'lodash'
import { mapRecipientInfo, mapContent } from './../../../constants/mapCertColumn'

const range = (len) => {
  const arr = []
  for (let i = 0; i < len; i += 1) {
    arr.push(i)
  }
  return arr
}

const transformProofToSingleString = (proofs) => {
  const result = []
  proofs.forEach((proof) => {
    if (proof.left) {
      result.push(`00${proof.left}`)
    }
    if (proof.right) {
      result.push(`01${proof.right}`)
    }
  })
  return `0x${result.join('')}`
}
const newDataRowCert = (content, arrHeader, cert) => {
  const dataRow = {}
  const recipientInfoKey = Object.keys(content.recipientInfo)
  const recipientInfoValue = Object.values(content.recipientInfo)
  const mapRecipientInfoKey = Object.keys(mapRecipientInfo)
  const mapRecipientInfoValue = Object.values(mapRecipientInfo)
    // map key - recipientInfo
  for (let i = 0; i < recipientInfoKey.length; i += 1) {
    for (let y = 0; y < mapRecipientInfoKey.length; y += 1) {
      if (recipientInfoKey[i] === mapRecipientInfoKey[y]) {
                // map to header value
        for (let x = 0; x < arrHeader.length; x += 1) {
                    // check header column vs db column name
          if (camelCase(mapRecipientInfoValue[y]) === camelCase(arrHeader[x])) {
            dataRow[`header${x}`] = recipientInfoValue[i]
          }
        }
      }
    }
  }

  const contentKey = Object.keys(content)
  const contentValue = Object.values(content)
  const mapContentKey = Object.keys(mapContent)
  const mapContentValue = Object.values(mapContent)
    // map key - content
  for (let i = 0; i < contentKey.length; i += 1) {
    for (let y = 0; y < mapContentKey.length; y += 1) {
      if (contentKey[i] === mapContentKey[y]) {
                // map to header value
        for (let x = 0; x < arrHeader.length; x += 1) {
                    // check header column vs db column name
          if (camelCase(mapContentValue[y]) === camelCase(arrHeader[x])) {
            dataRow[`header${x}`] = contentValue[i]
          }
        }
      }
    }
  }

  if (content.additionInfor !== undefined) {
    const additionInfoKey = Object.keys(content.additionInfor)
    const additionInfoValue = Object.values(content.additionInfor)
    for (let i = 0; i < additionInfoKey.length; i += 1) {
      for (let x = 0; x < arrHeader.length; x += 1) {
                // check header column vs db column name
        if (additionInfoKey[i] === camelCase(arrHeader[x])) {
          dataRow[`header${x}`] = additionInfoValue[i]
        }
      }
    }
  }
  dataRow.revoked = cert.metadata.revoked
  dataRow.merkleRoot = `0x${cert.signature.merkleRoot}`
  dataRow.proof = transformProofToSingleString(cert.signature.proof)
  dataRow.targetHash = `0x${cert.signature.targetHash}`
  dataRow.id = cert._id
  dataRow.recipientName = content.recipientInfo.name
  dataRow.certName = content.certName
  return dataRow
}

export function makeDataCert(certs, arrHeader) {
  return range(certs.length).map(d => ({
    ...newDataRowCert(certs[d].content, arrHeader, certs[d]),
  }))
}
