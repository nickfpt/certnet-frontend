import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Alert, Progress } from 'reactstrap'
import * as callAPI from '../../../constants/CallAPI'
import * as endpoints from '../../../constants/endpoints'
import { NETWORK } from '../../../constants/common'
import { web3, appendPrefix } from '../../../services/web3'
import Cookies from "universal-cookie"
import jwt from "jsonwebtoken";
import Certnet from '../../../contract/certnet'

const cookies = new Cookies()

class ConfirmModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      alert: false,
      error: '',
      web3Alert: false,
      web3Error: '',
      web3CurrentWallet: null,
      disableIssue: false,
      disableClose: false,
      message: null,
      loader: false,
      issuerETHaddresses: [],
      progress: 0
    }
  }

  async componentDidMount() {
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    this.setState({
      issuerETHaddresses: token.ethereumAccounts
    })
    const message = `Are you sure to revoke certificate ${this.props.certName} of ${this.props.recipientName} ?`
    if (typeof web3 !== 'undefined') {
      this.setState({ web3Checked: true, web3Supported: true })
      const checkAccountChange = async () => {
        const network = await web3.eth.net.getNetworkType()
        if (network !== NETWORK.RINKENY) {
          this.setState({
            web3Alert: true,
            web3Error: 'Please switch to Rinkeby network',
          })
        } else {
          const accounts = await web3.eth.getAccounts()
          if (accounts.length > 0) {
            if (this.state.issuerETHaddresses.includes(accounts[0])) {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: false,
              })
              if (!this.state.message) {
                this.setState({
                  message
                })
              }
            } else {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: true,
                web3Error: 'Please switch to Ethereum address you registered with this account',
              })
            }
          } else {
            this.setState({
              web3Alert: true,
              web3Error: 'Please login to Metamask',
            })
          }
        }
      }

      await checkAccountChange()
      if (!this.state.alert && !this.state.web3Alert) {
        this.setState({
          message
        })
      }
      this.checkChangeWalletInterval = setInterval(checkAccountChange, 1000)
    } else {
      this.setState({ web3Alert: true, web3Error: 'Metamask is not installed.' })
    }
  }

  async revoke() {
    const { revokeTargetHash, revokedCertId, revokeProof, revokeRoot, selectedBatchId, reload } = this.props
    const { web3CurrentWallet: sender } = this.state
    await this.setState({
      loader: true,
      disableClose: true,
      disableIssue: true,
      message: 'Revoke process in progress. Please do not close the dialog or browser!',
    })
    try {
      if (!revokeRoot) {
        return this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Merkle root is not found',
        })
      }
      let intervalId
      await Certnet.methods.revokeSingleCertificate(revokeTargetHash, revokeRoot, revokeProof).send({
        from: sender
      }, (e, processedContract) => {
        if (processedContract) {
          let time = 0
          intervalId = setInterval(() => {
            if (time <= 30) {
              time += 1
              this.setState({
                progress: time * 100 / 35
              })
            }
          }, 1000)
        }
      })
      const revokeResponse = await callAPI.fetch(endpoints.revokeCert(revokedCertId))
      if (revokeResponse.ok) {
        this.setState({
          progress: 100
        })
        clearInterval(intervalId)
        setTimeout(() => {
          this.setState({
            loader: false,
            disableClose: false,
            message: 'Revoke certificate successfully !',
          })
          reload(selectedBatchId)
        }, 800)
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: revokeResponse.error,
        })
      }
    } catch (err) {
      if (err.message.includes('User denied transaction signature')) {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Revoke process is canceled!',
        })
      }
    }
  }

  render() {
    const { modal, toggle } = this.props
    const { alert, error, disableIssue, message, loader, disableClose, web3Alert, web3Error, progress } = this.state
    return (<div>
      <Modal isOpen={modal} style={{ minWidth: '900px' }} className="text-center">
        <ModalHeader>Confirm revoke certificate</ModalHeader>
        <ModalBody>
          {alert && <Alert color="danger">
            {error}
          </Alert>}
          {web3Alert && <Alert color="danger">
            {web3Error}
          </Alert>}
          {!alert && (<strong>{message}</strong>)}
          {!alert && loader &&
          <Progress striped color="danger" value={progress} style={{ marginTop: 20 }}/>}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" size="lg" disabled={disableClose} onClick={toggle}>Close</Button>{' '}
          {!alert && !web3Alert &&
          <Button color="danger" size="lg" disabled={disableIssue} onClick={() => this.revoke()}>Revoke</Button>}
        </ModalFooter>
      </Modal>
    </div>)
  }
}

ConfirmModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  reload: PropTypes.func.isRequired,
  revokeRoot: PropTypes.string.isRequired,
  revokeTargetHash: PropTypes.string.isRequired,
  revokeProof: PropTypes.string.isRequired,
  revokedCertId: PropTypes.string.isRequired,
  recipientName: PropTypes.string.isRequired,
  certName: PropTypes.string.isRequired,
  selectedBatchId: PropTypes.string.isRequired
}

export default ConfirmModal
