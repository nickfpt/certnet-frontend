import { uniqBy } from 'lodash'
import React from 'react'

const createTemp = (temp, index) => ({
  no: index + 1,
  title: temp.title,
  description: temp.description,
  tags: temp.tags,
  time: temp.issueTime,
  quantity: temp.quantity,
  id: JSON.stringify(temp),
  merkleRoot: temp.merkleRoot,
  revoked: temp.revoked,
  _id: temp._id
})

export function makeData(arrTemp) {
  const result = []
  for (let index = 0; index < arrTemp.length; index += 1) {
    const element = createTemp(arrTemp[index], index)
    result.push(element)
  }
  return result
}
export function makeListTagData(batches) {
  const ListTagData = []
  if (batches !== undefined && batches.length > 0) {
    batches.forEach((batch, index) => {
      if (batch.tags !== undefined && batches.length > 0) {
        batch.tags.forEach((element) => {
          const option = {
            value: element,
            label: element,
          }
          ListTagData.push(option)
        })
      }
    })
  }
  return uniqBy(ListTagData, 'value')
}
export function setPageSizeOption(size, arrOption = [10, 20, 25, 50, 100]) {
  const arr = []
  if (arrOption[0] >= size) {
    arr.push(arrOption[0])
  } else {
    for (let index = 0; index < arrOption.length; index += 1) {
      if (arrOption[index] <= size) {
        arr.push(arrOption[index])
      } else {
        arr.push(size)
        break
      }
    }
  }
  return arr
}
export const Tips = () =>
  (<div style={{ textAlign: 'center' }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>)

