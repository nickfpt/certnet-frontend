/* eslint-disable class-methods-use-this,react/sort-comp */
import React from 'react'
import { connect } from 'react-redux'
import { Button, Row, Col, Table, Modal, ModalHeader, ModalBody } from 'reactstrap'
import Select from 'react-select'
import { Prompt } from 'react-router-dom'
import CanvasTpl from './CanvasTpl'
import { getContentPath } from './../../../Utils'
import { arrTplData } from './Utils'
import { saveVisualField } from './../../../actions/IssueBatchAction'
import { cloneDeep } from 'lodash'

// const tempHeader = ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB',
//   'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id',
//   'Issuer VN', 'Recipient Name VN', 'DoB VN', 'Major VN', 'Graduation Grade VN', 'Mode of Study VN', 'Issuer',
//   'So', 'Ten khai sinh', 'Ngay sinh', 'Ngay sinh bang chu', 'Gioi tinh', 'Dan toc', 'Quoc tich', 'Noi sinh',
//   'Que quan', 'So dinh danh', 'Ho ten me', 'Nam sinh me', 'Dan toc me', 'Quoc tich me', 'Noi cu tru me',
//   'Ho ten bo', 'Nam sinh bo', 'Dan toc bo', 'Quoc tich bo', 'Noi cu tru bo', 'Ho ten nguoi di khai sinh',
//   'Giay to tuy than', 'Noi dang ky khai sinh', 'Ngay dang ky khai sinh',
// ]

class VisualTemplate extends React.Component {
  constructor(props) {
    super(props)
    const arrTpl = cloneDeep(arrTplData)
    this.state = {
      image: null,
      arrTpl,
      tplSeleted: arrTpl[0],
      field: [],
      selectOptions: this.convertHeaderToSelect(this.props.issueData.header),
      // selectOptions: this.convertHeaderToSelect(tempHeader),
      modal: false,
      imgHeight: 0,
      imgWidth: 0,
      isMove: false,
      mapFlag: false,
      mapCount: 0
    }
  }
  keepOnPage(e) {
    const message = 'Are you sure to leave?'
    e.returnValue = message
    return message
  }
  componentWillMount = async () => {
    // set data when come back
    if (this.props.issueData.title.trim() === '') {
      this.handleBack()
    }
    const { rawVisualField, visualTplId } = this.props.issueData
    await this.setState({
      field: rawVisualField,
      tplSeleted: cloneDeep(arrTplData)[visualTplId - 1]
    })
    const { arrTpl } = this.state
    arrTpl[visualTplId - 1].select = true
    this.setState({
      arrTpl,
    })
  }
  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.keepOnPage)
  }
  componentDidMount = () => {
    window.addEventListener('beforeunload', this.keepOnPage)
    const selectFieldHeight = this.refs.selectField.clientHeight
    this.setState({
      imgHeight: this.refs.image.height,
      imgWidth: this.refs.image.width,
      selectFieldScrollFlag: selectFieldHeight > 730
    })
  }
  handleBack = async () => {
    const { path } = this.props.match
    await this.setState({
      isMove: true
    })
    const newPath = path.substring(0, path.lastIndexOf('/'))
    this.props.history.push(`${newPath}/initInfor`)
  }
  handleNext = async () => {
    // convert field to data map
    const { field, tplSeleted } = this.state
    await this.setState({
      isMove: true
    })
    const map = {}
    map.visualTplId = tplSeleted.index
    for (let index = 0; index < field.length; index++) {
      const element = field[index]
      if (element) {
        map[`${index + 1}`] = element.value
      }
    }
    map.rawVisualField = field
    this.props.handleSaveVisualField(map)
    const { path } = this.props.match
    const newPath = path.substring(0, path.lastIndexOf('/'))
    this.props.history.push(`${newPath}/confirmData`)
  }
  displayTpl = (arr) => {
    let result = null
    if (arr.length > 0) {
      result = arr.map((ele, index) =>
        <img key={index} className={ele.select ? 'img-tpl-selected' : 'img-tpl'} src={ele.src} alt="img-tpl" onClick={() => this.selectTpl(index)} />
      )
    }
    return result
  }
  displaySelectField = (fields) => {
    let result = null
    const { field } = this.state
    if (fields && fields.length > 0) {
      result = fields.map((ele, index) => (
        <tr key={ele.index}>
          <th scope="row">{ele.index}</th>
          <td>
            {field[index] !== null ? (
              <Select
                isSearchable="true"
                isClearable="true"
                defaultValue={field[index]}
                onChange={value => this.handleChangeSelect(index, value)}
                options={this.state.selectOptions}
              />
            ) :
              (
                <Select
                  isSearchable="true"
                  isClearable="true"
                  // defaultValue={field[index]}
                  onChange={value => this.handleChangeSelect(index, value)}
                  options={this.state.selectOptions}
                />
              )}

          </td>
        </tr>)
      )
    }
    return result
  }
  selectTpl = async (index) => {
    const { arrTpl } = this.state
    arrTpl.forEach((arr) => {
      arr.select = false
    })
    arrTpl[index].select = true
    await this.setState({
      tplSeleted: {}, // reset
    })
    this.setState({
      tplSeleted: arrTpl[index],
      field: [],
      selectFieldScrollFlag: arrTpl[index].fields.length > 10,
      mapFlag: false,
    })
    // const selectFieldHeight = this.refs.selectField.clientHeight
    // this.setState({
    //   selectFieldScrollFlag: selectFieldHeight > 730,
    // })
  }
  resetTpl = () => {
    const { arrTpl } = this.state
    arrTpl.forEach((arr) => {
      arr.select = false
    })
    this.setState({
      arrTpl
    })
  }
  handleChangeSelect = (index, value) => {
    const { field } = this.state
    field[index] = value
    this.setState({
      field,
      mapFlag: false,
    })
  }
  convertHeaderToSelect = (arrHeader) => {
    const result = []
    arrHeader.forEach((element) => {
      const obj = {
        value: getContentPath(element),
        label: element,
      }
      result.push(obj)
    })
    return result
  }
  toggle = () => {
    const { field, modal, tplSeleted } = this.state
    const { header, data } = this.props.issueData
    if (!modal) {
      for (let i = 0; i < field.length; i++) {
        if (field[i] && field[i].label !== null) {
          for (let j = 0; j < header.length; j++) {
            if (field[i].label === header[j]) {
              field[i].data = data[0][j] ? data[0][j].toString() : '' // case null data
              tplSeleted.fields[i].data = data[0][j] ? data[0][j].toString() : '' // case null data
              break
            }
          }
        } else {
          delete tplSeleted.fields[i].data
        }
      }
      this.setState({
        modal: !this.state.modal,
        imgHeight: this.refs.image.height,
        imgWidth: this.refs.image.width,
      })
    } else {
      this.setState({
        modal: !this.state.modal,
      })
    }
  }
  mapFields = async () => {
    const { selectOptions, tplSeleted } = this.state
    const tplSeletedTemp = cloneDeep(tplSeleted)
    const temp = []
    for (let i = 0; i < selectOptions.length; i++) {
      const option = selectOptions[i].label.toLowerCase()
      for (let j = 0; j < tplSeleted.fields.length; j++) {
        const name = tplSeleted.fields[j].name.toLowerCase()
        if (name === option) {
          temp[j] = selectOptions[i]
        }
      }
    }
    await this.setState({
      field: temp,
      tplSeleted: {},
      mapFlag: true,
      mapCount: temp.filter(x => x).length
    })
    this.setState({
      tplSeleted: tplSeletedTemp
    })
  }
  render() {
    const { arrTpl, tplSeleted, imgWidth, imgHeight, selectFieldScrollFlag, isMove, mapFlag, mapCount } = this.state
    return (
      <div>
        <Prompt
          when={!isMove}
          message={() =>
            'You haven\'t finished your issue yet. Do you want to leave without finishing?'
          }
        />

        <Row>
          <Col xs={{ size: 12, offset: 0 }} style={{ padding: '0px 50px' }}>
            <Row>
              <Col md="12">
                {/* load fontFamily */}
                <span style={{ fontFamily: 'Dancing Script', visibility: 'hidden' }}>
                  ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯăâêôơư1234567890
                </span>
                <h5>Select visual template:</h5>
              </Col>
              <Col md="12" className="list-tpl" style={{ marginBottom: '20px' }}>
                {this.displayTpl(arrTpl)}
              </Col>
            </Row>
            <Row>
              <Col xl="3">
                <div className={selectFieldScrollFlag ? 'select-field-scroll' : 'select-field'} ref="selectField">
                  <Table style={{ marginBottom: '0px' }} >
                    <thead>
                      <tr>
                        <th width="20%">No.</th>
                        <th>Field Data</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.displaySelectField(tplSeleted.fields)}
                    </tbody>
                  </Table>
                </div>
                <div className="text-center" style={{ padding: '10px 0px' }}>
                  <Button color="primary" outline onClick={this.mapFields} style={{ margin: '5px 10px' }}>Auto-Mapping</Button>
                  <Button color="primary" outline onClick={this.toggle} style={{ width: '100px', margin: '5px 10px' }}>Preview</Button>
                </div>
                <div className="text-center">
                  {mapFlag && tplSeleted.fields &&
                    <p style={{ color: '#007BFF' }}>
                      {mapCount}/{tplSeleted.fields.length} fields have been mapped!
                    </p>}
                </div>
              </Col>
              <Col xl={{ size: 9 }} className="tpl-select">
                <img ref="image" src={tplSeleted.src} className="main-template" alt="main-template" />
              </Col>
            </Row>
          </Col>
          <Col xs="12" style={{ marginTop: '25px' }}>
            <div className="text-center">
              <Button color="warning" size="lg" type="submit" onClick={this.handleBack} style={{ width: '100px', marginRight: '20px' }}>
                Back
              </Button>
              <Button color="primary" size="lg" type="submit" onClick={this.handleNext} style={{ width: '100px' }}>
                Next
              </Button>
            </div>

          </Col>
        </Row>
        {/* style={{ maxWidth: `${imgWidth}px !important` }} */}
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal-preview" >
          <ModalHeader toggle={this.toggle}>Visual Template Preview</ModalHeader>
          <ModalBody className="modal-body-preview">
            <CanvasTpl src={tplSeleted.src} imgHeight={imgHeight} imgWidth={imgWidth} fields={tplSeleted.fields} />
          </ModalBody>
        </Modal>
        {/* {this.state.modal &&
          <CanvasTpl src={tplSeleted.src} imgHeight={this.refs.image.height} imgWidth={this.refs.image.width} />
        } */}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    issueData: state.issueBatchReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleSaveVisualField: fields => dispatch(saveVisualField(fields)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VisualTemplate)

