import React from 'react'

class CanvasTpl extends React.Component {
  constructor(props) {
    super(props)
    let { imgWidth, imgHeight } = this.props
    console.log('imgWidth', imgWidth)
    console.log('imgHeight', imgHeight)
    // set image width and ratio
    const ratio = imgWidth / 1260
    imgHeight = 1260 * (imgHeight / imgWidth)
    imgWidth = 1260
    this.state = {
      imgHeight,
      imgWidth,
      ratio
    }
  }

  componentDidMount() {
    const { imgWidth, imgHeight } = this.state
    const { fields } = this.props
    console.log(fields)
    const canvas = this.refs.canvas
    const ctx = canvas.getContext("2d")
    const imageObj = new Image()
    // edit image source
    let imgSrc = this.props.src
    const index = imgSrc.lastIndexOf('.')
    const str1 = imgSrc.substring(0, index)
    const str2 = imgSrc.substring(index)
    imgSrc = str1.concat('_blank', str2)
    imageObj.src = imgSrc


    imageObj.onload = function () {
      ctx.drawImage(imageObj, 0, 0, imgWidth, imgHeight)
      fields.forEach((ele) => {
        const align = ele.textAlign ? ele.textAlign : 'center'
        ctx.textAlign = align
        const y = (imgWidth * ele.left) / 100
        const x = (imgHeight * ele.top) / 100
        const fontFamily = ele.fontFamily ? ele.fontFamily : 'Arial'
        console.log(fontFamily)
        if (ele.fontWeight) {
          ctx.font = `${ele.fontWeight} ${ele.fontSize}pt ${fontFamily}`
        } else {
          ctx.font = `${ele.fontSize}px ${fontFamily}`
        }
        const data = ele.data ? ele.data : ''
        ctx.fillText(data, y, x)
      })
    }
  }
  render() {
    const { imgWidth, imgHeight } = this.state
    return (
      <div>
        <canvas ref="canvas" width={imgWidth} height={imgHeight} />
      </div>
    )
  }
}
export default CanvasTpl
