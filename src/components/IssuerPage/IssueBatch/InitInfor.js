import React, { Component } from 'react'
import { Row, Col, Input, Label, Button, Form, Alert } from 'reactstrap'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import isEqual from 'lodash/isEqual'
import readXlsxFile from 'read-excel-file'
import Select from 'react-select'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import { connect } from 'react-redux'
import { initializeInfor, createNewBatch, fetchData } from '../../../actions/IssueBatchAction'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`

class InitInfor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      description: '',
      tags: [],
      fileUpload: '',
      data: [],
      templateAll: '',
      validateFlag: true,
      template: {},
      errorMsg: '',
      useVisualFlag: true
    }
  }
  componentWillMount() {
    // set data when come back
    const { issueData } = this.props
    this.setState({
      title: issueData.title,
      description: issueData.description,
      tags: issueData.tags,
      template: issueData.template,
      templateAll: issueData.templateAll
    })
  }

  componentDidMount() {
    // get API data
    this.props.fetchData()
  }
  handleChangeTag = (tags) => {
    // remove duplicate value in arr
    const uniArr = [...(new Set(tags))]
    this.setState({
      tags: uniArr,
    })
  }
  handleChangeSelect = (template) => {
    this.setState({ template })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      validateFlag: true,
    })
  }
  handleSubmit = async (event) => {
    event.preventDefault()
    if (this.state.title.trim() === '') {
      this.setState({
        validateFlag: false,
        errorMsg: 'Batch title must not be empty.',
      })
      return
    }
    await this.props.handleCreateNewBatch(this.state)
    // const templateHeader = this.getTemplateData(this.state.template);
    const { issueData } = this.props
    // check exist title
    if (issueData.titleFlag) {
      // check template vs excel file
      const templateHeader = this.state.template.value
      const { header, data } = this.state
      if (typeof (templateHeader) === 'undefined' || typeof (header) === 'undefined') {
        console.log('err 1')
        this.setState({
          validateFlag: false,
          errorMsg: "Template and Excel file does not match!",
        })
      } else if (isEqual([...templateHeader].sort(), [...header].sort())) {
        if (data.length > 0) {
          this.props.handleInitialize(this.state)
          const { path } = this.props.match
          const newPath = path.substring(0, path.lastIndexOf('/'))
          if (this.state.useVisualFlag) {
            this.props.history.push(`${newPath}/visualTemplate`)
          } else {
            this.props.history.push(`${newPath}/confirmData`)
          }
        } else {
          this.setState({
            validateFlag: false,
            errorMsg: "Excel file does not have any data.",
          })
        }
      } else {
        console.log('err 2')
        console.log([...templateHeader].sort())
        console.log([...header].sort())
        this.setState({
          validateFlag: false,
          errorMsg: "Template and Excel file does not match.",
        })
      }
    } else {
      this.setState({
        validateFlag: false,
        errorMsg: issueData.titleError,
      })
    }
  }
  handleUpload = (event) => {
    if (event.target.files.length > 0) {
      const value = event.target.files[0].name
      readXlsxFile(event.target.files[0]).then((rows) => {
        this.setState({
          header: rows.shift(),
          data: rows,
        })
      })
      this.setState({
        fileUpload: value,
        validateFlag: true,
      })
    }
  }
  render() {
    const { validateFlag, errorMsg, useVisualFlag } = this.state
    const { issueData } = this.props
    return (
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          {
            issueData.isFetching ? (
              <BeatLoader
                className={override}
                sizeUnit={'px'}
                size={15}
                color={'#36D7B7'}
              />) : (
                <Form onSubmit={this.handleSubmit} >
                  {
                    validateFlag ? null : <Alert color="danger">
                      {errorMsg}
                    </Alert>
                  }
                  <Label>Title</Label>
                  <Input
                    type="text"
                    name="title"
                    value={this.state.title}
                    id="title"
                    placeholder="Enter title"
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                  <br />
                  <Label>Select Template</Label>
                  <Select
                    value={this.state.template}
                    onChange={this.handleChangeSelect}
                    options={issueData.templateAll}
                  />
                  <br />
                  <Label>Description - <i>Optional</i></Label>
                  <Input
                    value={this.state.description}
                    type="textarea"
                    name="description"
                    onChange={this.handleInputChange}
                    rows="5"
                    maxLength="500"
                    style={{ resize: 'none' }}
                  />
                  <br />
                  <Label>Tags - <i>Optional</i></Label>
                  <TagsInput value={this.state.tags} onChange={this.handleChangeTag} />
                  <br />
                  <Label>Select Excel file:</Label>
                  <div className="input-group">
                    <label className="input-group-btn">
                      <span className="btn btn-primary">
                        Browse&hellip;
                                    <input
                          type="file"
                          style={{ display: 'none' }}
                          accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                          onChange={this.handleUpload}
                        />
                      </span>
                    </label>
                    {this.state.fileUpload}
                  </div>
                  <br />
                  <div className="pretty p-svg p-curve">
                    <input type="checkbox" onChange={this.handleInputChange} name="useVisualFlag" checked={useVisualFlag} />
                    <div className="state p-primary ">
                      <svg className="svg svg-icon" viewBox="0 0 20 20">
                        <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style={{ stroke: 'white', fill: 'white' }} />
                      </svg>
                      <label>Design and send certificate images to recipients</label>
                    </div>
                  </div>
                  <div className="text-center">
                    <br />
                    <Button color="primary" size="lg" type="submit">
                      Next
                    </Button>
                  </div>
                </Form>
              )
          }
        </Col>
      </Row>
    )
  }
}

function mapStateToProps(state) {
  return {
    issueData: state.issueBatchReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleInitialize: data => dispatch(initializeInfor(data)),
    fetchData: () => dispatch(fetchData()),
    handleCreateNewBatch: data => dispatch(createNewBatch(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InitInfor)
