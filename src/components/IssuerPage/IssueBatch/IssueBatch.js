import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'react-tagsinput/react-tagsinput.css'
import { Route, Switch, Redirect } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import StepConnector from '@material-ui/core/StepConnector'
import { setActiveStep } from './../../../actions/IssueBatchAction'
import InitInfor from '../../../components/IssuerPage/IssueBatch/InitInfor'

import jwt from 'jsonwebtoken'
import Cookies from 'universal-cookie'
import ConfirmData from './ConfirmData'
import VisualTemplate from './VisualTemplate'

const styles = theme => ({
  root: {
    width: '90%',
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  connectorActive: {
    '& $connectorLine': {
      borderColor: theme.palette.secondary.main,
    },
  },
  connectorCompleted: {
    '& $connectorLine': {
      borderColor: theme.palette.primary.main,
    },
  },
  connectorDisabled: {
    '& $connectorLine': {
      borderColor: theme.palette.grey[100],
    },
  },
  connectorLine: {
    transition: theme.transitions.create('border-color'),
  },
})

function getSteps() {
  return ['Input Batch Information', 'Select Visual Template', 'Confirm Certificate Data']
}


class IssueBatch extends Component {
  componentDidUpdate = (prevProps) => {
    if (this.props.location !== prevProps.location) {
      const { pathname } = this.props.location
      if (pathname === '/issuer/batch/initInfor') {
        this.props.setActiveStep(0)
      } else if (pathname === '/issuer/batch/visualTemplate') {
        this.props.setActiveStep(1)
      } else if (pathname === '/issuer/batch/confirmData') {
        this.props.setActiveStep(2)
      }
    }
  }
  render() {
    const { match } = this.props
    const { path } = match
    const { classes } = this.props
    const { activeStep, useVisualFlag } = this.props.issueData
    // const {issueFlag} = this.props.issueData;
    const steps = getSteps()

    const connector = (
      <StepConnector
        classes={{
          active: classes.connectorActive,
          completed: classes.connectorCompleted,
          disabled: classes.connectorDisabled,
          line: classes.connectorLine,
        }}
      />
    )
    const cookies = new Cookies()
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    return (
      <div>
        {token.issuableStatus ? (
          <div>
            <div className={classes.root}>
              <Stepper alternativeLabel activeStep={activeStep} connector={connector}>
                {steps.map((label, index) => {
                  const props = {}
                  const labelProps = {}
                  if (index === 1 && !useVisualFlag) {
                    props.completed = false
                  }
                  return (
                    <Step key={label} {...props}>
                      <StepLabel {...labelProps}>{label}</StepLabel>
                    </Step>
                  )
                })}
              </Stepper>
            </div>
            <div>
              {/* {this.props.issueData.issueFlag ? this.props.setActiveStep(3) : null} */}
              <Switch>
                <Route path={`${path}/initInfor`} component={InitInfor} />
                <Route path={`${path}/visualTemplate`} component={VisualTemplate} />
                <Route path={`${path}/confirmData`} component={ConfirmData} />
                <Redirect from={`${path}`} to={`${path}/initInfor`} />
              </Switch>
            </div>
          </div>
        ) :
          (
            <div style={{ width: '100%' }} className="text-center">
              <br />
              <p>Your issue feature is temporarily disabled by CertNet.</p>

            </div>
          )}

      </div>
    )
  }
}


// export default withStyles(styles)(IssueBatch)
function mapStateToProps(state) {
  return {
    issueData: state.issueBatchReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setActiveStep: step => dispatch(setActiveStep(step)),

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(IssueBatch))
