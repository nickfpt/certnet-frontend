import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Alert, Progress } from 'reactstrap'
import * as callAPI from '../../../constants/CallAPI'
import * as endpoints from '../../../constants/endpoints'
import { NETWORK } from '../../../constants/common'
import { web3, appendPrefix } from '../../../services/web3'
import Cookies from "universal-cookie"
import jwt from "jsonwebtoken";
import Certnet from '../../../contract/certnet'

const cookies = new Cookies()

class ConfirmModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      alert: false,
      error: '',
      web3Alert: false,
      web3Error: '',
      web3CurrentWallet: null,
      disableIssue: false,
      disableClose: false,
      message: null,
      loader: false,
      issuerETHaddresses: [],
      progress: 0
    }
  }

  async componentDidMount() {
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    this.setState({
      issuerETHaddresses: token.ethereumAccounts
    })
    if (typeof web3 !== 'undefined') {
      this.setState({ web3Checked: true, web3Supported: true })
      const checkAccountChange = async () => {
        const network = await web3.eth.net.getNetworkType()
        if (network !== NETWORK.RINKENY) {
          this.setState({
            web3Alert: true,
            web3Error: 'Please switch to Rinkeby network.',
          })
        } else {
          const accounts = await web3.eth.getAccounts()
          if (accounts.length > 0) {
            if (this.state.issuerETHaddresses.includes(accounts[0])) {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: false,
              })
              if (!this.state.message) {
                this.setState({
                  message: `Are you sure to issue batch by address ${this.state.web3CurrentWallet} ?`
                })
              }
            } else {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: true,
                web3Error: 'Please switch to Ethereum address you registered with this account.',
              })
            }
          } else {
            this.setState({
              web3Alert: true,
              web3Error: 'Please login to MetaMask.',
            })
          }
        }
      }

      await checkAccountChange()
      if (!this.state.alert && !this.state.web3Alert) {
        this.setState({
          message: `Are you sure to issue batch by address ${this.state.web3CurrentWallet} ?`
        })
      }
      this.checkChangeWalletInterval = setInterval(checkAccountChange, 1000)
    } else {
      this.setState({ web3Alert: true, web3Error: 'MetaMask is not installed.' })
    }
  }

  async issue() {
    const { issueData: { batchId }, createUnsignCert, signBatch, updatedData, handleReset } = this.props
    const { web3CurrentWallet: sender } = this.state
    const issueData = this.props.issueData
    await this.setState({
      loader: true,
      disableClose: true,
      disableIssue: true,
      message: 'Certificate issuance in progress. Please do not close the dialog or browser!',
    })
    const convertDataToSend = []
    updatedData.forEach((element) => {
      const temp = Object.values(element)
      convertDataToSend.push(temp)
    })
    let response = await createUnsignCert(issueData, convertDataToSend)
    if (response.ok) {
      response = await callAPI.fetch(endpoints.buildMerkleTree(batchId))
      if (response && response.ok) {
        const { merkleTreeRoot } = response
        if (sender) {
          try {
            let intervalId
            const receipt = await Certnet.methods.issueBatch(appendPrefix(merkleTreeRoot)).send({
              from: sender
            }, (e, processedContract) => {
              if (processedContract) {
                let time = 0
                intervalId = setInterval(() => {
                  if (time <= 30) {
                    time += 1
                    this.setState({
                      progress: time * 100 / 35
                    })
                  }
                }, 1000)
              }
            })
            const transactionId = receipt.transactionHash
            let signBatchResponse = {}
            if (issueData.useVisualFlag) {
              signBatchResponse = await signBatch({ batchId, transactionId, sender, map: issueData.visualFields, visualTemplateId: issueData.visualTplId }, true)
            } else {
              signBatchResponse = await signBatch({ batchId, transactionId, sender }, false)
            }
            if (signBatchResponse.ok) {
              this.setState({
                progress: 100
              })
              clearInterval(intervalId)
              setTimeout(() => {
                this.setState({
                  loader: false,
                  disableClose: false,
                  message: 'Certificates has been issued successfully!',
                })
              }, 1000)
              handleReset()
            } else {
              this.setState({
                alert: true,
                disableClose: false,
                error: JSON.stringify(signBatchResponse.error),
              })
            }
          } catch (err) {
            if (err.message.includes('User denied transaction signature')) {
              this.setState({
                alert: true,
                disableClose: false,
                disableIssue: false,
                error: 'Issuance process has been cancelled.',
              })
            }
          }
        } else {
          this.setState({
            web3Alert: true,
            disableClose: false,
            web3Error: 'Please login to MetaMask',
          })
        }
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          error: JSON.stringify(response.data.error),
        })
      }
    } else {
      this.setState({
        alert: true,
        disableClose: false,
        error: 'Unexpected error happened when creating unsigned certificates.',
      })
    }
  }

  render() {
    const { modal, toggle, path, history } = this.props
    const { alert, error, disableIssue, message, loader, disableClose, web3Alert, web3Error, progress } = this.state
    return (<div>

      <Modal isOpen={modal} style={{ minWidth: '900px' }} className="text-center">
        <ModalHeader>Confirm Issue Certificates</ModalHeader>
        <ModalBody>
          {alert && <Alert color="danger">
            {error}
          </Alert>}
          {web3Alert && <Alert color="danger">
            {web3Error}
          </Alert>}
          {!alert && (<strong>{message}</strong>)}
          {!alert && loader &&
            <Progress striped value={progress} style={{ marginTop: 20 }} />}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" size="lg" disabled={disableClose} onClick={() => {
            toggle()
            if (progress === 100) {
              const newPath = path.substring(0, path.indexOf('/'))
              history.push(`${newPath}/issuer/manage-cert`)
            }
          }}>Close</Button>{' '}
          {!alert && !web3Alert && <Button color="primary" size="lg" disabled={disableIssue} onClick={() => this.issue()}>Issue</Button>}
        </ModalFooter>
      </Modal>
    </div>)
  }
}

ConfirmModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  signBatch: PropTypes.func.isRequired,
  createUnsignCert: PropTypes.func.isRequired,
  issueData: PropTypes.shape().isRequired,
  updatedData: PropTypes.array.isRequired,
  handleReset: PropTypes.func.isRequired,

}

export default ConfirmModal
