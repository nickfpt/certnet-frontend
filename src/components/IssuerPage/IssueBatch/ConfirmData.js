/* eslint-disable class-methods-use-this,react/sort-comp */
import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { Prompt } from 'react-router-dom'
import TagsInput from 'react-tagsinput'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { makeData } from './Utils'
import ConfirmModal from './ConfirmModal'
import { createUnsignedCert, signBatch, resetState } from '../../../actions/IssueBatchAction'
import { setPageSizeOption } from './../../../Utils'

class ConfirmData extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      initData: [],
      modalBack: false,
      modalSubmit: false,
    }
    this.renderEditable = this.renderEditable.bind(this)
  }
  createTableHeader(arrHeader) {
    const tableHeader = []
    for (let index = 0; index < arrHeader.length; index += 1) {
      const obj = {
        Header: arrHeader[index],
        accessor: `header${index}`,
        Cell: this.renderEditable,
      }
      tableHeader.push(obj)
    }
    return tableHeader
  }
  // When quit tab or browser
  keepOnPage(e) {
    const message = 'Warning!\n\nNavigating away from this page will delete your data if you haven\'t already saved it.'
    e.returnValue = message
    return message
  }
  componentWillMount() {
    if (this.props.issueData.title.trim() === '') {
      this.handleBack()
    }
  }
  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.keepOnPage)
  }
  componentDidMount() {
    window.addEventListener('beforeunload', this.keepOnPage)
    const dataRows = this.props.issueData.data
    this.setState({
      initData: makeData(dataRows),
    })
  }
  toggleBtnBack = () => {
    this.setState({
      modalBack: !this.state.modalBack,
    })
  }
  toggleBtnSubmit = () => {
    this.setState({
      modalSubmit: !this.state.modalSubmit,
    })
  }
  renderEditable(cellInfo) {
    return (
      <div
        style={{ backgroundColor: '#fafafa' }}
        contentEditable
        suppressContentEditableWarning
        onBlur={(e) => {
          const initData = [...this.state.initData]
          initData[cellInfo.index][cellInfo.column.id] = e.target.innerHTML.replace(/&nbsp;/g, '').trim()
          this.setState({ initData })
        }}
        dangerouslySetInnerHTML={{
          __html: this.state.initData[cellInfo.index][cellInfo.column.id],
        }}
      />
    )
  }
  handleBack = () => {
    const { path } = this.props.match
    const newPath = path.substring(0, path.lastIndexOf('/'))
    const { useVisualFlag } = this.props.issueData
    if (useVisualFlag) {
      this.props.history.push(`${newPath}/visualTemplate`)
    } else {
      this.props.history.push(`${newPath}/initInfor`)
    }
  }
  render() {
    const { initData } = this.state
    const { header, title, tags, description, template } = this.props.issueData
    const tableHeader = this.createTableHeader(header)
    return (
      <div>
        <Prompt
          when={!this.state.modalBack && !this.state.modalSubmit}
          message={() =>
            'You haven\'t finished your issue yet. Do you want to leave without finishing?'
          }
        />

        <Row>
          <Col xs="12" sm="6" md="6" lg="3" xl="3" style={{ marginBottom: '15px' }}><b>Title: </b>{title}</Col>
          <Col xs="12" sm="6" md="6" lg="3" xl="3" style={{ marginBottom: '15px' }}><b>Template: </b>{template.label}</Col>
          <Col xs="12" sm="6" md="6" lg="3" xl="3" style={{ marginBottom: '15px' }}><b>Description: </b>{description}</Col>
          <Col xs="12" sm="6" md="6" lg="3" xl="3" style={{ marginBottom: '15px' }}>
            <Row>
              <Col xs="2">
                <b>Tags:</b>
              </Col>
              <Col xs="10">
                <TagsInput
                  className=""
                  value={tags}
                  onChange={this.handleChange}
                  disabled
                  inputProps={{ placeholder: '' }}
                />
              </Col>
            </Row>

          </Col>

        </Row>

        <ReactTable
          data={initData}
          columns={tableHeader}
          defaultPageSize={10}
          className="-striped -highlight"
          pageSizeOptions={setPageSizeOption(initData.length)}
        />
        <br />
        <Modal isOpen={this.state.modalBack} toggle={this.toggleBtnBack} centered>
          <ModalHeader toggle={this.toggleBtnBack}>Want to back?</ModalHeader>
          <ModalBody>
            Your data will be discarded!
          </ModalBody>
          <ModalFooter>
            <Button color="warning" size="lg" onClick={this.handleBack}>Back</Button>{' '}
            <Button color="secondary" size="lg" onClick={this.toggleBtnBack}>Cancel</Button>
          </ModalFooter>
        </Modal>
        {this.state.modalSubmit && <ConfirmModal
          path={this.props.match.path}
          history={this.props.history}
          toggle={this.toggleBtnSubmit}
          modal={this.state.modalSubmit}
          issueData={this.props.issueData}
          createUnsignCert={this.props.createUnsignedCert}
          signBatch={this.props.signBatch}
          updatedData={initData}
          handleReset={this.props.handleReset}
        />}
        <div className="text-center">
          <Button color="warning" size="lg" type="submit" onClick={this.toggleBtnBack} style={{ width: '100px', marginRight: '20px' }}>
            Back
          </Button>
          <Button color="primary" size="lg" type="submit" onClick={this.toggleBtnSubmit} style={{ width: '100px' }}>
            Submit
          </Button>
        </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    issueData: state.issueBatchReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    createUnsignedCert: (batchInfo, updatedData) => dispatch(createUnsignedCert(batchInfo, updatedData)),
    signBatch: (data, flag) => dispatch(signBatch(data, flag)),
    handleReset: () => dispatch(resetState()),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmData)

