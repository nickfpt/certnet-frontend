import React from 'react'
// import namor from "namor";
// import "./index.css";

const range = (len) => {
  const arr = []
  for (let i = 0; i < len; i += 1) {
    arr.push(i)
  }
  return arr
}

const newDataRow = (arr) => {
  // const statusChance = Math.random();
  const dataRow = {}
  for (let index = 0; index < arr.length; index += 1) {
    dataRow[`header${index}`] = arr[index]
  }
  return dataRow
}

export function makeData(arr) {
  return range(arr.length).map(d => ({
    ...newDataRow(arr[d]),
    //   children: range(10).map(newPerson)
  }))
}


export const Tips = () =>
  (<div style={{ textAlign: 'center' }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>)

export const arrTplData =
  [
    {
      index: 1,
      src: 'https://storage.googleapis.com/certnet/visual_templates/temp_1.png',
      select: false,
      fields: [
        {
          index: 1,
          top: 42,
          left: 50,
          fontSize: 56,
          fontFamily: 'Dancing Script',
          name: 'Recipient Name'
        },
        {
          index: 2,
          top: 55,
          left: 50,
          fontSize: 28,
          name: 'Certificate name'
        },
        {
          index: 3,
          top: 75,
          left: 50,
          fontSize: 32,
          name: 'Cert Id2'
        }
      ]
    },
    {
      index: 2,
      src: 'https://storage.googleapis.com/certnet/visual_templates/temp_2.png',
      select: false,
      fields: [
        {
          index: 1,
          top: 18,
          left: 75.5,
          fontSize: 24,
          fontWeight: 'bold',
          name: 'Nha phat hanh'
        },
        {
          index: 2,
          top: 37,
          left: 75,
          fontSize: 18,
          fontWeight: 'bold',
          name: 'Chuyen nganh'
        },
        {
          index: 3,
          top: 52,
          left: 75,
          fontSize: 28,
          fontWeight: 'bold',
          fontFamily: 'Dancing Script',
          name: 'Ten nguoi nhan'
        },
        {
          index: 4,
          top: 55.2,
          left: 72,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Ngay sinh'
        },
        {
          index: 5,
          top: 59.5,
          left: 72,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Graduation Year'
        },
        {
          index: 6,
          top: 63.75,
          left: 72,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Xep loai tot nghiep'
        },
        {
          index: 7,
          top: 68,
          left: 72,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Hinh thuc dao tao'
        },
        {
          index: 8,
          top: 90,
          left: 63,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Cert Id'
        },
        {
          index: 9,
          top: 93.5,
          left: 71,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Decision Id'
        },
        {
          index: 10,
          top: 18,
          left: 27,
          fontSize: 24,
          fontWeight: 'bold',
          name: 'Issuer'
        },
        {
          index: 11,
          top: 37,
          left: 27.5,
          fontSize: 18,
          fontWeight: 'bold',
          name: 'Major'
        },
        {
          index: 12,
          top: 52,
          left: 29,
          fontSize: 28,
          fontWeight: 'bold',
          fontFamily: 'Dancing Script',
          name: 'Recipient Name'
        },
        {
          index: 13,
          top: 55.2,
          left: 22,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'DoB'
        },
        {
          index: 14,
          top: 59.5,
          left: 22,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Graduation Year'
        },
        {
          index: 15,
          top: 63.75,
          left: 22,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Graduation Grade'
        },
        {
          index: 16,
          top: 68,
          left: 22,
          fontSize: 16,
          fontWeight: 'bold',
          textAlign: 'left',
          name: 'Mode of Study'
        },
        {
          index: 17,
          top: 93.5,
          left: 15,
          fontSize: 16,
          textAlign: 'left',
          fontWeight: 'bold',
          name: 'Cert Id'
        }
      ]
    },
    {
      index: 3,
      src: 'https://storage.googleapis.com/certnet/visual_templates/temp_3.png',
      select: false,
      fields: [
        {
          index: 1,
          top: 9.5,
          left: 81,
          fontSize: 28,
          textAlign: 'left',
          name: 'So'
        },
        {
          index: 2,
          top: 24.7,
          left: 53,
          fontSize: 32,
          fontWeight: 'bold',
          name: 'Ten khai sinh'
        },
        {
          index: 3,
          top: 30,
          left: 35,
          fontSize: 28,
          textAlign: 'left',
          name: 'Ngay sinh'
        },
        {
          index: 4,
          top: 32.5,
          left: 53,
          fontSize: 28,
          name: 'Ngay sinh bang chu'
        },
        {
          index: 5,
          top: 35,
          left: 21,
          fontSize: 28,
          textAlign: 'left',
          name: 'Gioi tinh'
        },
        {
          index: 6,
          top: 35,
          left: 41,
          fontSize: 28,
          textAlign: 'left',
          name: 'Dan toc'
        },
        {
          index: 7,
          top: 35,
          left: 72,
          fontSize: 28,
          textAlign: 'left',
          name: 'Quoc tich'
        },
        {
          index: 8,
          top: 37.5,
          left: 52,
          fontSize: 28,
          name: 'Noi sinh'
        },
        {
          index: 9,
          top: 42.5,
          left: 52,
          fontSize: 28,
          name: 'Que quan'
        },
        {
          index: 10,
          top: 50.25,
          left: 42,
          fontSize: 28,
          textAlign: 'left',
          name: 'Ho ten me'
        },
        {
          index: 11,
          top: 53,
          left: 21.5,
          fontSize: 28,
          textAlign: 'left',
          name: 'Nam sinh me'
        },
        {
          index: 12,
          top: 53,
          left: 42,
          fontSize: 28,
          textAlign: 'left',
          name: 'Dan toc me'
        },
        {
          index: 13,
          top: 53,
          left: 73,
          fontSize: 28,
          textAlign: 'left',
          name: 'Quoc tich me'
        },
        {
          index: 14,
          top: 55.5,
          left: 52,
          fontSize: 28,
          name: 'Noi cu tru me'
        },
        {
          index: 15,
          top: 60.5,
          left: 42,
          fontSize: 28,
          textAlign: 'left',
          name: 'Ho ten bo'
        },
        {
          index: 16,
          top: 63,
          left: 21.5,
          fontSize: 28,
          textAlign: 'left',
          name: 'Nam sinh bo'
        },
        {
          index: 17,
          top: 63,
          left: 42,
          fontSize: 28,
          textAlign: 'left',
          name: 'Dan toc bo'
        },
        {
          index: 18,
          top: 63,
          left: 73,
          fontSize: 28,
          textAlign: 'left',
          name: 'Quoc tich bo'
        },
        {
          index: 19,
          top: 65.5,
          left: 52,
          fontSize: 28,
          name: 'Noi cu tru bo'
        },
        {
          index: 20,
          top: 70.6,
          left: 50,
          fontSize: 28,
          textAlign: 'left',
          name: 'Ho ten nguoi di khai sinh'
        },
        {
          index: 21,
          top: 73.2,
          left: 52,
          fontSize: 28,
          name: 'Giay to tuy than'
        },
        {
          index: 22,
          top: 75.7,
          left: 40,
          fontSize: 28,
          textAlign: 'left',
          name: 'Noi dang ky khai sinh'
        },
        {
          index: 23,
          top: 81,
          left: 39,
          fontSize: 28,
          textAlign: 'left',
          name: 'Ngay dang ky khai sinh'
        }
      ]
    }
  ]
