import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { Row, Col, Label, Form, Input, Button, Alert, Modal, ModalHeader, ModalBody, ModalFooter, InputGroupAddon, InputGroup } from 'reactstrap'
import styled from 'styled-components'
import Icon from '../../form/icon'
import {
  // suggestedFieldDefault,
  requiredFieldDefault, setListItemsDefault, addCloseBtn,
  setListItemsBachelorDefault, setListItemsCourseDefault, setListItemsBirthCert
} from './Data'
import { getContentPath } from './../../../Utils'
import Chkboxlist from './Chkboxlist'
import { connect } from 'react-redux'
import { createNewTemplate } from '../../../actions/ManageTemplateAction'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const ErrMsgTag = styled.p`
    color: red
`
const SuccessMsgTag = styled.p`
color: #007BFF
`
const CloseButton = styled.div.attrs({
  className: 'd-flex justify-content-center align-items-center',
})`
width: 20px;
height: 20px;
margin-left: 100%;
margin-top: -20px;
cursor: pointer;
`

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
const grid = 4
const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: grid,
  margin: `0 0 ${grid}px 0`,
  // change background colour if dragging
  background: isDragging ? 'white' : 'white',
  border: '1px solid #341f97',
  borderRadius: '7px',
  // styles we need to apply on draggables
  ...draggableStyle,
})

const getListStyle = isDraggingOver => ({
  // background: isDraggingOver ? 'lightblue' : 'lightgrey',
  background: 'none',
  border: '1px solid #341f97',
  borderRadius: '7px 0px 0px 7px',
  padding: grid * 2,
  width: '100%',
  marginTop: '10px',
  overflowX: 'hidden',
  overflowY: 'auto',
  height: '500px',
})

class CreateNewTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // items: [{ id: "Email", content: "Email" }, { id: "Ethereum Address", content: "Ethereum Address" }],
      items: setListItemsDefault(),
      customFieldDefault: [],
      customField: [],
      // suggestedFieldDefault,
      requiredFieldDefault,
      customFieldName: '',
      tempalteName: '',
      templateDescription: '',
      errMsgField: '',
      errMsgFieldFlag: false,
      errMsgName: '',
      errMsgNameFlag: false,
      successMsg: '',
      successMsgFlag: false,
      createMsg: '',
      createFlag: false,
      createResultFlag: false,
      templateData: this.props.templateData,
      recommendFieldFlag: false,
      recommendOption1: '0',
      recommendOption2: '1'
    }
    this.onDragEnd = this.onDragEnd.bind(this)
  }
  checkKeyExist = (listItem, item) => {
    for (let index = 0; index < listItem.length; index += 1) {
      if (listItem[index].id.toLowerCase().replace(/\s/g, '') === item.toLowerCase().replace(/\s/g, '')) {
        return index
      }
    }
    return -1
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return
    }

    const items = reorder(
      this.state.items,
      result.source.index,
      result.destination.index,
    )

    this.setState({
      items,
    })
  }
  /** input change */
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      errMsgFieldFlag: false,
      errMsgNameFlag: false,
      successMsgFlag: false,
      createFlag: false,
    })
  }
  /** Enter custom field */
  handleSubmit = (event) => {
    event.preventDefault()
    const cstfield = this.state.customFieldName
    const curList = this.state.items
    // check empty
    const customFieldName = cstfield.trim().replace(/\s\s+/g, ' ')
    // check exist item
    if (this.checkKeyExist(curList, customFieldName) === -1) {
      const temp = {
        id: customFieldName,
        content: customFieldName,
      }
      curList.push(temp)

      this.setState({
        items: curList,
        customFieldDefault: [...this.state.customFieldDefault, customFieldName],
        // suggestedFieldDefault: curSuggestList,
        customFieldName: '',
        successMsg: `Field name "${customFieldName}" has been added!`,
        successMsgFlag: true,
      })
    } else {
      this.setState({
        errMsgField: `Field name "${customFieldName}" already exists.`,
        errMsgFieldFlag: true,
      })
    }
  }
  /** remove when close item */
  removeItem = (itemContent) => {
    const curList = this.state.items
    const i = this.checkKeyExist(this.state.items, itemContent)
    if (i !== -1) {
      curList.splice(i, 1)
    }
    this.setState({
      items: curList,
    })
  }
  onCreateNewTemplate = async (event) => {
    event.preventDefault()
    const { tempalteName, templateData, items, templateDescription } = this.state
    // check exist template's name
    for (let index = 0; index < templateData.length; index += 1) {
      if (tempalteName.trim() === templateData[index].name) {
        this.setState({
          errMsgName: `Template name "${tempalteName}" already exists.`,
          errMsgNameFlag: true,
        })
        return
      }
    }
    const content = {}
    // set data to post
    for (let index = 0; index < items.length; index += 1) {
      const key = items[index].id
      const value = getContentPath(items[index].id)
      content[key] = value
    }
    const dataPost = {
      template: {
        content,
        name: tempalteName,
        description: templateDescription,
      },
    }
    await this.props.handleCreateNewTemplate(dataPost)
    const { manageTemp } = this.props
    let msg = ''
    if (manageTemp.resultCreatNewTemplate) {
      msg = `Template ${tempalteName} has been created!`
    } else {
      msg = manageTemp.errMsgCreatNewTemplate
    }
    this.setState({
      createFlag: true,
      createMsg: msg,
      createResultFlag: manageTemp.resultCreatNewTemplate,
    })
  }
  toggleRecommend = () => {
    this.setState({
      recommendFieldFlag: !this.state.recommendFieldFlag
    })
  }
  handleChange = (event) => {
    this.setState({ value: event.target.value })
  };
  handleRecommend = () => {
    const { recommendOption1, recommendOption2 } = this.state
    switch (recommendOption1) {
      case '0':
        if (recommendOption2 === '1') {
          this.setState({
            items: setListItemsBachelorDefault(),
          })
        } else {
          this.setState({
            items: setListItemsBachelorDefault(false),
          })
        }
        break
      case '1':
        if (recommendOption2 === '1') {
          this.setState({
            items: setListItemsCourseDefault(),
          })
        } else {
          this.setState({
            items: setListItemsCourseDefault(false),
          })
        }
        break
      case '2':
        if (recommendOption2 === '1') {
          this.setState({
            items: setListItemsBirthCert(),
          })
        } else {
          this.setState({
            items: setListItemsBirthCert(false),
          })
        }
        break
      default:
    }
    this.toggleRecommend()
  }
  handleBack = () => {
    this.props.onBack()
  }
  render() {
    const { requiredFieldDefault, createFlag, createMsg, createResultFlag } = this.state
    return (
      <div>
        <Row>
          <Col xs="12" >
            <Button className="float-left" color="primary" size="md" onClick={this.handleBack}>
              <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
              Back
              </Button>
            <p className="recommend-field">If you don't have any idea about template, <label onClick={this.toggleRecommend}>try it!</label> </p>
          </Col>
          <Col xs="12" lg={{ size: 8, offset: 2 }} >
            <Row>
              <Col sm="12">
                {createFlag ? (<Alert color={createResultFlag ? 'primary' : 'danger'} >
                  {createMsg}
                </Alert>) : null}
              </Col>
              <Col xs="12" lg="8">
                <Form id="initInfor" onSubmit={this.onCreateNewTemplate}>
                  <Label>Template Name</Label>
                  <Input
                    type="text"
                    name="tempalteName"
                    value={this.state.tempalteName}
                    id="title"
                    placeholder="Enter template's name"
                    onChange={this.handleInputChange}
                    autoComplete="off"
                    required
                  />
                  {this.state.errMsgNameFlag ? (<ErrMsgTag color="danger">
                    {this.state.errMsgName}
                  </ErrMsgTag>) : null}
                  <br />
                  <Label>Description</Label>
                  <Input
                    value={this.state.templateDescription}
                    placeholder="Enter template's description"
                    type="textarea"
                    name="templateDescription"
                    onChange={this.handleInputChange}
                    maxLength="500"
                    rows="5"
                    style={{ resize: 'none' }}
                  />
                </Form>
                <br />
                <Label>Required Fields</Label>
                <Chkboxlist
                  onChange={values => this.onChange('requiredField', values)}
                  values={requiredFieldDefault}
                  disableFlag
                />
                <Form className="form" onSubmit={this.handleSubmit}>
                  <br />
                  <Label>Custom Fields</Label>
                  <InputGroup>
                    <Input
                      name="customFieldName" autoComplete="off" pattern="[a-zA-Z0-9\s]+" type="text"
                      placeholder="Enter custom field name (only letters and numbers are allowed)"
                      title="Only letters and numbers are allowed!"
                      value={this.state.customFieldName}
                      onChange={this.handleInputChange}
                      required
                    />
                    <InputGroupAddon addonType="append">
                      <Button color="primary" style={{ width: '100px' }}>Add</Button>
                    </InputGroupAddon>
                  </InputGroup>
                  {this.state.errMsgFieldFlag ? (<ErrMsgTag >
                    {this.state.errMsgField}
                  </ErrMsgTag>) : null}
                  {this.state.successMsgFlag ? (<SuccessMsgTag >
                    {this.state.successMsg}
                  </SuccessMsgTag>) : null}
                  <br />
                </Form>

              </Col>
              <Col xs="12" lg="4" >
                <Label>Current Fields</Label>
                <DragDropContext onDragEnd={this.onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        style={getListStyle(snapshot.isDraggingOver)}
                      >
                        {this.state.items.map((item, index) => (
                          <Draggable key={item.id} draggableId={item.id} index={index}>
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style,
                                )}
                              >
                                {item.content}
                                {(addCloseBtn(item.content)) ?
                                  (<CloseButton onClick={() => this.removeItem(item.content)}>
                                    <Icon name="fas fa-times" iconSize="15px" color="#007BFF" marginLeft="-45px" />
                                  </CloseButton>) : null}

                              </div>
                            )}
                          </Draggable>
                        ))}
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </Col>
            </Row>
          </Col>
        </Row>
        <div className="text-center">
          <br />
          <Button color="primary" size="lg" type="submit" form="initInfor">
            Create New Template
          </Button>
        </div>
        <div>
          <Modal isOpen={this.state.recommendFieldFlag} toggle={this.toggleRecommend}>
            <ModalHeader toggle={this.toggleRecommend}>Field Suggestion</ModalHeader>
            <ModalBody>
              <div>What kind of certificate do you want to issue?</div>
              <RadioGroup
                aria-label="a"
                name="recommendOption1"
                value={this.state.recommendOption1}
                onChange={this.handleInputChange}
              >
                <FormControlLabel className="label-recommend" value="0" control={<Radio className="radio-recommend" color="primary" />} label="Bachelor Degree" />
                <FormControlLabel className="label-recommend" value="1" control={<Radio className="radio-recommend" color="primary" />} label="Course Certificate" />
                <FormControlLabel className="label-recommend" value="2" control={<Radio className="radio-recommend" color="primary" />} label="Birth Certificate" />
              </RadioGroup>
              <br />
              <div>Does recipient have to provide Ethereum address?</div>
              <RadioGroup
                aria-label="b"
                name="recommendOption2"
                value={this.state.recommendOption2}
                onChange={this.handleInputChange}
              >
                <FormControlLabel className="label-recommend" value="1" control={<Radio className="radio-recommend" color="primary" />} label="Yes" />
                <FormControlLabel className="label-recommend" value="0" control={<Radio className="radio-recommend" color="primary" />} label="No" />
              </RadioGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.toggleRecommend} style={{ width: '100px' }}>Cancel</Button>{' '}
              <Button color="primary" onClick={this.handleRecommend} style={{ width: '100px' }}>OK</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div >
    )
  }

}

function mapStateToProps(state) {
  return {
    manageTemp: state.manageTemplateReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleCreateNewTemplate: data => dispatch(createNewTemplate(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewTemplate)

