import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { Row, Col, Label, Form, Input, Button, Alert, InputGroupAddon, InputGroup } from 'reactstrap'
import styled from 'styled-components'
import Icon from '../../form/icon'
import {
  requiredFieldDefault,
  // setSuggestedFieldCustome,
  getCurrentListField, addCloseBtn
} from './Data'
import { getContentPath } from './../../../Utils'
import Chkboxlist from './Chkboxlist'
import { connect } from 'react-redux'
import { updateTemplate } from '../../../actions/ManageTemplateAction'

const ErrMsgTag = styled.p`
    color: red
`
const SuccessMsgTag = styled.p`
color: #007BFF
`
const CloseButton = styled.div.attrs({
  className: 'd-flex justify-content-center align-items-center',
})`
width: 20px;
height: 20px;
margin-left: 100%;
margin-top: -20px;
cursor: pointer;
`

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
const grid = 4
const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: grid,
  margin: `0 0 ${grid}px 0`,
  // change background colour if dragging
  background: isDragging ? 'white' : 'white',
  border: '1px solid #341f97',
  borderRadius: '7px',
  // styles we need to apply on draggables
  ...draggableStyle,
})

const getListStyle = isDraggingOver => ({
  // background: isDraggingOver ? 'lightblue' : 'lightgrey',
  background: 'none',
  border: '1px solid #341f97',
  borderRadius: '7px 0px 0px 7px',
  padding: grid * 2,
  width: 350,
  marginTop: '10px',
  overflowX: 'hidden',
  overflowY: 'auto',
  height: '500px',
})

class EditTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: getCurrentListField(this.props.templateData.value),
      customFieldDefault: [],
      customField: [],
      requiredFieldDefault,
      customFieldName: '',
      tempalteName: this.props.templateData.name,
      templateDescription: this.props.templateData.description,
      errMsgField: '',
      errMsgFieldFlag: false,
      errMsgName: '',
      errMsgNameFlag: false,
      successMsg: '',
      successMsgFlag: false,
      updateMsg: '',
      updateFlag: false,
      updateResultFlag: false,
      templateAll: this.props.templateAll,
    }
    this.onDragEnd = this.onDragEnd.bind(this)
  }

  checkKeyExist = (listItem, item) => {
    for (let index = 0; index < listItem.length; index += 1) {
      if (listItem[index].id.toLowerCase() === item.toLowerCase()) {
        return index
      }
    }
    return -1
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return
    }

    const items = reorder(
      this.state.items,
      result.source.index,
      result.destination.index,
    )

    this.setState({
      items,
    })
  }
  /** input change */
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      errMsgFieldFlag: false,
      errMsgNameFlag: false,
      successMsgFlag: false,
      updateFlag: false,
    })
  }
  /** Enter custom field */
  handleSubmit = (event) => {
    event.preventDefault()
    const cstfield = this.state.customFieldName
    const curList = this.state.items
    // check empty
    const customFieldName = cstfield.trim().replace(/\s\s+/g, ' ')
    // check exist item
    if (this.checkKeyExist(curList, customFieldName) === -1) {
      const temp = {
        id: customFieldName,
        content: customFieldName,
      }
      curList.push(temp)
      this.setState({
        items: curList,
        customFieldDefault: [...this.state.customFieldDefault, customFieldName],
        customFieldName: '',
        successMsg: `Field name "${customFieldName}" has been added!`,
        successMsgFlag: true,
      })
    } else {
      this.setState({
        errMsgField: `Field name "${customFieldName}" already exists.`,
        errMsgFieldFlag: true,
      })
    }
  }
  /** remove when close item */
  removeItem = (itemContent) => {
    const curList = this.state.items
    const i = this.checkKeyExist(this.state.items, itemContent)
    if (i !== -1) {
      curList.splice(i, 1)
    }
    this.setState({
      items: curList,
    })
  }
  handleSaveTemplate = async (event) => {
    event.preventDefault()
    const { tempalteName, templateAll, items, templateDescription } = this.state
    // check exist template's name
    if (tempalteName !== this.props.templateData.name) {
      for (let index = 0; index < templateAll.length; index += 1) {
        if (tempalteName === templateAll[index].name) {
          this.setState({
            errMsgName: `Template name "${tempalteName}" already exists.`,
            errMsgNameFlag: true,
          })
          return
        }
      }
    }
    const content = {}
    // set data to post
    for (let index = 0; index < items.length; index += 1) {
      const key = items[index].id
      const value = getContentPath(items[index].id)
      content[key] = value
    }
    const dataPost = {
      template: {
        content,
        name: tempalteName,
        description: templateDescription,
      },
    }
    await this.props.handleUpdateNewTemplate(this.props.templateData.id, dataPost)
    const { manageTemp } = this.props
    let msg = ''
    if (manageTemp.resultUpdateTemplate) {
      msg = 'Template has been updated!'
    } else {
      msg = manageTemp.errMsgUpdateNewTemplate
    }
    this.setState({
      updateFlag: true,
      updateMsg: msg,
      updateResultFlag: manageTemp.resultUpdateTemplate,
    })
  }
  render() {
    const { requiredFieldDefault, updateFlag, updateMsg, updateResultFlag } = this.state
    return (
      <div>
        <br />
        <Row>
          <Col xs="12" lg={{ size: 8, offset: 2 }} >
            <Row>
              <Col sm="12">
                {updateFlag ? (<Alert color={updateResultFlag ? 'primary' : 'danger'} >
                  {updateMsg}
                </Alert>) : null}
              </Col>
              <Col xs="12" lg="8">
                <Form id="initInfor" onSubmit={this.handleSaveTemplate}>
                  <Label>Template Name</Label>
                  <Input
                    type="text"
                    name="tempalteName"
                    value={this.state.tempalteName}
                    id="title"
                    placeholder="Enter template's name"
                    onChange={this.handleInputChange}
                    autoComplete="off"
                    required
                  />
                  {this.state.errMsgNameFlag ? (<ErrMsgTag color="danger">
                    {this.state.errMsgName}
                  </ErrMsgTag>) : null}
                  <br />
                  <Label>Description</Label>
                  <Input
                    value={this.state.templateDescription}
                    placeholder="Enter template's description"
                    type="textarea"
                    name="templateDescription"
                    onChange={this.handleInputChange}
                    maxLength="500"
                    rows="5"
                    style={{ resize: 'none' }}
                  />
                </Form>
                <br />
                <Label>Required Fields</Label>
                <Chkboxlist
                  onChange={values => this.onChange('requiredField', values)}
                  values={requiredFieldDefault}
                  disableFlag
                />
                <br />
                <Form className="form" onSubmit={this.handleSubmit}>
                  <Label>Custom Fields</Label>
                  <InputGroup>
                    <Input
                      name="customFieldName" autoComplete="off" pattern="[a-zA-Z0-9\s]+" type="text"
                      placeholder="Enter custom field name (only letters and numbers are allowed)"
                      title="Only letters and numbers are allowed!"
                      value={this.state.customFieldName}
                      onChange={this.handleInputChange}
                      required
                    />
                    <InputGroupAddon addonType="append">
                      <Button color="primary" style={{ width: '100px' }}>Add</Button>
                    </InputGroupAddon>
                  </InputGroup>
                  {this.state.errMsgFieldFlag ? (<ErrMsgTag >
                    {this.state.errMsgField}
                  </ErrMsgTag>) : null}
                  {this.state.successMsgFlag ? (<SuccessMsgTag >
                    {this.state.successMsg}
                  </SuccessMsgTag>) : null}
                </Form>
              </Col>
              <Col xs="12" lg="4" >
                <Label>Current Fields</Label>
                <DragDropContext onDragEnd={this.onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        style={getListStyle(snapshot.isDraggingOver)}
                      >
                        {this.state.items.map((item, index) => (
                          <Draggable key={item.id} draggableId={item.id} index={index}>
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style,
                                )}
                              >
                                {item.content}
                                {(addCloseBtn(item.content)) ?
                                  (<CloseButton onClick={() => this.removeItem(item.content)}>
                                    <Icon name="fas fa-times" iconSize="15px" color="#007BFF" marginLeft="-45px" />
                                  </CloseButton>) : null}
                              </div>
                            )}
                          </Draggable>
                        ))}
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </Col>
            </Row>
          </Col>
        </Row>
        <div className="text-center">
          <br />
          <Button color="primary" size="lg" type="submit" form="initInfor">
            Save
          </Button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    manageTemp: state.manageTemplateReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleUpdateNewTemplate: (id, data) => dispatch(updateTemplate(id, data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTemplate)

