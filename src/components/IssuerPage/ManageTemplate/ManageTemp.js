import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Tooltip,
} from 'reactstrap'
import { fetchData } from '../../../actions/IssueBatchAction'
import { AlertList } from 'react-bs-notifier'
import { makeData, Tips } from './Utils'
import { BeatLoader } from 'react-spinners'
import Icon from './../../form/icon'
import CreateNewTemplate from './CreateNewTemplate'
import EditTemplate from './EditTemplate'
import { getContentPath, setPageSizeOption } from './../../../Utils'
import { createNewTemplate, fetchTemplateDataAgain, deleteTemplate } from '../../../actions/ManageTemplateAction'
import ReactTable from 'react-table'
import { css } from 'react-emotion'
import Workbook from 'react-excel-workbook'
import styled from 'styled-components'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`

const Container = styled.div`
  .ReactTable .rt-thead.-header {
    box-shadow: none;
    background: #E9E9E9;
    font-weight: bold;
  }
  .ReactTable.-striped .rt-tr.-odd {
    background: #00000000;
  }
  .ReactTable.-striped .rt-tr.-even {
    background: #00000004;
  }
`

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}

class ManageTemp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      displayTable: true,
      tooltipOpen: false,
      modalDelete: false,
      displayCreateNew: false,
      displayEdit: false,
      editTemplate: {},
      interactTempName: '',
      tempalteName: '',
      description: '',
      position: 'top-right',
      alerts: [],
      timeout: 1500,
    }
  }
  componentDidMount = async () => {
    await this.props.fetchData()
  }
  // generate alert
  generate = (msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type: 'info',
      headline: 'Notification:',
      message: msg,
    }

    this.setState({
      alerts: [...this.state.alerts, newAlert],
    })
  }

  onAlertDismissed(alert) {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  toggle = async () => {
    if (!this.state.displayTable) {
      await this.props.fetchTemplateDataAgain()
    }
    this.setState({
      displayTable: !this.state.displayTable,
      displayCreateNew: !this.state.displayCreateNew,
    })
  }
  editBack = async () => {
    await this.props.fetchTemplateDataAgain()
    this.setState({
      displayTable: !this.state.displayTable,
      displayEdit: !this.state.displayEdit,
    })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      validateFlag: true,
    })
  }
  // delete template
  handleDeleteTemplate = async () => {
    await this.props.deleteTemplate(this.state.interactTempId)
    await this.props.fetchTemplateDataAgain()
    this.setState({
      modalDelete: !this.state.modalDelete,
    })
    this.generate(`Template ${this.state.interactTempName} has been deleted!`)
  }
  toggleBtnDelete = (id) => {
    this.getInteractiveTemplate(id)
    this.setState({
      modalDelete: !this.state.modalDelete,
    })
  }
  // duplicate template
  handleDuplicate = async (id) => {
    const interactTempData = this.getInteractiveTemplate(id)
    let duplicateTempName = interactTempData.name
    const { templateAll } = this.props.manageTemp
    let nameFlag = false
    const content = {}
    // set data to post
    for (let index = 0; index < interactTempData.value.length; index += 1) {
      const key = interactTempData.value[index]
      const value = getContentPath(interactTempData.value[index])
      content[key] = value
    }
    loop:
    while (!nameFlag) {
      duplicateTempName += ' Copy'
      for (let index = 0; index < templateAll.length; index += 1) {
        if (duplicateTempName === templateAll[index].name) {
          continue loop
        }
      }
      nameFlag = true
    }
    const dataPost = {
      template: {
        content,
        name: duplicateTempName,
        description: interactTempData.description,
      },
    }
    await this.props.handleCreateNewTemplate(dataPost)
    await this.props.fetchTemplateDataAgain()
    this.generate(`Template "${this.state.interactTempName}" has been duplicated with new name "${duplicateTempName}"!`)
  }
  handelEdit = (id) => {
    const interactTempData = this.getInteractiveTemplate(id)
    this.setState({
      editTemplate: interactTempData,
      displayTable: !this.state.displayTable,
      displayEdit: !this.state.displayEdit,
    })
  }
  getInteractiveTemplate(id, setStateFlag = true) {
    const { templateAll } = this.props.manageTemp
    for (let index = 0; index < templateAll.length; index += 1) {
      if (templateAll[index].id === id) {
        if (setStateFlag) {
          this.setState({
            interactTempName: templateAll[index].name,
            interactTempId: templateAll[index].id,
          })
        }

        return templateAll[index]
      }
    }
    return null
  }
  renderExportButton = (id) => {
    const interactTempData = this.getInteractiveTemplate(id, false)
    return (
      <Workbook filename={`${interactTempData.name}.xlsx`} element={<TooltipItem item={id} btnName={'primary'} iconName={'fas fa-file-export'} content={'Export'} templateId={id} action={() => { }} />}>
        <Workbook.Sheet data={[]} name="Sheet A">
          {this.renderColunm(interactTempData.value)}
        </Workbook.Sheet>
      </Workbook>
    )
  }
  renderColunm = (arrColumn) => {
    let result = {}
    if (arrColumn.length > 0) {
      result = arrColumn.map((col, index) => (
        <Workbook.Column label={col} key={index} value={''} />
      ))
    }
    return result
  }
  render() {
    const data = makeData(this.props.manageTemp.templateAll)
    const { displayTable, displayCreateNew, displayEdit, editTemplate } = this.state
    return (
      <Container>
        <AlertList
          position={this.state.position}
          alerts={this.state.alerts}
          timeout={this.state.timeout}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed.bind(this)}
        />

        {displayTable && (
          <Row>
            <Col sm="12" >
              <Button color="primary" size="md" onClick={this.toggle}>Create New Template</Button>
              <br /><br />
            </Col>
            <Col sm="12">
              {
                this.props.manageTemp.isFetching ? (
                  <BeatLoader
                    className={override}
                    sizeUnit={'px'}
                    size={15}
                    color={'#36D7B7'}
                  />) : (<div>
                    <ReactTable
                      getTdProps={() => ({
                        style: {
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                        },
                      })}
                      data={data}
                      columns={
                        [{
                          Header: '#',
                          accessor: 'no',
                          maxWidth: 60,
                          ...ColumnCenterAlign,
                        },
                        {
                          Header: 'Name',
                          accessor: 'name',
                          ...ColumnLeftAlign,
                        },
                        {
                          Header: 'Description',
                          accessor: 'description',
                          ...ColumnLeftAlign,
                        },
                        {
                          Header: 'Actions',
                          accessor: 'id',
                          maxWidth: 250,
                          Cell: row => (
                            <div className="text-center">
                              <TooltipItem item={row.value} btnName={'danger'} iconName={'far fa-trash-alt'} content={'Delete'} templateId={row.value} action={this.toggleBtnDelete} />
                              <TooltipItem item={row.value} btnName={'info'} iconName={'far fa-clone'} content={'Duplicate'} templateId={row.value} action={this.handleDuplicate} />
                              {this.renderExportButton(row.value)}
                              <TooltipItem item={row.value} btnName={'success'} iconName={'far fa-edit'} content={'Edit'} templateId={row.value} action={this.handelEdit} />

                            </div>
                          ),
                        },
                        ]}
                      defaultPageSize={10}
                      pageSizeOptions={setPageSizeOption(data.length)}
                      className="-striped -highlight templateTable"
                    />
                    <Tips />
                  </div>)
              }
            </Col>
          </Row>
        )}
        {displayCreateNew && (
          <CreateNewTemplate templateData={this.props.manageTemp.templateAll} onBack={this.toggle} />
        )}
        {displayEdit && (
          <Row>
            <Col sm="12" >
              <Button color="primary" size="md" onClick={this.editBack}>
                <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
                Back
              </Button>
              <br />
            </Col>
            <Col sm="12" >
              <EditTemplate templateAll={this.props.manageTemp.templateAll} templateData={editTemplate} />
            </Col>
          </Row >
        )}
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleBtnDelete} centered>
          <ModalHeader toggle={this.toggleBtnDelete}>Delete Template</ModalHeader>
          <ModalBody>
            Are you sure to delete template "{this.state.interactTempName}" ?
          </ModalBody>
          <ModalFooter>
            <Button color="danger" size="lg" onClick={this.handleDeleteTemplate}>Delete</Button>{' '}
            <Button color="secondary" size="lg" onClick={this.toggleBtnDelete}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </Container>
    )
  }
}
class TooltipItem extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      tooltipOpen: false,
      templateId: this.props.templateId,
    }
  }
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    })
  }
  onClickHandle = () => {
    this.props.action(this.props.templateId)
  }
  render() {
    return (
      <span>
        <Button
          color={this.props.btnName}
          id={`id${this.props.content}${this.props.item}`}
          onClick={this.onClickHandle}
        >
          <Icon name={this.props.iconName} color="white" iconSize="1em" />
          {/* Delete */}
        </Button>
        <Tooltip
          placement="bottom"
          innerClassName="tooltipTemp"
          hideArrow
          isOpen={this.state.tooltipOpen}
          target={`id${this.props.content}${this.props.item}`}
          toggle={this.toggle}
          delay={150}
        >
          {this.props.content}
        </Tooltip>
        {' '}
      </span>
    )
  }
}

function mapStateToProps(state) {
  return {
    manageTemp: state.manageTemplateReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: () => dispatch(fetchData()),
    deleteTemplate: templateId => dispatch(deleteTemplate(templateId)),
    handleCreateNewTemplate: data => dispatch(createNewTemplate(data)),
    fetchTemplateDataAgain: () => dispatch(fetchTemplateDataAgain()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageTemp)

