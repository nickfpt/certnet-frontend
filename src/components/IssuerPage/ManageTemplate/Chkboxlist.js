import React from 'react'
import { Row, Col } from 'reactstrap'

class Chkboxlist extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    props.values.map((v) => {
      this.state[v.content] = v.checkFlag
    })
  }
  componentWillReceiveProps(nextProps) {
    nextProps.values.map((v) => {
      this.state[v.content] = v.checkFlag
    })
  }
  onChange = (key, value) => {
    this.setState({ [key]: value }, (state) => {
      this.props.onChange(this.state)
    })
  }

  render() {
    return (
      <div className="list-group-item form-group">
        <Row>
          {this.props.values.map((value, i) => (
            <Col sm="4" key={i}>
              <div className="pretty p-svg p-curve" key={i}>
                <input
                  onChange={e => this.onChange(value.content, e.target.checked)} checked={this.state[value.content]}
                  type="checkbox" disabled={this.props.disableFlag}
                  value={this.state[value.content]}
                />
                <div className="state p-primary">
                  <svg className="svg svg-icon" viewBox="0 0 20 20">
                    <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style={{ stroke: 'white', fill: 'white' }} />
                  </svg>
                  <label> {value.content}</label>
                </div>
              </div>
            </Col>
                    ))}
        </Row>
      </div>
    )
  }
}
export default Chkboxlist
