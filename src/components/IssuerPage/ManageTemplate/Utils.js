import React from 'react'

const createTemp = (temp, index) => ({
  no: index + 1,
  name: temp.name,
  description: temp.description,
  id: temp.id,
})

export function makeData(arrTemp) {
  const result = []
  for (let index = 0; index < arrTemp.length; index += 1) {
    const element = createTemp(arrTemp[index], index)
    result.push(element)
  }
  return result
}

export const Tips = () =>
  (<div style={{ textAlign: 'center' }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>)
