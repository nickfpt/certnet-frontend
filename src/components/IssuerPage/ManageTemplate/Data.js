// import { DialogContentText } from "@material-ui/core";
import { camelCase } from 'lodash'

export const suggestedFieldDefault = [
  { content: 'Issuer', checkFlag: false },
  { content: 'DoB', checkFlag: false },
  { content: 'Gender', checkFlag: false },
  { content: 'Major', checkFlag: false },
  { content: 'Mode of Study', checkFlag: false },
  { content: 'Graduation Grade', checkFlag: false },
  { content: 'Roll number', checkFlag: false },
  { content: 'PoB', checkFlag: false },
  { content: 'Nationality', checkFlag: false },
  { content: 'Race', checkFlag: false },
  { content: 'Graduation Year', checkFlag: false },
  { content: 'Cert Id', checkFlag: false },
  { content: 'Decision Id', checkFlag: false },
  { content: 'Ethereum Address', checkFlag: false },
  { content: 'Nha phat hanh', checkFlag: false },
  { content: 'Ten nguoi nhan', checkFlag: false },
  { content: 'Ngay sinh', checkFlag: false },
  { content: 'Chuyen nganh', checkFlag: false },
  { content: 'Xep loai tot nghiep', checkFlag: false },
  { content: 'Hinh thuc dao tao', checkFlag: false },

]
export const requiredFieldDefault = [
  { content: 'Email', checkFlag: true },
  { content: 'Recipient Name', checkFlag: true },
  { content: 'Certificate name', checkFlag: true },
]
export const suggestedFieldBirthCert = [
  { content: 'So', checkFlag: false },
  { content: 'Ten khai sinh', checkFlag: false },
  { content: 'Ngay sinh', checkFlag: false },
  { content: 'Ngay sinh bang chu', checkFlag: false },
  { content: 'Gioi tinh', checkFlag: false },
  { content: 'Dan toc', checkFlag: false },
  { content: 'Quoc tich', checkFlag: false },
  { content: 'Noi sinh', checkFlag: false },
  { content: 'Que quan', checkFlag: false },
  { content: 'Ho ten me', checkFlag: false },
  { content: 'Nam sinh me', checkFlag: false },
  { content: 'Dan toc me', checkFlag: false },
  { content: 'Quoc tich me', checkFlag: false },
  { content: 'Noi cu tru me', checkFlag: false },
  { content: 'Ho ten bo', checkFlag: false },
  { content: 'Nam sinh bo', checkFlag: false },
  { content: 'Dan toc bo', checkFlag: false },
  { content: 'Quoc tich bo', checkFlag: false },
  { content: 'Noi cu tru bo', checkFlag: false },
  { content: 'Ho ten nguoi di khai sinh', checkFlag: false },
  { content: 'Giay to tuy than', checkFlag: false },
  { content: 'Noi dang ky khai sinh', checkFlag: false },
  { content: 'Ngay dang ky khai sinh', checkFlag: false },
  { content: 'Ethereum Address', checkFlag: false },
]
export function setListItemsDefault() {
  const listItemsDefault = []
  for (let index = 0; index < requiredFieldDefault.length; index += 1) {
    const temp = {
      id: requiredFieldDefault[index].content,
      content: requiredFieldDefault[index].content,
    }
    listItemsDefault.push(temp)
  }
  return listItemsDefault
}

// case recommend Bachelor Degree
export function setListItemsBachelorDefault(requireEth = true) {
  const listItemsDefault = []
  for (let index = 0; index < requiredFieldDefault.length; index += 1) {
    const temp = {
      id: requiredFieldDefault[index].content,
      content: requiredFieldDefault[index].content,
    }
    listItemsDefault.push(temp)
  }
  for (let index = 0; index < suggestedFieldDefault.length; index += 1) {
    const temp = {
      id: suggestedFieldDefault[index].content,
      content: suggestedFieldDefault[index].content,
    }
    if (requireEth) {
      listItemsDefault.push(temp)
    } else if (temp.content !== 'Ethereum Address') {
      listItemsDefault.push(temp)
    }
  }
  return listItemsDefault
}
// case recommend Course Degree
export function setListItemsCourseDefault(requireEth = true) {
  const listItemsDefault = []
  for (let index = 0; index < requiredFieldDefault.length; index += 1) {
    const temp = {
      id: requiredFieldDefault[index].content,
      content: requiredFieldDefault[index].content,
    }
    listItemsDefault.push(temp)
  }
  const certID = {
    id: 'Cert Id',
    content: 'Cert Id'
  }
  listItemsDefault.push(certID)
  if (requireEth) {
    const ethAdd = {
      id: 'Ethereum Address',
      content: 'Ethereum Address'
    }
    listItemsDefault.push(ethAdd)
  }
  return listItemsDefault
}
// case recommend Birth Cert
export function setListItemsBirthCert(requireEth = true) {
  const listItemsDefault = []
  for (let index = 0; index < requiredFieldDefault.length; index += 1) {
    const temp = {
      id: requiredFieldDefault[index].content,
      content: requiredFieldDefault[index].content,
    }
    listItemsDefault.push(temp)
  }
  for (let index = 0; index < suggestedFieldBirthCert.length; index += 1) {
    const temp = {
      id: suggestedFieldBirthCert[index].content,
      content: suggestedFieldBirthCert[index].content,
    }
    if (requireEth) {
      listItemsDefault.push(temp)
    } else if (temp.content !== 'Ethereum Address') {
      listItemsDefault.push(temp)
    }
  }
  return listItemsDefault
}
export function addCloseBtn(content) {
  for (let index = 0; index < requiredFieldDefault.length; index += 1) {
    if (requiredFieldDefault[index].content === content) {
      return false
    }
  }
  return true
}
export function setSuggestedFieldCustome(arrCustom) {
  const suggestedFieldCustom = JSON.parse(JSON.stringify(suggestedFieldDefault))
  for (let index = 0; index < arrCustom.length; index += 1) {
    for (let i = 0; i < suggestedFieldCustom.length; i += 1) {
      if (suggestedFieldCustom[i].content === arrCustom[index]) {
        suggestedFieldCustom[i].checkFlag = true
      }
    }
  }
  return suggestedFieldCustom
}

export function getCurrentListField(arrFiled) {
  const currentListField = []
  for (let index = 0; index < arrFiled.length; index += 1) {
    const temp = {
      id: arrFiled[index],
      content: arrFiled[index],
    }
    currentListField.push(temp)
  }
  return currentListField
}
