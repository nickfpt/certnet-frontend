import React, { Component } from 'react'
import styled from 'styled-components'
import * as Color from './../../constants/Color'
import { makeDataCert } from './Data'
import { connect } from 'react-redux'
import { Form, Input, Button, Row, Col, Modal, ModalHeader, ModalBody, InputGroup, InputGroupAddon, Alert } from 'reactstrap'
import { setPageSizeOption } from './../../Utils'
import { getCertById, generateQuickShareLink } from './../../actions/QuickShareAction'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import ReactTable from 'react-table'
import { AlertList } from 'react-bs-notifier'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import get from 'lodash/get'

const dateFormat = require('dateformat')

const HomeWrapper = styled.section`
  background: ${Color.LV1};
`
const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 400px; 
`
const inputNumberDay = {
  marginLeft: '10px',
  width: '50px',
  display: 'inline-block',
  padding: '5px',
}
const btnGetLink = {
  marginLeft: '10px',
  marginBottom: '7px',
}
const colModalLink = {
  paddingRight: '5px',
  paddingLeft: '0px',
  marginBottom: '5px',
}

class QuickSharePage extends Component {
  constructor(props) {
    console.log('constructor')
    super(props)
    this.state = {
      certId: '',
      currCertId: '',
      msgFlag: false,
      msg: '',
      dataTable: [],
      numberDays: 7,
      alerts: [],
      activeFlag: false,
      validFlag: false
    }
  }
  componentWillMount = async () => {
    console.log('componentWillMount', this.props.match)
    const certId = get(this.props.match, ['params', 'certId'], null)
    console.log('certId', certId)

    if (certId !== null) {
      await this.setState({
        certId,
      })
      this.handleSubmit()
    } else {
      this.setState({
        certId: '',
      })
    }
  }
  // manage alert
  onAlertDismissed = (alert) => {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  generate = (msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type: 'info',
      headline: 'Notification:',
      message: msg,
    }
    this.setState({
      alerts: [newAlert],
    })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
    })
  }
  handleSubmit = async (e) => {
    console.log('handleSubmit', e)
    if (e) {
      e.preventDefault()
    }
    let { certId } = this.state
    certId = certId.split('/').join('') // remove "/"
    await this.props.handleGetCertById(certId)
    // check valid certId
    if (!this.props.quickShare.resultFlag) {
      this.setState({
        msg: "Cannot get certificate information, please check your certificate ID.",
        msgFlag: true,
        dataTable: [],
        currCertId: '',
        activeFlag: false,
        validFlag: false
      })
    } else if (this.props.quickShare.revokeFlag) {
      this.setState({
        msg: 'Certificate is invalid!',
        msgFlag: true,
        dataTable: makeDataCert(this.props.quickShare.resultData.certificate),
        currCertId: certId,
        activeFlag: true,
        validFlag: false
      })
    } else {
      this.setState({
        msg: 'Certificate is valid!',
        msgFlag: true,
        dataTable: makeDataCert(this.props.quickShare.resultData.certificate),
        currCertId: certId,
        activeFlag: true,
        validFlag: true
      })
    }
  }
  handleGetLink = async (e) => {
    e.preventDefault()
    const dataPost = {
      certId: this.state.currCertId,
      duration: this.state.numberDays,
    }
    await this.props.handleGenerateQuickShareLink(dataPost)
    this.setState({
      modalGetLink: !this.state.modalGetLink,
    })
  }
  toggleModalGetLink = () => {
    this.setState({
      modalGetLink: !this.state.modalGetLink,
    })
  }
  focus = () => {
    this.inputLink.focus()
    this.inputLink.select()
    this.generate('Link has been copied to clipboard!')
  }
  render() {
    const { msgFlag, msg, dataTable, activeFlag, validFlag } = this.state
    const { quickShare } = this.props
    return (
      <HomeWrapper>
        <AlertList
          position="top-right"
          alerts={this.state.alerts}
          timeout={1000}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed}
        />
        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col sm="12" className={activeFlag ? 'active-home' : 'disactive-home'} >
              <div className="text-center">
                <h3>The quickest way to share your certificate</h3>
              </div>
            </Col>
            <Col xs="12" lg={{ size: 8, offset: 2 }} xl={{ size: 6, offset: 3 }}>
              <InputGroup>
                <Input
                  placeholder="Enter your certificate ID"
                  value={this.state.certId}
                  name="certId"
                  onChange={this.handleInputChange}
                  type="text"
                  className="verify-input"
                  // autoComplete="off"
                  autoFocus
                  required
                />
                <InputGroupAddon addonType="prepend">
                  <Button color="primary" className="verify-input" style={{ width: '100px' }} >Submit</Button>
                </InputGroupAddon>
              </InputGroup>
            </Col>
          </Row>
        </Form>
        {quickShare.isFetching &&
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <BeatLoader
                className={override}
                sizeUnit={'px'}
                size={15}
                color={'#36D7B7'}
              />
            </Col>
          </Row>
        }
        {
          !quickShare.isFetching && msgFlag &&
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }} >
              <br />
              {msgFlag &&
                <Alert color={validFlag ? 'primary' : 'danger'} fade={false}>
                  {msg}
                </Alert>}

            </Col>
          </Row>
        }
        {!quickShare.isFetching && dataTable.length > 0 ? (
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }}> <h5>Certificate Information</h5></Col>
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <ReactTable
                data={dataTable}
                columns={[
                  {
                    Header: 'No.',
                    accessor: 'no',
                    maxWidth: 60,
                  },
                  {
                    Header: 'Subject',
                    accessor: 'subject',
                    maxWidth: 250,
                  },
                  {
                    Header: 'Value',
                    accessor: 'value',
                  },
                ]}
                defaultSorted={[
                  {
                    id: 'index',
                    desc: false,
                  },
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
                pageSizeOptions={setPageSizeOption(dataTable.length)}
              />

            </Col>
            <Col sm="12" md={{ size: 8, offset: 2 }} style={{ marginTop: '20px' }}>
              <div className="text-center">
                <h6 style={{ display: 'inline-block' }} >Duration days</h6>
                <Form style={{ display: 'inline-block' }} onSubmit={this.handleGetLink}>
                  <Input
                    value={this.state.numberDays}
                    onChange={this.handleInputChange}
                    type="number"
                    name="numberDays"
                    min="1" max="14"
                    style={inputNumberDay}
                  />
                  <Button color="primary" style={btnGetLink}>Get Link</Button>
                </Form>
              </div>
            </Col>
          </Row>
        ) : null}
        <Modal isOpen={this.state.modalGetLink} toggle={this.toggleModalGetLink} keyboard={false} backdrop={'static'} size="lg" centered>
          <ModalHeader toggle={this.toggleModalGetLink}>Share Link</ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12" sm="12" md="10" style={colModalLink}>
                <Input
                  innerRef={(input) => { this.inputLink = input }}
                  readOnly
                  value={`https://www.certnet.site/home/${quickShare.shareLinkData.token}`}
                />
              </Col>
              <Col xs="12" sm="12" md="2" style={colModalLink}>
                <CopyToClipboard text={`https://www.certnet.site/home/${quickShare.shareLinkData.token}`}>
                  <Button color="primary" block onClick={this.focus} >Copy</Button>
                </CopyToClipboard>
              </Col>
            </Row>
            Expire Time: {dateFormat(quickShare.shareLinkData.expiredTime, ' mmmm dS, yyyy, h:MM:ss TT')}
          </ModalBody>
        </Modal>
      </HomeWrapper>
    )
  }
}

function mapStateToProps(state) {
  return {
    quickShare: state.quickShareReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetCertById: async (certId) => { await dispatch(getCertById(certId)) },
    handleGenerateQuickShareLink: async (certData) => { await dispatch(generateQuickShareLink(certData)) },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuickSharePage)

