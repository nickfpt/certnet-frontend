import React, { Component } from 'react'
import styled from 'styled-components'
import { Row, Col } from 'reactstrap'
import { connect } from 'react-redux'
import * as Color from './../constants/Color'

const HomeWrapper = styled.section`
  padding-left: 1%;
  background: ${Color.LV1};
`
const rowStyle = {
  minHeight: '750px',
}
const colStyle = {
  background: Color.LV1,
}

class NotFound extends Component {
  render() {
    return (
      <HomeWrapper>
        <Row style={rowStyle}>
          <Col style={colStyle} md="12" lg={{ size: 10, offset: 1 }} >
            <h1>NOT FOUND</h1>
          </Col>
        </Row>
      </HomeWrapper>
    )
  }
}
function mapStateToProps() {
  return {
  }
}

function mapDispatchToProps() {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotFound)

