import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { Row, Col, Input, Button, Alert, Form, InputGroup, InputGroupAddon, Tooltip } from 'reactstrap'
import ReactTable from 'react-table'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import get from 'lodash/get'
import { makeDataAllCert, makeDataCert } from './Data'
import { setPageSizeOption } from './../../Utils'
import { verifyToken } from './../../actions/VerifyAction'
import * as Color from './../../constants/Color'
import Icon from './../form/icon'
import QrReader from 'react-qr-reader'
// import { off } from 'rsvp';

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 200px; 
`
const HomeWrapper = styled.section`
  background: ${Color.LV1};
`

const Container = styled(Col)`
  .ReactTable .rt-thead.-header {
    box-shadow: none;
    background: #E9E9E9;
    font-weight: bold;
  }
  .ReactTable.-striped .rt-tr.-odd {
    background: #00000000;
  }
  .ReactTable.-striped .rt-tr.-even {
    background: #00000004;
  }
`

const ContainerDiv = styled.div`
  .ReactTable .rt-thead.-header {
    box-shadow: none;
    background: #E9E9E9;
    font-weight: bold;
  }
  .ReactTable.-striped .rt-tr.-odd {
    background: #00000000;
  }
  .ReactTable.-striped .rt-tr.-even {
    background: #00000004;
  }
`

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}


const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      token: '',
      curToken: '',
      validFlag: false,
      validMsg: '',
      dataTable: [],
      numberDays: 7,
      duplicateErrFlap: false, // check duplicate err emply input and invalid token
      alerts: [],
      activeFlag: false,
      tooltipOpen: false
    }
  }
  componentWillMount = async () => {
    const token = get(this.props.match, ['params', 'token'], null)
    if (token !== null) {
      await this.setState({
        token,
      })
      this.handleVerify()
    } else {
      this.setState({
        token: '',
        curToken: '',
        validFlag: false,
        validMsg: '',
        dataTable: [],
        numberDays: 7,
        alerts: [],
      })
    }
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
    })
  }
  handleVerify = async () => {
    const arrStart = [
      'http://www.certnet.site/home/',
      'https://www.certnet.site/home/',
      'www.certnet.site/home/',
      'certnet.site/home/',
      'http://www.certnet.info/home/',
      'https://www.certnet.info/home/',
      'www.certnet.info/home/',
      'certnet.info/home/'
    ]
    let { token } = this.state
    this.setState({ duplicateErrFlap: false })
    for (let index = 0; index < arrStart.length; index += 1) {
      if (token.startsWith(arrStart[index])) {
        token = token.slice(arrStart[index].length)
        break
      }
    }
    token = token.split('/').join('') // remove "/"
    // console.log(token)
    await this.props.handleVerifyToken(token)
    // check token valid or not
    if (!this.props.verifyTokenData.validFlag) {
      // check token error code : 1 mean invalid token
      if (this.props.verifyTokenData.errorCode === 1) {
        this.setState({
          validMsg: this.props.verifyTokenData.errMsg,
          validFlag: false,
          dataTable: [],
          activeFlag: false
        })
      } else {
        // valid token, but somes certificate failed
        this.setState({
          validMsg: 'Some certificates are invalid!',
          validFlag: false,
          dataTable: this.props.verifyTokenData.resultsData,
          activeFlag: true
        })
      }
    } else {
      // success
      this.setState({
        validFlag: true,
        validMsg: 'All certificates are valid!',
        dataTable: this.props.verifyTokenData.resultsData,
        activeFlag: true
      })
    }
  }
  handleSubmit = (e) => {
    e.preventDefault()
    this.handleVerify()
  }
  handleScan = (data) => {
    // console.log('data', data)
    if (data) {
      this.setState({
        token: data
      })
      this.handleVerify()
    } else {
      this.setState({
        validMsg: 'Cannot read QR code from uploaded image.',
        token: '',
        validFlag: false,
        dataTable: [],
        activeFlag: false
      })
    }
  }
  handleError = (err) => {
    console.error(err)
  }
  openImageDialog = () => {
    this.refs.qrReader.openImageDialog()
  }
  toggle = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render() {
    const { dataTable, validFlag, validMsg, duplicateErrFlap, activeFlag } = this.state
    const { verifyTokenData } = this.props
    const columns = [
      {
        Header: '#',
        accessor: 'no',
        width: 30,
        ...ColumnCenterAlign,
      },
      {
        Header: 'Certificate Name',
        accessor: 'certName',
        ...ColumnLeftAlign,
      },
      {
        Header: 'Recipient Name',
        accessor: 'recipientName',
        ...ColumnLeftAlign,
      },
      {
        Header: 'Issuer',
        accessor: 'issuer',
        ...ColumnLeftAlign,
      },
      {
        Header: 'Status',
        accessor: 'status',
        width: 200,
        Cell: row => (
          <div className="text-center">
            <Button color={row.value === 'success' ? 'primary' : 'danger'} disabled style={{ width: '150px' }}>
              {row.value === 'success' ? 'Valid' : 'Invalid'}
            </Button>
          </div>
        ),
      },
    ]
    const data = makeDataAllCert(dataTable)
    return (
      <HomeWrapper>
        <div className="main-homepage">
          <Row>
            <Col sm="12" md={{ size: 10, offset: 1 }} className="main-homepage-body">
              <Row>
                {/* <Col sm="12" className={activeFlag ? 'active-home' : 'disactive-home'} > */}
                {/* <Col sm="12" className="disactive-home"> */}
                <Col sm="12" style={{ marginTop: '100px' }}>
                  <div className="text-center">
                    <h1 style={{ color: 'white', fontSize: '45px', marginBottom: '30px' }}>Verify Certificates</h1>
                  </div>
                </Col>
                <Col xs="12" lg={{ size: 8, offset: 2 }} xl={{ size: 6, offset: 3 }}>
                  <Form onSubmit={this.handleSubmit}>
                    <InputGroup>
                      <Input
                        value={this.state.token}
                        name="token"
                        onChange={this.handleInputChange}
                        type="text"
                        className="verify-input"
                        style={{ paddingRight: '40px' }}
                        required
                        autoFocus
                        autoComplete="off"
                      />
                      <Icon name="fas fa-file-upload icon-upload" color="#ADB5BD" iconSize="2em" onClick={this.openImageDialog} id="TooltipExample" />
                      <Tooltip placement="bottom" isOpen={this.state.tooltipOpen} target="TooltipExample" toggle={this.toggle}>
                        Upload
                      </Tooltip>
                      <InputGroupAddon addonType="prepend">
                        <Button color="primary" className="verify-input" style={{ width: '100px' }} >Verify</Button>
                      </InputGroupAddon>
                    </InputGroup>
                    <div className="verify-input-note">Verify certificate by URL or file</div>
                  </Form>
                  <div style={{ display: 'none' }}>
                    <QrReader
                      ref="qrReader"
                      delay={0}
                      onError={this.handleError}
                      onScan={this.handleScan}
                      legacyMode
                    />
                  </div>
                </Col>
              </Row>

              {
                verifyTokenData.isFetching &&
                <Row>
                  <Col sm="12" md={{ size: 8, offset: 2 }}>
                    <BeatLoader
                      className={override}
                      sizeUnit={'px'}
                      size={15}
                      color={'#36D7B7'}
                    />
                  </Col>
                </Row>
              }
              {
                !verifyTokenData.isFetching && !duplicateErrFlap && validMsg !== '' &&
                <Row>
                  <Col xs="12" lg={{ size: 8, offset: 2 }} xl={{ size: 6, offset: 3 }}>
                    <div className="verify-result-alert">
                      {validFlag ?
                        <Icon name="fas fa-check-circle" color="#6bbe5e" iconSize="2em" />
                        :
                        <Icon name="fas fa-exclamation-triangle" color="#e34e4b" iconSize="2em" />
                      }
                      <br />
                      {validMsg}
                    </div>
                  </Col>
                </Row>
              }
              {
                !verifyTokenData.isFetching && dataTable.length > 0 ? (
                  <Row>
                    <Col sm="12" md={{ size: 10, offset: 1 }} >
                      <div className="text-center">
                        <h5 style={{ margin: '10px 0px', color: 'white' }}>Certificate Infor</h5>
                      </div>
                    </Col>

                    <Container sm="12" md={{ size: 10, offset: 1 }} style={{ padding: '15px', backgroundColor: 'white' }}>
                      <ReactTable
                        getTdProps={() => ({
                          style: {
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                          },
                        })}
                        data={data}
                        columns={columns}
                        defaultPageSize={10}
                        pageSizeOptions={setPageSizeOption(data.length)}
                        className="-striped -highlight"
                        SubComponent={(row) => {
                          const dataDetail = makeDataCert(row.original.data.certificate)
                          return (
                            <ContainerDiv style={{ padding: '10px 25px 10px 65px' }}>
                              <ReactTable
                                data={dataDetail}
                                columns={[
                                  {
                                    Header: '#',
                                    accessor: 'no',
                                    maxWidth: 60,
                                    ...ColumnCenterAlign,
                                  },
                                  {
                                    Header: 'Subject',
                                    accessor: 'subject',
                                    maxWidth: 350,
                                  },
                                  {
                                    Header: 'Value',
                                    accessor: 'value',
                                  },
                                ]}
                                defaultSorted={[
                                  {
                                    id: 'index',
                                    desc: false,
                                  },
                                ]}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                pageSizeOptions={setPageSizeOption(dataDetail.length)}
                              />
                            </ContainerDiv>
                          )
                        }}
                      />
                    </Container>

                  </Row>
                ) : null
              }
            </Col>
          </Row>
        </div>
        <section className="features" id="features">
          <div className="container">
            <div className="section-heading text-center">
              <h2>CertNet Features</h2>
              <p className="text-muted">Compare to traditional way</p>
              <hr />
            </div>
            <div className="row">
              <div className="col-lg-4 my-auto">
                <div className="device-container">
                  <div className="device-mockup iphone6_plus portrait white">
                    <div className="device">
                      <div className="screen">
                        {/* Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! */}
                        <img src="https://storage.googleapis.com/certnet/img/item1.png" className="img-fluid" alt="" />
                      </div>
                      <div className="button">
                        {/* You can hook the "home button" to some JavaScript events or just remove it */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-8 my-auto">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="feature-item">
                        {/* <i className="icon-check text-primary" /> */}
                        <Icon name="far fa-check-circle" color="#A4366C" iconSize="5em" marginBottom="25px" />
                        <h3>No fake certificates</h3>
                        <p className="text-muted">Cannot fake certificate in CertNet.</p>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="feature-item">
                        <Icon name="far fa-clock" color="#A4366C" iconSize="5em" marginBottom="25px" />
                        <h3>Reduce time</h3>
                        <p className="text-muted">Reduce time for issuing and verifying.</p>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="feature-item">
                        <Icon name="fas fa-layer-group" color="#A4366C" iconSize="5em" marginBottom="25px" />
                        <h3>Reduce cost</h3>
                        <p className="text-muted">Just $0.01-$0.02 to issue.</p>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="feature-item">
                        <Icon name="far fa-lightbulb" color="#A4366C" iconSize="5em" marginBottom="25px" />
                        <h3>Baseline for future</h3>
                        <p className="text-muted">Baseline for digital certificate solution.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="features bg-light text-center" id="step">
          <div className="container">
            <div className="section-heading text-center">
              <h2>How to issue?</h2>
              <p className="text-muted">Just follow these easy steps</p>
              <hr style={{ marginBottom: '6rem' }} />
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                  <div className="features-item-icon-circle"><span className="number">1</span></div>
                  <h3>Create a new batch</h3>
                  <p className="lead mb-0">Create a new batch of certificates with content template.</p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                  <div className="features-item-icon-circle"><span className="number">2</span></div>
                  <h3>Select design template</h3>
                  <p className="lead mb-0">Select the predefined visual template for your certificates.</p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="features-icons-item mx-auto mb-0 mb-lg-3">
                  <div className="features-item-icon-circle"><span className="number">3</span></div>
                  <h3>Issue</h3>
                  <p className="lead mb-0">Issue certificate batch by sending an Ethereum transaction.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="main-homepage-2">
          <div className="container">
            <Row style={{ paddingTop: '100px' }}>
              <Col lg={{ size: 4, offset: 2 }}>
                <div className="header-content mx-auto">
                  <h2 style={{ color: 'white' }}>With the Blockchain technology, your official records are now yours forever.
                Receive them once, share and verify them for a lifetime.</h2>
                </div>
              </Col>
              <Col lg={{ size: 6 }}>
                <div className="device-mockup macbook white">
                  <div className="device">
                    <div className="screen">
                      {/* Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! */}
                      <iframe width="100%" height="300px" src="https://www.youtube.com/embed/JB3IryQqQBQ" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </HomeWrapper >
    )
  }
}
function mapStateToProps(state) {
  return {
    verifyTokenData: state.verifyTokenReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleVerifyToken: async (token) => { await dispatch(verifyToken(token)) },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)

