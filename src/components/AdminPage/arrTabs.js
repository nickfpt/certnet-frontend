const arrTabs = [
  {
    id: '1',
    title: 'Manage Requests',
    to: '/manage-request'
  },
  {
    id: '2',
    title: 'Manage Issuers',
    to: '/manage-issuer'
  }
]
export default arrTabs
