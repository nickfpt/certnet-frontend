import React, { Component } from 'react'
import { getAllIssuers, changeIssuerStatus, reGetAllIssuers } from './../../../actions/AdminIssuerAction'
import { connect } from 'react-redux'
import { BeatLoader } from 'react-spinners'
import { css } from 'react-emotion'
import { AlertList } from 'react-bs-notifier'
import { Row, Col, Button } from 'reactstrap'
import ReactTable from 'react-table'
import { makeData } from './Utils'
import UpdateIssuer from './UpdateIssuer'
import { Tips, setPageSizeOption } from './../../../Utils'
import Icon from '../../form/icon'
import Toggle from 'react-toggle'
import 'react-toggle/style.css'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`
const actionBtn = {
  width: '100px',
  margin: '0px 5px',
}

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}

class ManageIssuer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      position: 'top-right',
      alerts: [],
      timeout: 1500,
      issuerUpdatedData: {},
      displayMain: true
    }
  }
  componentDidMount() {
    this.props.handleGetAllIssuers()
  }
  onAlertDismissed = (alert) => {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  generate = (msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type: 'info',
      headline: 'Notification:',
      message: msg,
    }
    this.setState({
      alerts: [...this.state.alerts, newAlert],
    })
  }
  changeIssuableStatus = async (issuer, checked) => {
    await this.props.handleChangeIssuerStatus(issuer.id, issuer.issuableStatus)
    await this.props.handleReGetAllIssuers()
    this.generate(`The status of issuer '${issuer.name}' has been changed to [${checked ? 'Issuable' : 'Not Issuable'}]!`)
  }
  toggleUpdateView = (issuerId) => {
    const { adminData } = this.props
    const issuerUpdated = adminData.issuers.filter(issuer =>
      issuer._id === issuerId
    )
    if (issuerUpdated.length > 0) {
      this.setState({
        displayMain: !this.state.displayMain,
        issuerUpdatedData: issuerUpdated[0]
      })
    }
  }
  editBack = () => {
    this.setState({
      displayMain: !this.state.displayMain
    })
    this.props.handleReGetAllIssuers()
  }
  render() {
    const { issuerUpdatedData, displayMain } = this.state
    const { adminData } = this.props
    return (
      <div>
        <AlertList
          position={this.state.position}
          alerts={this.state.alerts}
          timeout={this.state.timeout}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed}
        />
        {displayMain ? (<Row>
          <Col xs="12" xl={{ size: 10, offset: 1 }}>
            {
              adminData.isFetching ? (
                <BeatLoader
                  className={override}
                  sizeUnit={'px'}
                  size={15}
                  color={'#36D7B7'}
                />)
                :
                (
                  <div>
                    {adminData.errMsg === '' ?
                      (<div>
                        <ReactTable
                          getTdProps={() => (
                            {
                              style: {
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                              },
                            })}
                          data={makeData(adminData.issuers)}
                          filterable
                          defaultFilterMethod={(filter, row) =>
                            String(row[filter.id]).toLowerCase().includes(filter.value.toLowerCase())}
                          columns={[{
                            Header: '#',
                            accessor: 'no',
                            maxWidth: 60,
                            ...ColumnCenterAlign,
                            filterable: false
                          },
                          {
                            Header: 'Name',
                            accessor: 'name',
                            ...ColumnLeftAlign,
                          },
                          {
                            Header: 'Email',
                            accessor: 'email',
                            ...ColumnLeftAlign,
                          },
                          {
                            Header: 'Phone Number',
                            accessor: 'phone',
                            ...ColumnLeftAlign,
                          },
                          {
                            Header: 'Actions',
                            accessor: 'id',
                            width: 120,
                            filterable: false,
                            sortable: false,
                            Cell: row => (
                              <div className="text-center">
                                <Button color="primary" style={actionBtn} onClick={() => this.toggleUpdateView(row.value)} outline>Details</Button>
                              </div>
                            ),
                          },
                          {
                            Header: 'Issuable Status',
                            accessor: 'data',
                            width: 200,
                            filterable: false,
                            sortable: false,
                            Cell: row => (
                              <div className="text-center">
                                <Toggle checked={row.value.issuableStatus} onChange={e => this.changeIssuableStatus(row.value, e.target.checked)} />
                              </div>
                            ),
                          },
                          ]}
                          defaultPageSize={10}
                          className="-striped -highlight templateTable"
                          pageSizeOptions={setPageSizeOption(adminData.issuers.length)}
                        />
                        <Tips />
                      </div>) : <p>{adminData.errMsg}</p>
                    }
                  </div>
                )}
          </Col>
        </Row>
        ) :
          (
            <div>
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <Button color="primary" size="lg" onClick={this.editBack}>
                    <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
                    Back
                  </Button>
                  <br /><br />
                </Col>
              </Row >
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <UpdateIssuer data={issuerUpdatedData} />
                </Col>
              </Row >
            </div>
          )}
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    adminData: state.adminIssuerReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetAllIssuers: () => dispatch(getAllIssuers()),
    handleReGetAllIssuers: () => dispatch(reGetAllIssuers()),
    handleChangeIssuerStatus: (id, value) => dispatch(changeIssuerStatus(id, value)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageIssuer)

