import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Alert, Progress } from 'reactstrap'
import * as callAPI from '../../../constants/CallAPI'
import * as endpoints from '../../../constants/endpoints'
import { NETWORK } from '../../../constants/common'
import { web3, ZERO_ADDRESS, isAddress } from '../../../services/web3'
import Cookies from "universal-cookie"
import jwt from "jsonwebtoken";
import Certnet from '../../../contract/certnet'

const cookies = new Cookies()

class ConfirmModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      alert: false,
      error: '',
      web3Alert: false,
      web3Error: '',
      web3CurrentWallet: null,
      disableIssue: false,
      disableClose: false,
      message: null,
      loader: false,
      issuerETHaddresses: [],
      progress: 0
    }
  }

  async componentDidMount() {
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    this.setState({
      issuerETHaddresses: token.ethereumAccounts
    })
    const message = `Are you sure to ${this.props.addAddress? `add address ${this.props.ethAddressToAdd}`
      : `remove address ${this.props.ethAddressToRemove}`}`
    if (typeof web3 !== 'undefined') {
      this.setState({ web3Checked: true, web3Supported: true })
      const checkAccountChange = async () => {
        const network = await web3.eth.net.getNetworkType()
        if (network !== NETWORK.RINKENY) {
          this.setState({
            web3Alert: true,
            web3Error: 'Please switch to Rinkeby network.',
          })
        } else {
          const accounts = await web3.eth.getAccounts()
          if (accounts.length > 0) {
            if (this.state.issuerETHaddresses.includes(accounts[0])) {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: false
              })
              if (!this.state.message) {
                this.setState({
                  message
                })
              }
            } else {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: true,
                web3Error: 'Please switch to Ethereum address of the administrator on MetaMask.',
              })
            }
          } else {
            this.setState({
              web3Alert: true,
              web3Error: 'Please login to MetaMask.',
            })
          }
        }
      }

      await checkAccountChange()
      if (!this.state.alert && !this.state.web3Alert) {
        this.setState({
          message
        })
      }
      this.checkChangeWalletInterval = setInterval(checkAccountChange, 1000)
    } else {
      this.setState({ web3Alert: true, web3Error: 'MetaMask is not installed.' })
    }
  }

  async add() {
    const { ethAddressToAdd, issuerId, email, handleAddEthAddress } = this.props
    console.log(ethAddressToAdd)
    const { web3CurrentWallet: sender } = this.state
    if (!isAddress(ethAddressToAdd)) {
      this.setState({ alert: true, error: `${ethAddressToAdd} is not a valid Ethereum address.` })
      return
    }
    const checkIfThisAddressExistAlready = await Certnet.methods.issuers(ethAddressToAdd).call()
    if (checkIfThisAddressExistAlready && checkIfThisAddressExistAlready.issuerAddress !== ZERO_ADDRESS) {
      this.setState({ alert: true, error: `Address ${ethAddressToAdd} already belongs to another issuer.` })
      return
    }
    await this.setState({
      loader: true,
      disableClose: true,
      disableIssue: true,
      message: 'Adding address in progress. Please do not close the dialog or browser!',
    })
    try {
      if (!ethAddressToAdd) {
        return this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Ethereum address not found.',
        })
      }
      let intervalId
      await Certnet.methods.addIssuer(ethAddressToAdd, email).send({
        from: sender
      }, (e, processedContract) => {
        if (processedContract) {
          let time = 0
          intervalId = setInterval(() => {
            if (time <= 30) {
              time += 1
              this.setState({
                progress: time * 100 / 35
              })
            }
          }, 1000)
        }
      })
      const addAddressResponse = await callAPI.fetch(endpoints.addETHAddress(issuerId), true, {
        address: ethAddressToAdd
      })
      if (addAddressResponse.ok) {
        this.setState({
          progress: 100
        })
        handleAddEthAddress()
        clearInterval(intervalId)
        setTimeout(() => {
          this.setState({
            loader: false,
            disableClose: false,
            message: 'Address has been added!',
          })
        }, 800)
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: addAddressResponse.error,
        })
      }
    } catch (err) {
      if (err.message.includes('User denied transaction signature')) {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Address adding process has been cancelled.',
        })
      }
    }
  }

  async remove() {
    const { ethAddressToRemove, issuerId, handleRemoveETH } = this.props
    console.log(ethAddressToRemove)
    const { web3CurrentWallet: sender } = this.state
    if (!isAddress(ethAddressToRemove)) {
      this.setState({ alert: true, error: `${ethAddressToRemove} is not a valid Ethereum address.` })
      return
    }
    const checkAddressInUsed = await callAPI.fetch(endpoints.checkAddressInUsed(ethAddressToRemove))
    if (!checkAddressInUsed.ok) {
      this.setState({ alert: true, error: checkAddressInUsed.error })
      return
    }
    await this.setState({
      loader: true,
      disableClose: true,
      disableIssue: true,
      message: 'Removing address in progress. Please do not close the dialog or browser!',
    })
    try {
      if (!ethAddressToRemove) {
        return this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Ethereum address not found',
        })
      }
      let intervalId
      await Certnet.methods.removeIssuer(ethAddressToRemove).send({
        from: sender
      }, (e, processedContract) => {
        if (processedContract) {
          let time = 0
          intervalId = setInterval(() => {
            if (time <= 30) {
              time += 1
              this.setState({
                progress: time * 100 / 35
              })
            }
          }, 1000)
        }
      })
      const removeAddressResponse = await callAPI.fetch(endpoints.removeETHAddress(issuerId), true, {
        address: ethAddressToRemove
      })
      if (removeAddressResponse.ok) {
        this.setState({
          progress: 100
        })
        handleRemoveETH()
        clearInterval(intervalId)
        setTimeout(() => {
          this.setState({
            loader: false,
            disableClose: false,
            message: 'Address has been removed!',
          })
        }, 800)
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: removeAddressResponse.error,
        })
      }
    } catch (err) {
      if (err.message.includes('User denied transaction signature')) {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Address removing process has been cancelled.',
        })
      }
    }
  }

  render() {
    const { modal, toggle, addAddress } = this.props
    const { alert, error, disableIssue, message, loader, disableClose, web3Alert, web3Error, progress } = this.state
    return (<div>

      <Modal isOpen={modal} style={{ minWidth: '900px' }} className="text-center">
        <ModalHeader>Confirm {addAddress? 'Add' : 'Remove'}  Ethereum Address</ModalHeader>
        <ModalBody>
          {alert && <Alert color="danger">
            {error}
          </Alert>}
          {web3Alert && <Alert color="danger">
            {web3Error}
          </Alert>}
          {!alert && (<strong>{message}</strong>)}
          {!alert && loader &&
          <Progress striped color="primary" value={progress} style={{ marginTop: 20 }}/>}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" size="lg" disabled={disableClose} onClick={toggle}>Close</Button>{' '}
          {!alert && !web3Alert &&
          <Button color="danger" size="lg" disabled={disableIssue} onClick={() => {
            if (addAddress) {
              this.add()
            } else {
              this.remove()
            }
          }}>{addAddress ? 'Add' : 'Remove'}</Button>}
        </ModalFooter>
      </Modal>
    </div>)
  }
}

ConfirmModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  ethAddressToAdd: PropTypes.string,
  ethAddressToRemove: PropTypes.string,
  addAddress: PropTypes.bool.isRequired,
  issuerId: PropTypes.string,
  email: PropTypes.string,
  handleAddEthAddress: PropTypes.func.isRequired,
  handleRemoveETH: PropTypes.func.isRequired
}

export default ConfirmModal
