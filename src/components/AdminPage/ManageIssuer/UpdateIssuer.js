import React, { Component } from 'react'
import 'react-datepicker/dist/react-datepicker.css'
import { updateIssuer, uploadLogo } from './../../../actions/AdminIssuerAction'
import { connect } from 'react-redux'
import {
  Row, Col, Card, Button, CardHeader, CardBody, Modal, ModalHeader, ModalBody, ModalFooter,
  CardTitle, Table, Input, InputGroup, InputGroupAddon, Form, Alert
} from 'reactstrap'
import { IMAGE_URL } from './../../../constants/common'

import ConfirmModal from './ConfirmModal'
const colImage = {
  textAlign: 'center',
}
const actionBtn = {
  width: '90px',
  margin: '0px 5px',
}

const dateFormat = require('dateformat')


class UpdateIssuer extends Component {
  constructor(props) {
    super(props)
    const { data } = this.props
    const arrETHdata = []
    if (data.ethereumAccounts.length > 0) {
      data.ethereumAccounts.forEach((element) => {
        arrETHdata.push(element.address)
      })
    }
    const { _id, email, issuableStatus, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, logo } = this.props.data

    this.state = {
      issuerId: _id,
      email,
      status: issuableStatus === 1 ? 'Yes' : 'No',
      fullname,
      description,
      address,
      phone,
      contactEmail,
      taxAccount,
      establishedDate,
      ownerName,
      website,
      arrETH: arrETHdata,
      errAddETH: '',
      modalRemoveETH: false,
      removeETHaddress: '',
      flagAddETH: true,
      disableBtnUpdate: true,
      changeLogo: false,
      updateFlag: false,
      updateMsg: '',
      updateResultFlag: false,
      file: '',
      logo,
      confirmModal: false,
      addAddress: false,
      ethAddressToAdd: '',
      ethAddressToRemove: ''
    }
  }
  toggleConfirmModal = () => {
    this.setState({
      confirmModal: !this.state.confirmModal
    })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      flagAddETH: true,
      disableBtnUpdate: false,
      updateFlag: false,
    })
  }
  handleImageChange = (e) => {
    e.preventDefault()

    const reader = new FileReader()
    const file = e.target.files[0]
    reader.onloadend = () => {
      console.log(file)
      console.log(reader.result)
      this.setState({
        file,
        logo: reader.result,
        disableBtnUpdate: false,
        changeLogo: true,
      })
    }
    reader.readAsDataURL(file)
  }
  handleAddEthAddress = async () => {
    const { arrETH, ethAddressToAdd } = this.state
    this.setState({
      arrETH: [ethAddressToAdd, ...arrETH],
      ethAddressToAdd: ''
    })
  }
  displayETHaddress = (arr) => {
    let result = null
    if (arr.length > 0) {
      result = arr.map(element => (
        <li className="eth-list" key={element}>{element}
          <span className="close" onClick={() => this.handleClickClose(element)}>&times;</span>
        </li>
      ))
    }
    return result
  }
  handleClickClose = (ETHaddress) => {
    this.setState({
      ethAddressToRemove: ETHaddress,
      addAddress: false,
    })
    this.toggleConfirmModal()
  }
  toggleBtnRemove = () => {
    this.setState({
      modalRemoveETH: !this.state.modalRemoveETH,
    })
  }
  handleRemoveETH = () => {
    const { arrETH, removeETHaddress } = this.state
    const index = arrETH.indexOf(removeETHaddress)
    arrETH.splice(index, 1)
    this.setState({
      removeETHaddress: '',
      flagAddETH: true,
      disableBtnUpdate: false,
      updateFlag: false,
    })
  }
  handleSubmitUpdate = async (event) => {
    event.preventDefault()
    let msg = ''
    const { issuerId, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, changeLogo, file } = this.state
    const data = {
      fullname,
      description,
      address,
      phone,
      contactEmail,
      taxAccount,
      establishedDate,
      ownerName,
      website: decodeURIComponent(website),
    }
    if (changeLogo) {
      data.logo = `${IMAGE_URL}/${issuerId}`
      const formData = new FormData()
      formData.append('image', file)
      formData.append('path', `/logo_partners/${issuerId}`)
      await this.props.handleUploadLogo(formData)
    }
    console.log('data', data)
    await this.props.handleUpdateIssuer(issuerId, data)
    const { adminData } = this.props
    if (adminData.updateResultFlag) {
      msg = 'Issuer information has been updated!'
    } else {
      msg = 'Something wrong happened, please try again.'
    }
    this.setState({
      updateFlag: true,
      disableBtnUpdate: true,
      updateMsg: msg,
      updateResultFlag: adminData.updateResultFlag,
    })
  }
  render() {
    const { email, status, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, errAddETH,
      website, arrETH, flagAddETH, removeETHaddress, disableBtnUpdate, updateFlag, updateMsg,
      updateResultFlag, confirmModal, addAddress, issuerId, ethAddressToAdd, ethAddressToRemove, logo } = this.state
    return (
      <div>
        <Card>
          <CardHeader className="text-center"><h4>Issuer Information</h4></CardHeader>
          <CardBody>
            <Row>
              <Col sm="12">
                {updateFlag ? (<Alert color={updateResultFlag ? 'primary' : 'danger'} >
                  {updateMsg}
                </Alert>) : null}
              </Col>
              <Col md="12" xl="4" style={colImage} >
                <CardTitle>Logo</CardTitle>
                <img
                  style={{ width: '90%', maxWidth: '400px' }}
                  alt="User Pic"
                  src={logo ? logo : `${IMAGE_URL}/default_user`}
                  className="img-circle img-responsive" />
                <label className="btnUpload">
                  Change Logo <input type="file" hidden onChange={e => this.handleImageChange(e)} accept="image/*" />
                </label>
              </Col>

              <Col md="12" xl="8">
                <Form onSubmit={this.handleSubmitUpdate}>
                  <Table borderless>
                    <tbody>
                      <tr>
                        <td width="20%">Email</td>
                        <td><Input type="email" name="email" value={email} disabled /></td>
                      </tr>
                      <tr>
                        <td width="20%">Issuable Status</td>
                        <td><Input type="text" name="status" value={status} disabled /></td>
                      </tr>
                      <tr>
                        <td width="20%">Name</td>
                        <td><Input type="text" name="fullname" value={fullname} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td width="20%">Owner Name</td>
                        <td><Input type="text" name="ownerName" value={ownerName} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td><Input type="tel" name="phone" value={phone} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td width="20%">Contact Email</td>
                        <td><Input type="email" name="contactEmail" value={contactEmail} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td><Input type="textarea" name="address" id="exampleText" value={address} onChange={this.handleInputChange} maxLength="500" required /></td>
                      </tr>
                      <tr>
                        <td width="20%">Established Date</td>
                        <td><Input type="date" name="establishedDate" value={dateFormat(establishedDate, 'isoDate')} onChange={this.handleInputChange} required /></td>
                      </tr>
                      <tr>
                        <td width="20%">Tax Account</td>
                        <td><Input type="text" name="taxAccount" value={taxAccount} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Website</td>
                        <td><Input type="text" name="website" value={unescape(website)} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Description</td>
                        <td><Input type="textarea" name="description" id="exampleText" value={description} onChange={this.handleInputChange} maxLength="500" required /></td>
                      </tr>
                      <tr>
                        <td colSpan="2" className="text-center">
                          <Button color="danger" type="submit" disabled={disableBtnUpdate}>Update</Button>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </Form>
                <Table>
                  <tbody>
                  <tr>
                    <td width="20%">Ethereum Addresses</td>
                    <td>
                      <InputGroup>
                        <Input type="text" name="ethAddressToAdd" value={ethAddressToAdd} onChange={this.handleInputChange} autoComplete="off" />
                        <InputGroupAddon addonType="prepend">
                          <Button color="primary" type="submit" style={{ width: '100px' }} onClick={() => {
                            this.setState({
                              addAddress: true
                            })
                            this.toggleConfirmModal()
                          }}>
                            Add
                          </Button>
                          </InputGroupAddon>
                        </InputGroup>
                      </td>
                    </tr>
                    <tr>
                      <td />
                      <td>
                        {!flagAddETH && <p style={{ color: 'red' }}>{errAddETH}</p>}
                        <ul style={{ padding: '0px' }}>
                          {this.displayETHaddress(arrETH)}
                        </ul>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
            {confirmModal && <ConfirmModal
              toggle={this.toggleConfirmModal}
              modal={confirmModal}
              ethAddressToAdd={ethAddressToAdd}
              ethAddressToRemove={ethAddressToRemove}
              addAddress={addAddress}
              issuerId={issuerId}
              email={email}
              handleAddEthAddress={this.handleAddEthAddress}
              handleRemoveETH={this.handleRemoveETH}
            />}
          </CardBody>
        </Card>
        <Modal isOpen={this.state.modalRemoveETH} toggle={this.toggleBtnRemove} centered>
          <ModalHeader toggle={this.toggleBtnRemove}>Remove Ethereum address</ModalHeader>
          <ModalBody>
            <p>Are you sure to remove address &#34;{removeETHaddress}&#34; ?</p>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.handleRemoveETH} style={actionBtn}>Remove</Button>{' '}
            <Button color="secondary" onClick={this.toggleBtnRemove} style={actionBtn}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div >
    )
  }
}
function mapStateToProps(state) {
  return {
    adminData: state.adminIssuerReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleUpdateIssuer: async (issuerId, data) => { await dispatch(updateIssuer(issuerId, data)) },
    handleUploadLogo: data => dispatch(uploadLogo(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateIssuer)
