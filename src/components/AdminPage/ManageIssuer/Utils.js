const createReq = (req, index) => ({
  no: index + 1,
  name: req.fullname,
  email: req.email,
  phone: req.phone,
  id: req._id,
  data: {
    issuableStatus: req.issuableStatus === 1,
    id: req._id,
    name: req.fullname,
  }
})

export function makeData(arrReq) {
  const result = []
  for (let index = 0; index < arrReq.length; index += 1) {
    const element = createReq(arrReq[index], index)
    result.push(element)
  }
  return result
}
