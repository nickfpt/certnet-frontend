import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Alert, Progress } from 'reactstrap'
import * as callAPI from '../../../constants/CallAPI'
import * as endpoints from '../../../constants/endpoints'
import { NETWORK } from '../../../constants/common'
import { web3, ZERO_ADDRESS, isAddress } from '../../../services/web3'
import Cookies from "universal-cookie"
import jwt from "jsonwebtoken";
import Certnet from '../../../contract/certnet'

const cookies = new Cookies()

class ConfirmModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      alert: false,
      error: '',
      web3Alert: false,
      web3Error: '',
      web3CurrentWallet: null,
      disableIssue: false,
      disableClose: false,
      message: null,
      loader: false,
      issuerETHaddresses: [],
      progress: 0
    }
  }

  async componentDidMount() {
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    this.setState({
      issuerETHaddresses: token.ethereumAccounts
    })
    const message = `Are you sure to ${this.props.reject? 'reject' : 'approve'} request from ${this.props.name} ?`
    if (typeof web3 !== 'undefined') {
      this.setState({ web3Checked: true, web3Supported: true })
      const checkAccountChange = async () => {
        const network = await web3.eth.net.getNetworkType()
        if (network !== NETWORK.RINKENY) {
          this.setState({
            web3Alert: true,
            web3Error: 'Please switch to Rinkeby network.',
          })
        } else {
          const accounts = await web3.eth.getAccounts()
          if (accounts.length > 0) {
            if (this.state.issuerETHaddresses.includes(accounts[0])) {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: false
              })
              if (!this.state.message) {
                this.setState({
                  message
                })
              }
            } else {
              this.setState({
                web3CurrentWallet: accounts[0],
                web3Alert: true,
                web3Error: 'Please switch to Ethereum address of the administrator on MetaMask.',
              })
            }
          } else {
            this.setState({
              web3Alert: true,
              web3Error: 'Please login to MetaMask.',
            })
          }
        }
      }

      await checkAccountChange()
      if (!this.state.alert && !this.state.web3Alert) {
        this.setState({
          message
        })
      }
      this.checkChangeWalletInterval = setInterval(checkAccountChange, 1000)
    } else {
      this.setState({ web3Alert: true, web3Error: 'MetaMask is not installed.' })
    }
  }

  async accept() {
    const { address, requestId, changeStatus, requesterEmail } = this.props
    const { web3CurrentWallet: sender } = this.state
    if (!isAddress(address)) {
      this.setState({ alert: true, error: `${address} is not a valid Ethereum address.` })
      return
    }
    const checkIfThisAddressExistAlready = await Certnet.methods.issuers(address).call()
    if (checkIfThisAddressExistAlready && checkIfThisAddressExistAlready.issuerAddress !== ZERO_ADDRESS) {
      this.setState({ alert: true, error: `Address ${address} already belongs to another issuer.` })
      return
    }
    await this.setState({
      loader: true,
      disableClose: true,
      disableIssue: true,
      message: 'Request approval in progress. Please do not close the dialog or browser!',
    })
    try {
      if (!address) {
        return this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Candidate\'s Ethereum address not found',
        })
      }
      let intervalId
      await Certnet.methods.addIssuer(address, requesterEmail).send({
        from: sender
      }, (e, processedContract) => {
        if (processedContract) {
          let time = 0
          intervalId = setInterval(() => {
            if (time <= 30) {
              time += 1
              this.setState({
                progress: time * 100 / 35
              })
            }
          }, 1000)
        }
      })
      const revokeResponse = await callAPI.fetch(endpoints.approveRequest(requestId))
      if (revokeResponse.ok) {
        this.setState({
          progress: 100
        })
        clearInterval(intervalId)
        setTimeout(() => {
          this.setState({
            loader: false,
            disableClose: false,
            message: 'Request has been approved!',
          })
          changeStatus('reject')
        }, 800)
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: revokeResponse.error,
        })
      }
    } catch (err) {
      if (err.message.includes('User denied transaction signature')) {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: 'Request approval process has been cancelled.',
        })
      }
    }
  }

  async reject() {
    const { requestId, changeStatus } = this.props
    try {
      const revokeResponse = await callAPI.fetch(endpoints.rejectRequest(requestId))
      if (revokeResponse.ok) {
        this.setState({
          loader: false,
          disableClose: false,
          message: 'Request has been rejected!',
        })
        changeStatus('reject')
      } else {
        this.setState({
          alert: true,
          disableClose: false,
          disableIssue: false,
          error: revokeResponse.error,
        })
      }
    } catch (err) {
      this.setState({
        alert: true,
        disableClose: false,
        disableIssue: false,
        error: err.message
      })
    }
  }

  render() {
    const { modal, toggle, reject } = this.props
    const { alert, error, disableIssue, message, loader, disableClose, web3Alert, web3Error, progress } = this.state
    return (<div>

      <Modal isOpen={modal} style={{ minWidth: '900px' }} className="text-center">
        <ModalHeader>Confirm {reject? 'Reject Request' : 'Approve Request'}</ModalHeader>
        <ModalBody>
          {alert && <Alert color="danger">
            {error}
          </Alert>}
          {web3Alert && <Alert color="danger">
            {web3Error}
          </Alert>}
          {!alert && (<strong>{message}</strong>)}
          {!alert && loader &&
          <Progress striped color="primary" value={progress} style={{ marginTop: 20 }}/>}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" size="lg" disabled={disableClose} onClick={toggle}>Close</Button>{' '}
          {!alert && !web3Alert &&
          <Button color="danger" size="lg" disabled={disableIssue} onClick={() => {
            if (reject) {
              this.reject()
            } else {
              this.accept()
            }
          }}>{reject ? 'Reject' : 'Approve'}</Button>}
        </ModalFooter>
      </Modal>
    </div>)
  }
}

ConfirmModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  changeStatus: PropTypes.func.isRequired,
  address: PropTypes.string,
  requestId: PropTypes.string,
  name: PropTypes.string,
  reject: PropTypes.bool.isRequired,
  requesterEmail: PropTypes.string
}

export default ConfirmModal
