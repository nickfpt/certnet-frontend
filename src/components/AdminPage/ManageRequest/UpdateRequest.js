import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'react-datepicker/dist/react-datepicker.css'
import {
  Row, Col, Card, Button, CardHeader, CardBody,
  CardTitle, Table, Input, Form, Alert
} from 'reactstrap'
import { isAddress } from './../../../services/web3'
import { updateRequest, uploadLogo } from './../../../actions/AdminRequestAction'
import ConfirmModal from './ConfirmModal'
import { IMAGE_URL } from './../../../constants/common'

const colImage = {
  textAlign: 'center',
}

const dateFormat = require('dateformat')

const actionBtn = {
  width: '90px',
  margin: '0px 5px',
}

class UpdateRequest extends Component {
  constructor(props) {
    super(props)
    const { _id, requesterEmail, ethereumAccount, status, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, logo } = this.props.data
    this.state = {
      reqId: _id,
      requesterEmail,
      status,
      fullname,
      description,
      address,
      phone,
      ethereumAccount,
      contactEmail,
      taxAccount,
      establishedDate: dateFormat(establishedDate, 'isoDate'),
      ownerName,
      website,
      updateFlag: false,
      updateMsg: '',
      updateResultFlag: false,
      disableBtnUpdate: true,
      confirmModal: false,
      reject: false,
      file: '',
      logo,
      changeLogo: false
    }
  }
  toggleConfirmModal = () => {
    this.setState({
      confirmModal: !this.state.confirmModal
    })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      updateFlag: false,
      disableBtnUpdate: false
    })
  }
  handleSubmitUpdate = async (event) => {
    event.preventDefault()
    const { reqId, fullname, description, ethereumAccount, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, changeLogo, file } = this.state
    let msg = ''
    if (isAddress(ethereumAccount)) {
      const data = {
        fullname,
        description,
        address,
        phone,
        ethereumAccount,
        contactEmail,
        taxAccount,
        establishedDate,
        ownerName,
        website,
      }
      if (changeLogo) {
        data.logo = `${IMAGE_URL}/${reqId}`
        const formData = new FormData()
        formData.append('image', file)
        formData.append('path', `/logo_partners/${reqId}`)
        await this.props.handleUploadLogo(formData)
      }
      await this.props.handlUpdateRequest(reqId, data)
      const { adminData } = this.props
      if (adminData.updateResultFlag) {
        msg = 'Request information has been updated!'
      } else {
        msg = 'Something wrong happened, please try again.'
      }
      this.setState({
        updateFlag: true,
        disableBtnUpdate: true,
        updateMsg: msg,
        updateResultFlag: adminData.updateResultFlag,
      })
    } else {
      msg = `"${ethereumAccount}" is not a valid Ethereum address.`
      this.setState({
        updateFlag: true,
        disableBtnUpdate: true,
        updateMsg: msg,
      })
    }
  }
  changeStatus = (status) => {
    this.setState({
      status
    })
  }
  handleImageChange = (e) => {
    e.preventDefault()

    const reader = new FileReader()
    const file = e.target.files[0]

    reader.onloadend = () => {
      this.setState({
        file,
        logo: reader.result,
        changeLogo: true,
        disableBtnUpdate: false
      })
    }
    reader.readAsDataURL(file)
  }
  render() {
    const { requesterEmail, status, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website,
      updateFlag, updateMsg, updateResultFlag, disableBtnUpdate, ethereumAccount, confirmModal, reject, reqId,
      logo } = this.state
    return (
      <div>
        <Card>
          <CardHeader className="text-center"><h4>Request Information</h4></CardHeader>
          <CardBody>
            <Row>
              <Col sm="12">
                {updateFlag ? (<Alert color={updateResultFlag ? 'primary' : 'danger'} >
                  {updateMsg}
                </Alert>) : null}
              </Col>
              <Col md="12" xl="4" style={colImage} >
                <CardTitle>Logo</CardTitle>
                <img
                  style={{ width: '90%', maxWidth: '400px' }}
                  alt="User Pic"
                  src={logo ? logo : `${IMAGE_URL}/default_user`}
                  className="img-circle img-responsive" />
                <label className="btnUpload">
                Change Logo
                  <input type="file" hidden onChange={e => this.handleImageChange(e)} accept="image/*" />
                </label>
              </Col>
              <Col md="12" xl="8">
                <Form onSubmit={this.handleSubmitUpdate}>
                  <Table borderless>
                    <tbody>
                      <tr>
                        <td width="20%">Requester Email</td>
                        <td><Input type="email" name="requesterEmail" value={requesterEmail} disabled /></td>
                      </tr>
                      <tr>
                        <td width="20%">Request Status</td>
                        <td><Input type="email" name="status" value={status} disabled /></td>
                      </tr>
                      <tr>
                        <td width="20%">Name</td>
                        <td><Input type="text" name="fullname" value={fullname} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td width="20%">Ethereum Address</td>
                        <td><Input type="text" name="ethereumAccount" value={ethereumAccount} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td width="20%">Owner Name</td>
                        <td><Input type="text" name="ownerName" value={ownerName} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td><Input type="tel" name="phone" value={phone} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td width="20%">Contact Email</td>
                        <td><Input type="email" name="contactEmail" value={contactEmail} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td><Input type="textarea" name="address" id="exampleText" value={address} onChange={this.handleInputChange} maxLength="500" required /></td>
                      </tr>
                      <tr>
                        <td width="20%">Established Date</td>
                        <td><Input type="date" name="establishedDate" value={establishedDate} onChange={this.handleInputChange} required /></td>
                      </tr>
                      <tr>
                        <td width="20%">Tax Account</td>
                        <td><Input type="text" name="taxAccount" value={taxAccount} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Website</td>
                        <td><Input type="text" name="website" value={website} onChange={this.handleInputChange} required autoComplete="off" /></td>
                      </tr>
                      <tr>
                        <td>Description</td>
                        <td><Input type="textarea" name="description" id="exampleText" value={description} onChange={this.handleInputChange} maxLength="500" required /></td>
                      </tr>
                    </tbody>
                  </Table>

                  {status === 'pending' &&
                    <div>
                      <Button color="warning" type="submit" disabled={disableBtnUpdate} size="md" style={actionBtn}>Update</Button>
                      <Button
                        color="primary" style={actionBtn} size="md" disabled={status !== 'pending'} onClick={() => {
                          this.setState({
                            reject: false
                          })
                          this.toggleConfirmModal()
                        }} >
                        Approve</Button>
                      <Button
                        color="danger" style={actionBtn} size="md" disabled={status !== 'pending'} onClick={() => {
                          this.setState({
                            reject: true
                          })
                          this.toggleConfirmModal()
                        }} > Reject</Button>
                    </div>
                  }

                </Form>
              </Col>
            </Row>
            {confirmModal && (<ConfirmModal
              modal={confirmModal}
              toggle={this.toggleConfirmModal}
              address={ethereumAccount}
              requestId={reqId}
              reject={reject}
              name={fullname}
              changeStatus={this.changeStatus}
              requesterEmail={requesterEmail}
            />)}
          </CardBody>
        </Card>
      </div >
    )
  }
}
function mapStateToProps(state) {
  return {
    adminData: state.adminRequestReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handlUpdateRequest: async (reqId, data) => { await dispatch(updateRequest(reqId, data)) },
    handleUploadLogo: data => dispatch(uploadLogo(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateRequest)
