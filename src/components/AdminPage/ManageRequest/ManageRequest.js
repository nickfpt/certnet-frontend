import React, { Component } from 'react'
import { getRequestToBeIssuer, reGetRequestToBeIssuer, rejectRequest, approveRequest } from './../../../actions/AdminRequestAction'
import { connect } from 'react-redux'
import { BeatLoader } from 'react-spinners'
import { AlertList } from 'react-bs-notifier'
import { css } from 'react-emotion'
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import ReactTable from 'react-table'
import UpdateRequest from './UpdateRequest'
import { makeData } from './Utils'
import { Tips, setPageSizeOption } from './../../../Utils'
import Icon from '../../form/icon'

const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`
const actionBtn = {
  width: '90px',
  margin: '0px 5px',
}

const ColumnCenterAlign = {
  style: { textAlign: 'center' },
  headerStyle: { textAlign: 'center' },
}

const ColumnLeftAlign = {
  style: { textAlign: 'left' },
  headerStyle: { textAlign: 'left' },
}

const StatusTag = ({ status }) => {
  if (status === 'pending') return (<b style={{ color: '#DFA801' }}>Pending</b>)
  if (status === 'accept') return (<b style={{ color: '#0169D9' }}>Approved</b>)
  if (status === 'reject') return (<b style={{ color: '#C82333' }}>Rejected</b>)
  return null
}
class ManageRequest extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalReject: false,
      modalApprove: false,
      reqReject: {},
      reqApprove: {},
      position: 'top-right',
      alerts: [],
      timeout: 1500,
      displayMain: true,
      reqUpdatedData: {},
      selectedCandidateAddress: '',
      filteredValue: [{ id: 'status', value: 'pending' }]
    }
  }
  componentDidMount() {
    // async componentDidMount() {
    //   const owner = await Certnet.methods.owner().call()
    //   const balance = await web3.eth.getBalance(owner)
    //   console.log({ owner }, web3.utils.fromWei(balance, 'ether'))
    this.props.handleGetRequest('true')
  }

  onAlertDismissed = (alert) => {
    const alerts = this.state.alerts
    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert)
    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)],
      })
    }
  }
  generate = (msg) => {
    const newAlert = {
      id: (new Date()).getTime(),
      type: 'info',
      headline: 'Notification:',
      message: msg,
    }
    this.setState({
      alerts: [...this.state.alerts, newAlert],
    })
  }

  toggleBtnReject = (data) => {
    this.setState({
      modalReject: !this.state.modalReject,
      reqReject: data
    })
  }
  toggleBtnApprove = (data) => {
    this.setState({
      modalApprove: !this.state.modalApprove,
      reqApprove: data
    })
  }
  handleRejectReq = async () => {
    const { reqReject, selectedCandidateAddress } = this.state
    console.log(selectedCandidateAddress)
    await this.props.handleRejectRequest(reqReject.id)
    await this.props.handleReGetRequest('true')
    this.setState({
      modalReject: !this.state.modalReject,
    })
    this.generate(`Request from '${reqReject.name}' has been rejected!`)
  }
  handleApproveReq = async () => {
    const { reqApprove } = this.state
    await this.props.handlApproveRequest(reqApprove.id)
    await this.props.handleReGetRequest('true')
    this.setState({
      modalApprove: !this.state.modalApprove,
    })
    this.generate(`Request from '${reqApprove.name}' has been approved!`)
  }
  toggleUpdateView = (data) => {
    const { adminData } = this.props
    const reqUpdated = adminData.requests.filter(req =>
      req._id === data.id
    )
    if (reqUpdated.length > 0) {
      this.setState({
        displayMain: !this.state.displayMain,
        reqUpdatedData: reqUpdated[0]
      })
    }
  }
  editBack = () => {
    this.setState({
      displayMain: !this.state.displayMain
    })
    this.props.handleReGetRequest('true')
  }
  render() {
    const { reqReject, reqApprove, displayMain, reqUpdatedData, filteredValue } = this.state
    const { adminData } = this.props
    return (
      <div>
        {displayMain ?
          (<Row>
            <Col xs="12" xl={{ size: 10, offset: 1 }}>
              <AlertList
                position={this.state.position}
                alerts={this.state.alerts}
                timeout={this.state.timeout}
                dismissTitle="Begone!"
                onDismiss={this.onAlertDismissed}
              />
              {
                adminData.isFetching ? (
                  <BeatLoader
                    className={override}
                    sizeUnit={'px'}
                    size={15}
                    color={'#36D7B7'}
                  />)
                  :
                  (
                    <div>
                      {adminData.errMsg === '' ?
                        (<div >
                          <ReactTable
                            getTdProps={() => (
                              {
                                style: {
                                  display: 'flex',
                                  flexDirection: 'column',
                                  justifyContent: 'center',
                                },
                              })}
                            data={makeData(adminData.requests)}
                            filterable
                            filtered={filteredValue}
                            onFilteredChange={filtered => this.setState({ filteredValue: filtered })}
                            defaultFilterMethod={(filter, row) =>
                              String(row[filter.id]).toLowerCase().includes(filter.value.toLowerCase())}
                            columns={[{
                              Header: '#',
                              accessor: 'no',
                              maxWidth: 60,
                              ...ColumnCenterAlign,
                              filterable: false
                            },
                            {
                              Header: 'Name',
                              accessor: 'name',
                              ...ColumnLeftAlign,
                            },
                            {
                              Header: 'Requester Email',
                              accessor: 'email',
                              ...ColumnLeftAlign,
                            },
                            {
                              Header: 'Phone Number',
                              accessor: 'phone',
                              ...ColumnLeftAlign,
                            },
                            {
                              Header: 'Status',
                              accessor: 'status',
                              width: 150,
                              ...ColumnCenterAlign,
                              filterMethod: (filter, row) => {
                                if (filter.value === 'pending') {
                                  return row[filter.id] === 'pending'
                                }
                                if (filter.value === 'reject') {
                                  return row[filter.id] === 'reject'
                                }
                                if (filter.value === 'accept') {
                                  return row[filter.id] === 'accept'
                                }
                                return true
                              },
                              Cell: row => (
                                <div className="text-center">
                                  <StatusTag status={row.value} />
                                </div>
                              ),
                              Filter: ({ filter, onChange }) => {
                                return (
                                  <select
                                    onChange={event => onChange(event.target.value)}
                                    style={{ width: '100%' }}
                                    value={filter ? filter.value : 'pending'}
                                  >
                                    <option value="pending">Pending</option>
                                    <option value="reject">Rejected</option>
                                    <option value="accept">Approved</option>
                                    <option value="all">All</option>
                                  </select>
                                )
                              }
                            },
                            {
                              Header: 'Action',
                              accessor: 'data',
                              width: 150,
                              filterable: false,
                              sortable: false,
                              Cell: row => (
                                <div className="text-center">
                                  <Button color="primary" style={actionBtn} onClick={() => this.toggleUpdateView(row.value)} outline>Details</Button>
                                </div>
                              ),
                            },
                            ]}
                            defaultPageSize={10}
                            className="-striped -highlight templateTable"
                            pageSizeOptions={setPageSizeOption(adminData.requests.length)}
                          />
                          <Tips />
                        </div>
                        ) : (<p>{adminData.errMsg}</p>)
                      }
                    </div>
                  )}
            </Col>
          </Row>
          )
          : (
            <div>
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <Button color="primary" size="lg" onClick={this.editBack}>
                    <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
                    Back
                  </Button>
                  <br /><br />
                </Col>
              </Row >
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <UpdateRequest data={reqUpdatedData} />
                </Col>
              </Row >
            </div>
          )}
        <Modal isOpen={this.state.modalReject} toggle={this.toggleBtnReject} centered>
          <ModalHeader toggle={this.toggleBtnReject}>Reject Request</ModalHeader>
          <ModalBody>
            <p>Are you sure to reject request from &#34;{reqReject.name}&#34; ?</p>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.handleRejectReq} style={actionBtn}>Reject</Button>{' '}
            <Button color="secondary" onClick={this.toggleBtnReject} style={actionBtn}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.modalApprove} toggle={this.toggleBtnApprove} centered>
          <ModalHeader toggle={this.toggleBtnApprove}>Approve Request</ModalHeader>
          <ModalBody>
            <p>Are you sure to approve request from &#34;{reqApprove.name}&#34; ?</p>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleApproveReq} style={actionBtn}>Approve</Button>{' '}
            <Button color="secondary" onClick={this.toggleBtnApprove} style={actionBtn}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    adminData: state.adminRequestReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetRequest: flag => dispatch(getRequestToBeIssuer(flag)),
    handleReGetRequest: flag => dispatch(reGetRequestToBeIssuer(flag)),
    handleRejectRequest: reqId => dispatch(rejectRequest(reqId)),
    handlApproveRequest: reqId => dispatch(approveRequest(reqId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageRequest)

