const createReq = (req, index) => ({
  no: index + 1,
  name: req.fullname,
  email: req.requesterEmail,
  phone: req.phone,
  status: req.status,
  data: {
    id: req._id,
    status: req.status,
    name: req.fullname,
  },
  ethereumAccount: req.ethereumAccount
})

export function makeData(arrReq) {
  const result = []
  for (let index = 0; index < arrReq.length; index += 1) {
    const element = createReq(arrReq[index], index)
    result.push(element)
  }
  return result
}
