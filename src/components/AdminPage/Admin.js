import React, { Component } from 'react'
import styled from 'styled-components'
import ManageRequest from './ManageRequest/ManageRequest'
import ManageIssuer from './ManageIssuer/ManageIssuer'
import arrTabs from './arrTabs'
import { Route, Switch, NavLink, Redirect } from 'react-router-dom'
import * as Color from './../../constants/Color'
import { TabContent, Nav, NavItem, Row, Col } from 'reactstrap'

const HomeWrapper = styled.section`
  padding-left: 1%
  background: ${Color.LV1}
`
const rowStyle = {
  minHeight: '750px',
}
const colStyle = {
  background: Color.LV1,
}
const tabStyle = {
  padding: '30px 15px',
}

class Admin extends Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      activeTab: '1',
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      })
    }
  }
  showContentMenu(arr) {
    const { path } = this.props.match
    let result = null
    if (arr.length > 0) {
      result = arr.map((nav, index) => (
        <NavItem key={index} className="NavItem-Recipient">
          <NavLink key={index} className="nav-link" to={`${path}${nav.to}`} onClick={() => { this.toggle(nav.id) }}>
            {nav.title}
          </NavLink>
        </NavItem>

      ))
    }
    return result
  }
  render() {
    const { match } = this.props
    const { path } = match
    return (

      <HomeWrapper>
        <Row style={rowStyle}>
          <Col style={colStyle} md="12" lg={{ size: 12, offset: 0 }} >
            <Nav tabs className="mr-auto">
              <div className="mx-auto d-sm-flex d-block flex-sm-nowrap">
                {this.showContentMenu(arrTabs)}
              </div>
            </Nav>
            <TabContent activeTab={this.state.activeTab} style={tabStyle}>
              <Switch>
                <Route path={`${path}/manage-request`} component={ManageRequest} />
                <Route path={`${path}/manage-issuer`} component={ManageIssuer} />
                <Redirect from={`${path}`} to={`${path}/manage-request`} />
              </Switch>
            </TabContent>
          </Col>
        </Row>
      </HomeWrapper>
    )
  }

}

export default (Admin)

