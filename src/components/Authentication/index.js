import React from 'react'
import PropTypes from 'prop-types'
import { Modal } from 'reactstrap'
import styled from 'styled-components'
import Cookies from 'universal-cookie'
import { connect } from 'react-redux'
import Icon from './../form/icon'
import Login from './login'
import SignUp from './signup'
import ResetPassword from './reset-password'
import {
  getMessageByCode, DISPLAY_TYPES, CHECK_MAIL_ACTIVE_ACCOUT, PASS_NOT_SATISFY, USER_NOT_FOUND,
  INVALID_EMAIL, INVALID_CREDENTIALS, ACCOUNT_IS_NOT_ACTIVATED, SEND_EMAIL_RESET_PASS_MSG
} from './utils'
import NavBar from './nav-bar'
import * as Color from './../../constants/Color'
import { HeaderDiv, LoaderDiv, MessageDiv } from './common'
import { createUser, login, resetPass } from './../../actions/AuthenAction'


const cookies = new Cookies()

const BackButton = styled.div.attrs({
  className: 'd-flex justify-content-center align-items-center',
})`
  background: rgb(255, 255, 255);
  width: 20px;
  height: 20px;
  border-radius: 9px;
  position: absolute;
  top: 5%;
  left: 5%;
  cursor: pointer;
`
const LogoText = styled.h1`
font-size: ${props => props.fontSize || '40px'};
text-align: center;
width: fit-content;
margin: 0px auto;
display: block;
color :  ${Color.LV1};
`
class AuthenticationModal extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      display: DISPLAY_TYPES.LOG_IN,
      loading: false,
      message: null,
      modal: true,
    }
  }

  changeDisplay = (display) => {
    this.setState({
      display,
      message: null,
    })
  }

  changeLoading = (loading) => {
    this.setState({
      loading,
    })
  }

  changeMessage = (message) => {
    this.setState({
      message,
    })
  }

  toggleModal = () => {
    this.setState({
      message: null,
      display: DISPLAY_TYPES.LOG_IN,
    })
    this.props.toggle()
  }
  createUser = async (user) => {
    await this.props.handleCreateUser(user)
    await this.changeLoading(false)
    const { authen } = this.props
    if (authen.createFlag) {
      this.changeMessage(getMessageByCode(CHECK_MAIL_ACTIVE_ACCOUT))
      this.setState({
        display: DISPLAY_TYPES.LOG_IN,
      })
    } else if (authen.errMsg === INVALID_EMAIL) {
      this.changeMessage(getMessageByCode(INVALID_EMAIL))
    } else {
      this.changeMessage(getMessageByCode(PASS_NOT_SATISFY))
    }
  }
  resetPass = async (email) => {
    await this.props.handleResetPass(email)
    await this.changeLoading(false)
    const { authen } = this.props
    if (authen.resetFlag) {
      this.changeMessage(getMessageByCode(SEND_EMAIL_RESET_PASS_MSG))
      this.setState({
        display: DISPLAY_TYPES.LOG_IN,
      })
    } else {
      this.changeMessage(getMessageByCode(USER_NOT_FOUND))
    }
  }

  login = async (user) => {
    await this.props.handleLogin(user)
    await this.changeLoading(false)
    const { authen } = this.props
    if (authen.loginFlag) {
      cookies.set('token', authen.tokenData, {
        maxAge: 24 * 3600,
        path: '/'
      })
      this.toggleModal()
    } else if (authen.errCode === 401) {
      this.changeMessage(getMessageByCode(INVALID_CREDENTIALS))
    } else {
      this.changeMessage(getMessageByCode(ACCOUNT_IS_NOT_ACTIVATED))
    }
  }
  render() {
    const { modal } = this.props
    const { display, loading, message } = this.state
    let mainDisplay
    let loaderDivHeight
    if (display === DISPLAY_TYPES.LOG_IN) {
      mainDisplay = (<Login
        changeDisplay={this.changeDisplay}
        toggleModal={this.toggleModal}
        loading={loading}
        changeLoading={this.changeLoading}
        changeMessage={this.changeMessage}
        login={this.login}
      />)
      loaderDivHeight = '263px'
    } else if (display === DISPLAY_TYPES.SIGN_UP) {
      mainDisplay = (<SignUp
        toggleModal={this.toggleModal}
        loading={loading}
        changeLoading={this.changeLoading}
        changeMessage={this.changeMessage}
        createUser={this.createUser}
      />)
      loaderDivHeight = '355px'
    } else {
      mainDisplay = (<ResetPassword
        toggleModal={this.toggleModal}
        loading={loading}
        changeLoading={this.changeLoading}
        changeMessage={this.changeMessage}
        resetPass={this.resetPass}
      />)
      loaderDivHeight = '129px'
    }

    const isResetPasswordDisplay = display === DISPLAY_TYPES.RESET_PASS

    return (
      <div>
        <Modal isOpen={modal} toggle={this.toggleModal} style={{ width: '400', top: '35px' }} >
          <div>
            <HeaderDiv divHeight={isResetPasswordDisplay ? '126px' : '104px'}>
              {isResetPasswordDisplay &&
                <BackButton onClick={() => this.changeDisplay(DISPLAY_TYPES.LOG_IN)}>
                  <Icon name="fas fa-arrow-left" iconSize="10px" />
                </BackButton>}
              <LogoText>CertNet</LogoText>
              {/* <img style={{ width: 69, height: 50 }} alt="BitScreenerIcon" src={'/static/icon/BitScreenerIcon.png'} /> */}
              {isResetPasswordDisplay &&
                <span style={{ fontSize: 20, color: '#ffffff', position: 'absolute', top: 85 }}>Reset your password</span>}
            </HeaderDiv>
            {message &&
              <MessageDiv background={message.background} dangerouslySetInnerHTML={{ __html: message.content }} />
            }
            {loading && <LoaderDiv divHeight={loaderDivHeight} />}

            {!isResetPasswordDisplay &&
              <NavBar display={display} changeDisplay={this.changeDisplay} />}
            {mainDisplay}
          </div>
        </Modal>
      </div>
    )
  }
}

AuthenticationModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  return {
    authen: state.authenReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleCreateUser: async (data) => { await dispatch(createUser(data)) },
    handleLogin: async (data) => { await dispatch(login(data)) },
    handleResetPass: async (data) => { await dispatch(resetPass(data)) },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationModal)
