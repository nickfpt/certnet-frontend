import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavItem } from 'reactstrap'
import { DISPLAY_TYPES } from './utils'

const NavLink = styled.a.attrs({
  className: 'nav-link',
})`
  border-bottom: ${props => props.active ? '1px solid #232f3e' : '1px solid #E1E1E1'};
  border-radius: 0 !important;
  color: #333;
  .night-mode & {
    border-bottom: ${props => props.active ? '1px solid #E1E1E1' : '1px solid #232f3e'};
  }
`

const NavBar = ({ display, changeDisplay }) => (
  <ul className="nav nav-pills nav-fill">
    <NavItem>
      <NavLink
        active={display === DISPLAY_TYPES.LOG_IN}
        onClick={() => changeDisplay(DISPLAY_TYPES.LOG_IN)}
      >Login</NavLink>
    </NavItem>
    <NavItem>
      <NavLink
        active={display === DISPLAY_TYPES.SIGN_UP}
        onClick={() => changeDisplay(DISPLAY_TYPES.SIGN_UP)}
      >Sign Up</NavLink>
    </NavItem>
  </ul>
)

NavBar.propTypes = {
  display: PropTypes.string.isRequired,
  changeDisplay: PropTypes.func.isRequired,
}

export default NavBar

