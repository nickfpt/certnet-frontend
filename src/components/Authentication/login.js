import React from 'react'
import PropTypes from 'prop-types'
import Icon from './../form/icon'
import { EmailPassword, textInputStyle, SubmitButton } from './common'

import { DISPLAY_TYPES } from './utils'

class LoginForm extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      error: null,
    }
  }

  onSubmit = async (event) => {
    event.preventDefault()

    this.props.changeLoading(true)
    const user = {
      email: this.state.email,
      password: this.state.password,
    }
    await this.props.login(user)
  }


  render() {
    const { changeDisplay, loading } = this.props

    const {
      email,
      password,
      error,
    } = this.state

    return (<div style={{ paddingTop: '15px' }}>
      <div>{error && error.code}</div>
      <div>{error && error.message}</div>
      <form onSubmit={this.onSubmit}>
        <div className="d-flex justify-content-center pt-3">
          <EmailPassword>
            <Icon name="far fa-envelope" color="#363C4E" iconSize="1.6em" />
          </EmailPassword>
          <input
            style={textInputStyle}
            value={email}
            onChange={event => this.setState({
              email: event.target.value,
            })}
            type="email"
            name="email"
            placeholder="yours@example.com"
            required
            autoFocus
          />
        </div>
        <div className="d-flex justify-content-center pt-2">
          <EmailPassword>
            <Icon name="fas fa-lock" color="#363C4E" iconSize="1.6em" />
          </EmailPassword>
          <input
            style={textInputStyle}
            value={password}
            onChange={event => this.setState({
              password: event.target.value,
            })}
            type="password"
            name="password"
            placeholder="Your password"
            required
          />
        </div>
        <div className="d-flex justify-content-center pt-3">
          <span tabIndex="0" role="link" style={{ cursor: 'pointer', fontSize: '14px' }} onClick={() => changeDisplay(DISPLAY_TYPES.RESET_PASS)}>
            Don&apos;t remember your password?
          </span>
        </div>
        <div className="d-flex justify-content-center pt-3">
          {!loading
            ?
              <SubmitButton>
              LOG IN
                <Icon name="fas fa-chevron-right" color="#ffffff" iconSize="0.8em" marginLeft="6px" />
              </SubmitButton> :
            // eslint-disable-next-line
            <div style={{ width: '100%', height: '50px', background: '#EEEEEE' }}>
              {/* eslint-disable-next-line  */}
              <div style={{ fontSize: 5, marginTop: 16, marginBottom: 0 }} className="customized-loader" />
            </div>}
        </div>
      </form>
    </div>)
  }
}

LoginForm.propTypes = {
  changeDisplay: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  changeLoading: PropTypes.func.isRequired,
}

export default LoginForm

