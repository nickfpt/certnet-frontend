import React from 'react'
import Icon from './../form/icon'
import PropTypes from 'prop-types'
// import { auth } from '~/firebase'
import { EmailPassword, textInputStyle, SubmitButton } from './common'
// import { DISPLAY_TYPES, SEND_EMAIL_RESET_PASS_MSG, getMessageByCode } from './utils'

class ResetPassword extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
    }
  }

  onSubmit = async (event) => {
    event.preventDefault()
    this.props.changeLoading(true)
    await this.props.resetPass(this.state.email)
  }

  render() {
    const {
      email,
    } = this.state
    const { loading } = this.props
    return (<div style={{ paddingTop: '15px' }}>
      <div className="d-flex justify-content-center pt-3">
        <span style={{ color: '#6B6868', width: 255 }}>Please enter your email address. We will send you an email to reset your password.</span>
      </div>
      <form onSubmit={this.onSubmit}>
        <div className="d-flex justify-content-center pt-3">
          <EmailPassword>
            <Icon name="far fa-envelope" color="#ffffff" iconSize="1.6em" />
          </EmailPassword>
          <input
            style={textInputStyle}
            value={email}
            onChange={event => this.setState({
              email: event.target.value,
            })}
            type="email"
            name="email"
            placeholder="yours@example.com"
            required
            autoFocus
          />
        </div>
        <div className="d-flex justify-content-center pt-3">
          {!loading
            ?
            <SubmitButton>
              SEND EMAIL
              <Icon name="fas fa-chevron-right" color="#ffffff" iconSize="0.8em" marginLeft="6px" />
            </SubmitButton>
            :
            <div style={{ width: '100%', height: '50px', background: '#EEEEEE' }}>
              <div style={{ fontSize: 5, marginTop: 16, marginBottom: 0 }} className="customized-loader" />
            </div>
          }
        </div>
      </form>
    </div>)
  }
}

ResetPassword.propTypes = {
  loading: PropTypes.bool.isRequired,
  changeLoading: PropTypes.func.isRequired,
}

export default ResetPassword

