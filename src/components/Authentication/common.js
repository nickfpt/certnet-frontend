import styled from 'styled-components'

export const LogoDiv = styled.div.attrs({
  className: 'd-flex justify-content-center align-items-center',
})`
  height: 40px;
  width: 40px;
  box-sizing: border-box;
  border-radius: 3px 0 0 3px;
`

export const TextDiv = styled.div.attrs({
  className: 'd-flex align-items-center',
})`
  padding-left: 15px;
  font-size: 14px;
  height: 40px;
  width: 215px;
  color: #ffffff;
  border-radius: 0 3px 3px 0;
`

export const EmailPassword = styled(LogoDiv)`
  background: #EEEEEF;
`

export const SubmitButton = styled.button.attrs({
  className: 'btn',
  type: 'submit',
})`
  background: #1485BF;
  width: 100%;
  color: #ffffff;
  border-radius: 0;
  height: 50px;
`

export const textInputStyle = {
  width: 350,
  paddingLeft: 15,
  border: '1px solid #EEEEEF',
}

export const passwordPolicy = {
  margin: '15px 0 0 50px',
}


export const HeaderDiv = styled.div.attrs({
  className: 'd-flex justify-content-center',
})`
  width: 100%;
  height: ${props => props.divHeight};
  background: #222;
  padding-top: 30px;
`

export const MessageDiv = styled.div`
  min-height: 36px;
  background: ${props => props.background};
  color: #ffffff;
  vertical-align: middle;
  text-align: center;
  padding: 6px;
`


export const LoaderDiv = styled.div`
  background: #fff;
  position: absolute;
  height: ${props => props.divHeight};
  width: 100%;
  opacity: 0.7;
  z-index: 1000;
  .night-mode & {
    background: #2A3136;
  }
`
