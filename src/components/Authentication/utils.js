export const DISPLAY_TYPES = {
  LOG_IN: 'Log in',
  SIGN_UP: 'Sign up',
  RESET_PASS: 'Reset password',
}

export const PROVIDERS = {
  FACEBOOK: 'Facebook',
  TWITTER: 'Twitter',
  GOOGLE: 'Google',
}

const COLOR = {
  RED: '#F44336',
  GREEN: '#7ED222',
}

// addition message to handle
export const SEND_EMAIL_RESET_PASS_MSG = 'SEND_EMAIL_RESET_PASS_MSG'
export const CHECK_MAIL_ACTIVE_ACCOUT = 'CHECK_MAIL_ACTIVE_ACCOUT'
export const CF_PASSWORD_DOES_NOT_MATCH = 'CF_PASSWORD_DOES_NOT_MATCH'
export const PASS_NOT_SATISFY = 'PASS_NOT_SATISFY'
export const INVALID_EMAIL = 'Invalid email'
export const INVALID_CREDENTIALS = 'Invalid credentials'
export const ACCOUNT_IS_NOT_ACTIVATED = 'ACCOUNT_IS_NOT_ACTIVATED'
export const USER_NOT_FOUND = 'USER_NOT_FOUND'

const MESSAGES = [
  {
    code: INVALID_CREDENTIALS,
    content: 'You have given a wrong email or password.',
    background: COLOR.RED,
  },

  {
    code: USER_NOT_FOUND,
    content: 'User not found, please check again.',
    background: COLOR.RED,
  },
  {
    code: ACCOUNT_IS_NOT_ACTIVATED,
    content: 'Your account has not been activated. Please check your email.',
    background: COLOR.RED,
  },
  {
    code: INVALID_EMAIL,
    content: 'This email has already been registered.',
    background: COLOR.RED,
  },
  {
    code: PASS_NOT_SATISFY,
    content: 'Password does not satisfy password policy.',
    background: COLOR.RED,
  },
  {
    code: 'auth/too-many-requests',
    content: 'We have blocked all requests from this account due to unusual activity. Try again later.',
    background: COLOR.RED,
  },
  {
    code: SEND_EMAIL_RESET_PASS_MSG,
    content: 'An email to reset your password has been sent!',
    background: COLOR.GREEN,
  },
  {
    code: CF_PASSWORD_DOES_NOT_MATCH,
    content: 'Password does not match the confirm password.',
    background: COLOR.RED,
  },
  {
    code: CHECK_MAIL_ACTIVE_ACCOUT,
    content: 'Your account has been created! <br/> Please check your email to activate your account!',
    background: COLOR.GREEN,
  },
]

export const getMessageByCode = code => MESSAGES.filter(message => message.code === code)[0]
