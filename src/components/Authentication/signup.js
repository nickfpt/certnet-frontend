/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'
import Icon from './../form/icon'
// import { auth } from '~/firebase'
import { EmailPassword, textInputStyle, SubmitButton, passwordPolicy } from './common'
import { getMessageByCode, CF_PASSWORD_DOES_NOT_MATCH } from './utils'

class SignUpForm extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      cfpassword: '',
      ppFlag: false,
      error: null,
    }
  }
  onSubmit = async (event) => {
    event.preventDefault()
    const {
			email,
			password,
			cfpassword,
		} = this.state
    const user = {
      email,
      password,
    }
    if (password !== cfpassword) {
      this.props.changeMessage(getMessageByCode(CF_PASSWORD_DOES_NOT_MATCH))
    } else {
      this.props.changeLoading(true)
      await this.props.createUser(user)
    }
  }
  togglePolicy = () => {
    this.setState({
      ppFlag: !this.state.ppFlag,
    })
  }


  render() {
    const {
			email,
			password,
			cfpassword,
			ppFlag,
		} = this.state

    const { loading } = this.props

    return (<div style={{ paddingTop: '15px' }}>

      <form onSubmit={this.onSubmit}>
        <div className="d-flex justify-content-center pt-3">
          <EmailPassword>
            <Icon name="far fa-envelope" color="#363C4E" iconSize="1.6em" />
          </EmailPassword>
          <input
            autoComplete="off"
            style={textInputStyle}
            value={email}
            onChange={event => this.setState({
              email: event.target.value,
            })}
            autoFocus
            type="email"
            name="email"
            placeholder="yours@example.com"
            required
          />
        </div>
        <div className="d-flex justify-content-center pt-2">
          <EmailPassword>
            <Icon name="fas fa-lock" color="#363C4E" iconSize="1.6em" />
          </EmailPassword>
          <input
            style={textInputStyle}
            value={password}
            onChange={event => this.setState({
              password: event.target.value,
            })}
            type="password"
            name="password"
            placeholder="Your password"
            required
          />
        </div>
        <div className="d-flex justify-content-center pt-2">
          <EmailPassword>
            <Icon name="fas fa-check-square" color="#363C4E" iconSize="1.6em" />
          </EmailPassword>
          <input
            style={textInputStyle}
            value={cfpassword}
            onChange={event => this.setState({
              cfpassword: event.target.value,
            })}
            type="password"
            name="cfpassword"
            placeholder="Confirm password"
            required
          />
        </div>
        <div style={passwordPolicy}>
          <label className="hover-i" onClick={this.togglePolicy} style={{ fontSize: '14px' }}>Password policy
          </label>
          {ppFlag && <ul>
            <li>Be at least eight characters in length</li>
            <li>Contain uppercase characters (A through Z)</li>
            <li>Contain lowercase characters (a through z)</li>
            <li>Contain base 10 digits (0 through 9)</li>
          </ul>}

        </div>

        <div className="d-flex justify-content-center pt-3">
          {!loading ?
            <SubmitButton>
              SIGN UP
              <Icon name="fas fa-chevron-right" color="#ffffff" iconSize="0.8em" marginLeft="6px" />
            </SubmitButton> :
            <div style={{ width: '100%', height: '50px', background: '#EEEEEE' }}>
              <div style={{ fontSize: 5, marginTop: 16, marginBottom: 0 }} className="customized-loader" />
            </div>
          }
        </div>
      </form>
    </div>)
  }
}

SignUpForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  changeLoading: PropTypes.func.isRequired,
  changeMessage: PropTypes.func.isRequired,
}

export default SignUpForm

