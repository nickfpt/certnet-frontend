import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import moment from 'moment'
import { ROLES } from './../../../constants/auth'
import jwt from 'jsonwebtoken'
import Cookies from 'universal-cookie'
import {
  Row, Col, Card, Button, CardHeader, CardBody, Table, Input, Modal, ModalHeader, ModalBody, ModalFooter, Form, Alert
} from 'reactstrap'
import { getCurrentUser, changePassword, updateEthRecipient } from './../../../actions/AccountAction'
import ImportKeyModal from './ImportKeyModal'
import RequestIssuer from './RequestIssuer'
import * as Color from '../../../constants/Color'
import Loader from '../../layout/Loader'
import Icon from '../../form/icon'

const cookies = new Cookies()

const HomeWrapper = styled.section`
padding-top: 50px;
  padding-left: 1%;
  background: ${Color.LV1};
`
const colStyle = {
  background: Color.LV1,
}

const actionBtn = {
  width: '90px',
  margin: '0px 5px',
}

class RecipientProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dob: moment('20111031', 'YYYYMMDD'),
      changePass: false,
      selectedAddress: null,
      importModal: false,
      ppFlag: false,
      email: '',
      modalRemoveETH: false,
      removeETHaddress: '',
      arrETHdata: [],
      changeInputFlag: false,
      changeETHFlag: false,
      curPass: '',
      newPass: '',
      cfPass: '',
      errFlag: false,
      errMsg: '',
      successMsg: '',
      successFlag: false,
      requestToBeIssuer: false,
      modalAlertRequest: false,
      displayRequest: false
    }
  }
  componentDidMount = async () => {
    await this.props.handleGetCurrentUser()
    const arrETHdata = []
    const { email, recipientEthereumAccounts, requestToBeIssuer } = this.props.accountData.user
    if (recipientEthereumAccounts.length > 0) {
      recipientEthereumAccounts.forEach((element) => {
        arrETHdata.push(element.address)
      })
    }
    await this.setState({
      email,
      arrETHdata,
      requestToBeIssuer,
    })
  }

  onChangePass = (e) => {
    this.setState({
      changePass: e.target.checked,
      curPass: '',
      newPass: '',
      cfPass: '',
      errFlag: false,
      successFlag: false,
    })
    if (!e.target.checked) {
      this.setState({
        changeInputFlag: false
      })
    }
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      changeInputFlag: true,
      errFlag: false,
      successFlag: false,
    })
  }
  convertToUpdateETHaccount = (arr) => {
    const result = []
    arr.forEach((element) => {
      const obj = {
        address: element
      }
      result.push(obj)
    })
    return result
  }
  displayETHaddress = (arr) => {
    let result = null
    if (arr.length > 0) {
      result = arr.map(element => (
        <li className="eth-list" key={element}>{element}
          <span className="close" onClick={() => this.handleClickClose(element)}>&times;</span>
        </li>
      ))
    }
    return result
  }
  handleClickClose = (ETHadress) => {
    this.setState({
      removeETHaddress: ETHadress
    })
    this.toggleBtnRemove()
  }
  toggleBtnRemove = () => {
    this.setState({
      modalRemoveETH: !this.state.modalRemoveETH,
    })
  }
  handleRemoveETH = () => {
    const { arrETHdata, removeETHaddress } = this.state
    const index = arrETHdata.indexOf(removeETHaddress)
    arrETHdata.splice(index, 1)
    this.setState({
      removeETHaddress: '',
      changeETHFlag: true,
    })
    this.toggleBtnRemove()
  }
  handleSelectETHChange = (selectedOption) => {
    this.setState({ selectedAddress: selectedOption })
  }

  toggleConfirmationModal = () => {
    this.setState({
      importModal: !this.state.importModal,
    })
  }
  togglePolicy = () => {
    this.setState({
      ppFlag: !this.state.ppFlag,
    })
  }
  handleAddEthAddress = (ethAcc) => {
    this.setState({
      arrETHdata: [ethAcc, ...this.state.arrETHdata],
      changeETHFlag: true
    })
  }
  handleSubmitChange = async (e) => {
    e.preventDefault()
    const { changePass, curPass, newPass, cfPass, changeETHFlag } = this.state
    // change pass only
    if (changePass) {
      let updateEthFlag = false
      if (newPass !== cfPass) {
        await this.setState({
          errFlag: true,
          errMsg: 'New password does not match the confirm password.'
        })
      } else {
        const data = {
          oldPassword: curPass,
          newPassword: newPass
        }
        await this.props.handleChangePass(data)
        if (this.props.accountData.resultFlag) {
          updateEthFlag = true
          this.setState({
            successFlag: true,
            successMsg: 'Password has been changed!',
            changePass: false,
            curPass: '',
            newPass: '',
            cfPass: '',
          })
        } else {
          await this.setState({
            errFlag: true,
            errMsg: `${this.props.accountData.error}`
          })
        }
      }
      if (changeETHFlag && updateEthFlag) {
        const data = {
          addresses: this.convertToUpdateETHaccount(this.state.arrETHdata)
        }
        await this.props.handleUpdateEthRecipient(data)

        if (this.props.accountData.updateFlag) {
          this.setState({
            successMsg: 'Update successful! Password has been changed!',
            changeETHFlag: false,
          })
        }
      }
    } else if (changeETHFlag) {
      const data = {
        addresses: this.convertToUpdateETHaccount(this.state.arrETHdata)
      }
      await this.props.handleUpdateEthRecipient(data)
      if (this.props.accountData.updateFlag) {
        this.setState({
          successMsg: 'Update successful!',
          changeETHFlag: false,
          successFlag: true,
        })
      }
    }
  }
  convertToUpdateETHaccount = (arr) => {
    const result = []
    arr.forEach((element) => {
      const obj = {
        address: element
      }
      result.push(obj)
    })
    return result
  }
  toggleAlertRequest = () => {
    this.setState({
      modalAlertRequest: !this.state.modalAlertRequest
    })
  }
  handleRequestIssuer = () => {
    if (this.state.requestToBeIssuer) {
      this.toggleAlertRequest()
    } else {
      this.setState({
        displayRequest: true
      })
    }
  }
  editBack = async () => {
    await this.props.handleGetCurrentUser()
    // update requestToBeIssuer's status
    const { requestToBeIssuer } = this.props.accountData.user
    this.setState({
      displayRequest: false,
      requestToBeIssuer
    })
  }
  render() {
    const { importModal, ppFlag, email, arrETHdata, removeETHaddress, changeInputFlag, changeETHFlag, curPass,
      newPass, cfPass, errFlag, errMsg, successFlag, successMsg, displayRequest } = this.state
    const { accountData } = this.props
    const tokenData = cookies.get('token')

    let decodedToken = null
    try {
      decodedToken = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      decodedToken = null
    }
    return (
      <HomeWrapper>
        {!displayRequest ?
          (<Row>
            <Col style={colStyle} xs="12" md={{ size: 8, offset: 2 }} >
              <Card>
                <CardHeader>Account Information</CardHeader>
                {
                  accountData.isFetching ? (<Loader />)
                    : (
                      <CardBody>
                        {errFlag && <Alert color="danger">{errMsg} </Alert>}
                        {successFlag && <Alert color="primary">{successMsg} </Alert>}
                        <Form onSubmit={this.handleSubmitChange}>
                          <Row>
                            <Col md="12" >
                              <Table borderless >
                                <tbody>
                                  <tr>
                                    <td width="20%">Email</td>
                                    <td><Input type="email" name="email" id="exampleEmail" value={email} disabled /></td>
                                  </tr>
                                  <tr>
                                    <td>Ethereum Addresses</td>
                                    <td>
                                      <ul style={{ padding: '0px' }}>
                                        {this.displayETHaddress(arrETHdata)}
                                      </ul>
                                      <Button color="primary" size="md" outline onClick={this.toggleConfirmationModal}>Add Address</Button>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td />
                                    <td>
                                      <div className="pretty p-svg p-curve">
                                        <input type="checkbox" onChange={this.onChangePass} checked={this.state.changePass} />
                                        <div className="state p-primary ">
                                          <svg className="svg svg-icon" viewBox="0 0 20 20">
                                            <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style={{ stroke: 'white', fill: 'white' }} />
                                          </svg>
                                          <label>Change Password</label>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                              {this.state.changePass ? (
                                <Table borderless>
                                  <tbody>
                                    <tr>
                                      <td width="20%">Current Password</td>
                                      <td>
                                        <Input type="password" name="curPass" value={curPass} onChange={this.handleInputChange} required />
                                      </td>
                                    </tr >
                                    <tr>
                                      <td>New Password</td>
                                      <td>
                                        <Input type="password" name="newPass" value={newPass} onChange={this.handleInputChange} required />
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Confirm Password</td>
                                      <td>
                                        <Input type="password" name="cfPass" value={cfPass} onChange={this.handleInputChange} required />
                                      </td>
                                    </tr>
                                    <tr>
                                      <td />
                                      <td>
                                        <div>
                                          <label className="hover-i" onClick={this.togglePolicy} style={{ fontSize: '14px' }}>Password policy </label>
                                          {ppFlag && <ul>
                                            <li>Be at least eight characters in length</li>
                                            <li>Contain uppercase characters (A through Z)</li>
                                            <li>Contain lowercase characters (a through z)</li>
                                            <li>Contain base 10 digits (0 through 9)</li>
                                          </ul>}
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </Table>
                              ) : null}
                              <div className="text-center">
                                <Button color="danger" size="lg" type="submit" disabled={!(changeInputFlag || changeETHFlag)}>Update</Button>
                                {decodedToken && decodedToken.roles.includes(ROLES.RECIPIENT) && !decodedToken.roles.includes(ROLES.ISSUER) &&
                                  <Button color="primary" size="lg" type="submit" outline style={{ marginLeft: '20px' }} onClick={this.handleRequestIssuer}>Request to Become Issuer</Button>
                                }
                              </div>
                            </Col>
                          </Row>
                        </Form>
                      </CardBody>

                    )
                }
              </Card>
            </Col>
          </Row>
          ) : (
            <div>
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <Button color="primary" size="lg" onClick={this.editBack}>
                    <Icon name="fas fa-long-arrow-alt-left" color="#ffffff" iconSize="0.8em" marginRight="6px" />
                    Back
                  </Button>
                  <br /><br />
                </Col>
              </Row >
              <Row>
                <Col md={{ size: 10, offset: 1 }} >
                  <RequestIssuer email={email} />
                </Col>
              </Row >
            </div>
          )}
        {importModal && <ImportKeyModal
          modal={importModal}
          toggle={this.toggleConfirmationModal}
          addresses={arrETHdata}
          addAddress={this.handleAddEthAddress}
          onlyOne={false}
        />}
        <Modal isOpen={this.state.modalRemoveETH} toggle={this.toggleBtnRemove} centered>
          <ModalHeader toggle={this.toggleBtnRemove}>Remove Ethereum Address</ModalHeader>
          <ModalBody>
            <p>Are you sure to remove address &#34;{removeETHaddress}&#34; ?</p>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.handleRemoveETH} style={actionBtn}>Remove</Button>{' '}
            <Button color="secondary" onClick={this.toggleBtnRemove} style={actionBtn}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.modalAlertRequest} toggle={this.toggleAlertRequest} centered>
          <ModalHeader toggle={this.toggleAlertRequest}>Notification</ModalHeader>
          <ModalBody>
            <p>Your request was sent ago! Please wait for Administrator approval.</p>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleAlertRequest} style={actionBtn}>OK</Button>
          </ModalFooter>
        </Modal>
      </HomeWrapper >
    )
  }
}
function mapStateToProps(state) {
  return {
    accountData: state.accountReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetCurrentUser: async () => { await dispatch(getCurrentUser()) },
    handleChangePass: async (data) => { await dispatch(changePassword(data)) },
    handleUpdateEthRecipient: async (data) => { await dispatch(updateEthRecipient(data)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipientProfile)
