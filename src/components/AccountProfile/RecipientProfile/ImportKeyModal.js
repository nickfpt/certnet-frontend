import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Modal, ModalBody, ModalHeader, Button, FormGroup, Input, Label, Form, Alert } from 'reactstrap'

const web3Utils = require('../../../services/web3')
const Web3 = require('web3')

const web3 = window.web3

const CustomizedModalHeader = styled(ModalHeader)`
  align-items: left; 
  padding-left: 30px;
  font-weight: bold;
`

const ModalButtonDiv = styled.div`
  text-align: right;
  margin-right: 30px;
  padding-top: 20px;
`


const ModalLabel = styled(Label)`
  font-weight: bold;
  font-size: 15px !important;
`

class ImportAddressModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      address: '',
      web3Checked: false,
      web3Supported: false,
      web3Unlocked: false,
      web3CurrentWallet: '',
      web3Error: '',
      alert: false,
      inform: false,
      message: '',
    }
  }

  componentDidMount() {
    if (typeof web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      window.web3 = new Web3(web3.currentProvider)
      this.web3 = window.web3
      this.setState({ web3Checked: true, web3Supported: true })
      const checkAccountChange = () => {
        web3.eth.getAccounts((err, accounts) => {
          if (accounts.length > 0) {
            this.setState({
              web3CurrentWallet: accounts[0],
            })
          } else {
            this.setState({
              web3Unlocked: false,
              web3CurrentWallet: '',
              alert: true,
              web3Error: 'Please login to MetaMask.',
            })
          }
        })
      }

      checkAccountChange()
      this.checkChangeWalletInterval = setInterval(checkAccountChange, 1000)
    } else {
      this.setState({ web3Checked: true, web3Supported: false, alert: true, web3Error: 'MetaMask is not installed.' })
    }
  }
  async handleChange(event, field) {
    const value = event.target.value
    this.setState({ [field]: value, alert: false, inform: false })
  }

  async confirmAndImport() {
    this.setState({ alert: false, inform: false })
    const newAddress = this.state.address
    if (!this.props.addresses.includes(newAddress)) {
      if (web3Utils.isAddress(newAddress)) {
        if (web3Utils.isEqualAddresses(newAddress, this.state.web3CurrentWallet)) {
          const walletAddress = this.state.web3CurrentWallet
          const signMessage = `Sign to confirm you own the address: ${walletAddress}`
          web3.personal.sign(web3.toHex(signMessage), walletAddress, async (err, signature) => {
            if (err) {
              this.setState({ web3Error: 'Failed to confirm ownership of the address.', alert: true })
            } else {
              const ownerVerified = web3Utils.verifyOwner(walletAddress, signMessage, signature)
              this.setState({ alert: false, inform: false })
              if (ownerVerified) {
                this.props.addAddress(newAddress)
                if (this.props.onlyOne) {
                  this.props.toggle()
                } else {
                  this.setState({ inform: true, message: 'Address has been imported!', address: '' })
                }
              } else {
                this.setState({ alert: false, web3Error: 'You do not own this address.' })
              }
            }
          })
        } else {
          this.setState({ web3Error: 'Please switch to this address on MetaMask.', alert: true })
        }
      } else {
        this.setState({ web3Error: 'Invalid Ethereum address.', alert: true })
      }
    } else {
      this.setState({ web3Error: 'This address has already been imported.', alert: true })
    }
  }

  render() {
    const { modal, toggle } = this.props
    const { alert, web3Error, inform, message } = this.state
    return (<div>
      <Modal isOpen={modal} toggle={toggle} style={{ minWidth: '900px' }} className="text-center">
        <CustomizedModalHeader toggle={toggle}>
          <span style={{ fontWeight: 'bold' }}>Import Ethereum Address</span>
        </CustomizedModalHeader>
        <ModalBody>
          {alert && <Alert color="danger">
            {web3Error}
          </Alert>}
          {inform && <Alert color="primary">
            {message}
          </Alert>}
          <Form style={{ paddingLeft: '44px', textAlign: 'left' }}>
            <FormGroup>
              <ModalLabel for="address">Address</ModalLabel>
              <Input
                step={'any'}
                type="text" name="address" id="address"
                value={this.state.address}
                onChange={event => this.handleChange(event, 'address')}
                required
                autoComplete="off"
              />
            </FormGroup>
          </Form>
          <ModalButtonDiv>
            <Button color="primary" size="lg" style={{ marginRight: '20px' }} onClick={() => this.confirmAndImport()}>Import</Button>
            <Button color="secondary" size="lg" onClick={toggle}>Close</Button>
          </ModalButtonDiv>
        </ModalBody>
      </Modal>
    </div>)
  }
}

ImportAddressModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  addresses: PropTypes.arrayOf(PropTypes.string).isRequired,
  addAddress: PropTypes.func.isRequired,
  onlyOne: PropTypes.bool.isRequired,
}

export default ImportAddressModal
