import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'react-datepicker/dist/react-datepicker.css'
import {
  Row, Col, Card, Button, CardHeader, CardBody,
  Table, Input, Form, Alert, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap'
import ImportKeyModal from './ImportKeyModal'
import { requestToBeIssuer } from './../../../actions/AccountAction'

const dateFormat = require('dateformat')


class RequestIssuer extends Component {
  constructor(props) {
    super(props)
    // const { data } = this.props
    const now = new Date()
    this.state = {
      reqId: '',
      requesterEmail: this.props.email,
      status: '',
      fullname: '',
      ethereumAccount: '',
      description: '',
      address: '',
      phone: '',
      contactEmail: '',
      taxAccount: '',
      establishedDate: dateFormat(now, 'isoDate'),
      ownerName: '',
      website: '',
      successFlag: false,
      msg: '',
      resultFlag: false,
      importModal: false,
      modalRemoveETH: false,
      removeETHaddress: '',
      ethFlag: true,
    }
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      updateFlag: false,
      disableBtnUpdate: false
    })
  }
  handleSubmitUpdate = async (event) => {
    event.preventDefault()
    const { fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, ethereumAccount } = this.state
    if (!ethereumAccount) {
      this.setState({
        ethFlag: false,
      })
    } else {
      const data = {
        fullname,
        description,
        address,
        phone,
        ethereumAccount,
        contactEmail,
        taxAccount,
        establishedDate,
        ownerName,
        website,
        resultFlag: false,
      }
      console.log(data)
      await this.props.handleRequestToBeIssuer(data)
      const { accountData } = this.props
      if (accountData.requestFlag) {
        this.setState({
          successFlag: true,
          msg: 'Your request has been sent!',
          resultFlag: true,

        })
      } else {
        this.setState({
          successFlag: false,
          msg: 'Something is wrong! Please try again!',
          resultFlag: true,
        })
      }
    }
  }
  handleRemoveETH = () => {
    this.setState({
      ethereumAccount: '',
    })
    this.toggleBtnRemove()
  }
  handleAddEthAddress = (ethAcc) => {
    this.setState({
      ethereumAccount: ethAcc,
      ethFlag: true
    })
  }
  handleClickClose = (ETHadress) => {
    this.setState({
      removeETHaddress: ETHadress
    })
    this.toggleBtnRemove()
  }
  toggleBtnRemove = () => {
    this.setState({
      modalRemoveETH: !this.state.modalRemoveETH,
    })
  }
  toggleConfirmationModal = () => {
    this.setState({
      importModal: !this.state.importModal,
    })
  }
  render() {
    const { requesterEmail, fullname, description, address, ethereumAccount, phone, contactEmail, taxAccount,
      establishedDate, ownerName, website, resultFlag, msg, successFlag, importModal, removeETHaddress, ethFlag
    } = this.state
    return (
      <div>

        <Row>
          <Col xs="12" md={{ size: 8, offset: 2 }} >
            <Card>
              <CardHeader className="text-center"><h4>Request Information</h4></CardHeader>
              <CardBody>
                {resultFlag &&
                  <Alert color={successFlag ? 'primary' : 'danger'}>
                    {msg}
                  </Alert>}
                <Form onSubmit={this.handleSubmitUpdate}>
                  <Table borderless>
                    <tbody>
                      <tr>
                        <td width="20%">Requester Email</td>
                        <td><Input type="email" name="requesterEmail" value={requesterEmail} disabled /></td>
                      </tr>
                      <tr>
                        <td width="20%">Name</td>
                        <td><Input type="text" name="fullname" value={fullname} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td width="20%">Owner Name</td>
                        <td><Input type="text" name="ownerName" value={ownerName} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td>Ethereum Address</td>
                        <td>
                          {ethereumAccount ?
                            (
                              <ul style={{ padding: '0px' }}>
                                <li className="eth-list">{ethereumAccount}
                                  {!successFlag &&
                                    <span className="close" onClick={() => this.handleClickClose(ethereumAccount)}>&times;</span>
                                  }
                                </li>
                              </ul>
                            ) : (
                              <Button color="primary" size="md" outline onClick={this.toggleConfirmationModal}>Add Address</Button>
                            )}

                          {ethFlag ? null : <p style={{ color: 'red' }}>Ethereum address cannot be empty!</p>}
                        </td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td><Input type="tel" name="phone" value={phone} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td width="20%">Contact Email</td>
                        <td><Input type="email" name="contactEmail" value={contactEmail} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td><Input type="textarea" name="address" id="exampleText" value={address} onChange={this.handleInputChange} maxLength="500" required disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td width="20%">Established Date</td>
                        <td><Input type="date" name="establishedDate" value={establishedDate} onChange={this.handleInputChange} required disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td width="20%">Tax Account</td>
                        <td><Input type="text" name="taxAccount" value={taxAccount} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td>Website</td>
                        <td><Input type="text" name="website" value={website} onChange={this.handleInputChange} required autoComplete="off" disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td>Description</td>
                        <td><Input type="textarea" name="description" id="exampleText" value={description} onChange={this.handleInputChange} maxLength="500" required disabled={successFlag} /></td>
                      </tr>
                      <tr>
                        <td colSpan="2" className="text-center">
                          <Button color="danger" size="lg" type="submit" disabled={successFlag}>Send Request</Button>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {importModal && <ImportKeyModal
          modal={importModal}
          toggle={this.toggleConfirmationModal}
          addresses={[]}
          addAddress={this.handleAddEthAddress}
          onlyOne={true}
        />}
        <Modal isOpen={this.state.modalRemoveETH} toggle={this.toggleBtnRemove} centered>
          <ModalHeader toggle={this.toggleBtnRemove}>Remove Ethereum Address</ModalHeader>
          <ModalBody>
            <p>Are you sure to remove address &#34;{removeETHaddress}&#34; ?</p>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.handleRemoveETH}>Remove</Button>{' '}
            <Button color="secondary" onClick={this.toggleBtnRemove} >Cancel</Button>
          </ModalFooter>
        </Modal>
      </div >
    )
  }
}
function mapStateToProps(state) {
  return {
    accountData: state.accountReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleRequestToBeIssuer: async (data) => { await dispatch(requestToBeIssuer(data)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RequestIssuer)
