const arrTabs = [
  {
    id: '1',
    title: 'Issuer Profile',
    to: '/issuer-profile'
  },
  {
    id: '2',
    title: 'Recipient Profile',
    to: '/recipient-profile'
  }
]
export default arrTabs
