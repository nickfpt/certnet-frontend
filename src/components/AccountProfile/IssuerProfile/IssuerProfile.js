import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getCurrentUser } from './../../../actions/AccountAction'
import { Input, Table, Row, Col, Card, CardHeader, CardBody } from 'reactstrap'
import Loader from '../../layout/Loader'

const dateFormat = require('dateformat')

class IssuerProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {


    }
  }
  componentDidMount = async () => {
    await this.props.handleGetCurrentUser()
    const { user } = this.props.accountData
    const arrETHdata = []
    if (user.ethereumAccounts.length > 0) {
      user.ethereumAccounts.forEach((element) => {
        arrETHdata.push(element.address)
      })
    }
    const { _id, email, issuableStatus, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website } = this.props.accountData.user

    await this.setState({
      isuerId: _id,
      email,
      status: issuableStatus === 1 ? 'Yes' : 'No',
      fullname,
      description,
      address,
      phone,
      contactEmail,
      taxAccount,
      establishedDate,
      ownerName,
      website,
      arrETH: arrETHdata,
    })
  }
  displayETHaddress = (arr) => {
    let result = null
    if (arr !== undefined && arr.length > 0) {
      result = arr.map(element => (
        <li key={element}>{element}
        </li>
      ))
    }
    return result
  }
  render() {
    const { email, status, fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName,
      website, arrETH } = this.state
    const { accountData } = this.props
    return (
      <div>
        <Row>
          <Col xs="12" md={{ size: 8, offset: 2 }} >
            <Card>
              <CardHeader className="text-center"><h4>Issuer Information</h4></CardHeader>
              <CardBody>
                {accountData.isFetching ? (<Loader />)
                  : (
                    <Table borderless>
                      <tbody>
                        <tr>
                          <td width="20%">Email</td>
                          <td><Input type="email" name="email" defaultValue={email} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Issuable Status</td>
                          <td><Input type="text" name="status" defaultValue={status} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Name</td>
                          <td><Input type="text" name="fullname" defaultValue={fullname} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Owner Name</td>
                          <td><Input type="text" name="ownerName" defaultValue={ownerName} disabled /></td>
                        </tr>
                        <tr>
                          <td>Phone Number</td>
                          <td><Input type="tel" name="phone" defaultValue={phone} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Contact Email</td>
                          <td><Input type="email" name="contactEmail" defaultValue={contactEmail} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Ethereum Accounts</td>

                          <td>
                            <ul style={{ padding: '0px 0px 0px 20px' }}>
                              {this.displayETHaddress(arrETH)}
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td>Address</td>
                          <td><Input type="textarea" name="address" id="exampleText" value={address} maxLength="500" disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Established Date</td>
                          <td><Input type="date" name="establishedDate" defaultValue={dateFormat(establishedDate, 'isoDate')} disabled /></td>
                        </tr>
                        <tr>
                          <td width="20%">Tax Account</td>
                          <td><Input type="text" name="taxAccount" defaultValue={taxAccount} disabled /></td>
                        </tr>
                        <tr>
                          <td>Website</td>
                          <td><Input type="text" name="website" defaultValue={website} disabled /></td>
                        </tr>
                        <tr>
                          <td>Description</td>
                          <td><Input type="textarea" name="description" id="exampleText" value={description} maxLength="500" disabled /></td>
                        </tr>
                      </tbody>
                    </Table>

                  )}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div >
    )
  }
}
function mapStateToProps(state) {
  return {
    accountData: state.accountReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetCurrentUser: async () => { await dispatch(getCurrentUser()) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IssuerProfile)
