import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import {
  Row, Col, Card, Button, CardHeader, CardBody, Table, Input, Form, Alert
} from 'reactstrap'
import { BeatLoader } from 'react-spinners'
import { getCurrentUser, changePassword } from './../../../actions/AccountAction'
import * as Color from '../../../constants/Color'
import { css } from 'react-emotion'

const HomeWrapper = styled.section`
padding-top: 50px;
  padding-left: 1%;
  background: ${Color.LV1};
`
const colStyle = {
  background: Color.LV1,
}
const override = css`
    text-align: center;
    vertical-align: middle;
    line-height: 500px; 
`

class AdminProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      ethereumAccounts: [],
      changeInputFlag: false,
      curPass: '',
      newPass: '',
      cfPass: '',
      errFlag: false,
      errMsg: '',
      successMsg: '',
      successFlag: false,
      ppFlag: false,

    }
  }
  componentDidMount = async () => {
    await this.props.handleGetCurrentUser()
    const arrETHdata = []
    const { email, ethereumAccounts } = this.props.accountData.user
    if (ethereumAccounts.length > 0) {
      ethereumAccounts.forEach((element) => {
        arrETHdata.push(element.address)
      })
    }
    await this.setState({
      email,
      ethereumAccounts,
      arrETHdata,
    })
  }
  handleInputChange = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value,
      changeInputFlag: true,
      errFlag: false,
      successFlag: false,
    })
  }

  togglePolicy = () => {
    this.setState({
      ppFlag: !this.state.ppFlag,
    })
  }

  handleSubmitChange = async (e) => {
    e.preventDefault()
    const { curPass, newPass, cfPass } = this.state

    if (newPass !== cfPass) {
      this.setState({
        errFlag: true,
        errMsg: 'New password does not match the confirm password.'
      })
    } else {
      const data = {
        oldPassword: curPass,
        newPassword: newPass
      }
      await this.props.handleChangePass(data)
      if (this.props.accountData.resultFlag) {
        this.setState({
          successFlag: true,
          successMsg: 'Password has been changed!',
          changePass: false,
          curPass: '',
          newPass: '',
          cfPass: '',
        })
      } else {
        this.setState({
          errFlag: true,
          errMsg: `${this.props.accountData.error}`
        })
      }
    }
  }

  render() {
    const { email, ppFlag, changeInputFlag, curPass, newPass, cfPass, errFlag, errMsg, successFlag, successMsg } = this.state
    const { accountData } = this.props
    return (
      <HomeWrapper>
        <Row>
          <Col style={colStyle} xs="12" md={{ size: 8, offset: 2 }} >
            <Card>
              <CardHeader>Account Information</CardHeader>
              {
                accountData.isFetching ? (
                  <BeatLoader
                    className={override}
                    sizeUnit={'px'}
                    size={15}
                    color={'#36D7B7'}
                  />)
                  : (
                    <CardBody>
                      {errFlag && <Alert color="danger">{errMsg} </Alert>}
                      {successFlag && <Alert color="primary">{successMsg} </Alert>}
                      <Form onSubmit={this.handleSubmitChange}>
                        <Row>
                          <Col md="12" >
                            <Table borderless >
                              <tbody>
                                <tr>
                                  <td width="20%">Email</td>
                                  <td><Input type="email" name="email" id="exampleEmail" value={email} disabled /></td>
                                </tr>
                                <tr>
                                  <td width="20%">Current Password</td>
                                  <td>
                                    <Input type="password" name="curPass" value={curPass} onChange={this.handleInputChange} required />
                                  </td>
                                </tr >
                                <tr>
                                  <td>New Password</td>
                                  <td>
                                    <Input type="password" name="newPass" value={newPass} onChange={this.handleInputChange} required />
                                  </td>
                                </tr>
                                <tr>
                                  <td>Confirm Password</td>
                                  <td>
                                    <Input type="password" name="cfPass" value={cfPass} onChange={this.handleInputChange} required />
                                  </td>
                                </tr>
                                <tr>
                                  <td />
                                  <td>
                                    <div>
                                      <label className="hover-i" onClick={this.togglePolicy} style={{ fontSize: '14px' }}>Password policy</label>
                                      {ppFlag && <ul>
                                        <li>Be at least eight characters in length</li>
                                        <li>Contain uppercase characters (A through Z)</li>
                                        <li>Contain lowercase characters (a through z)</li>
                                        <li>Contain base 10 digits (0 through 9)</li>
                                      </ul>}
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </Table>
                            <div className="text-center">
                              <Button color="danger" size="lg" type="submit" disabled={!changeInputFlag}>Update</Button>
                            </div>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>

                  )
              }
            </Card>
          </Col>
        </Row>
      </HomeWrapper >
    )
  }
}
function mapStateToProps(state) {
  return {
    accountData: state.accountReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleGetCurrentUser: async () => { await dispatch(getCurrentUser()) },
    handleChangePass: async (data) => { await dispatch(changePassword(data)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminProfile)
