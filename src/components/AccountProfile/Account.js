import React, { Component } from 'react'
import { ROLES } from './../../constants/auth'
import jwt from 'jsonwebtoken'
import RecipientProfile from './RecipientProfile/RecipientProfile'
import IssuerIndex from './IssuerProfile/IssuerIndex'
import AdminProfile from './AdminProfile/AdminProfile'

class Account extends Component {
  render() {
    const token = this.props.tokenData
    let decodedToken = null
    try {
      decodedToken = jwt.verify(token.token, process.env.JWT_SECRET)
    } catch (ex) {
      decodedToken = null
    }

    return (
      <div>
        {
          decodedToken && decodedToken.roles.includes(ROLES.ADMIN) &&
          <AdminProfile />
        }
        {
          decodedToken && decodedToken.roles.includes(ROLES.ISSUER) &&
          <IssuerIndex match={this.props.match} />
        }
        {
          decodedToken && decodedToken.roles.includes(ROLES.RECIPIENT) && !decodedToken.roles.includes(ROLES.ISSUER) &&
          <RecipientProfile />
        }
      </div>
    )
  }
}

export default (Account)
