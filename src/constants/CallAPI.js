import Cookies from 'universal-cookie'
import get from 'lodash/get'

const cookies = new Cookies()
const axios = require('axios')

export async function fetch(api, post = false, data = {}, formData = false) {
  const accessToken = get(cookies.get('token'), 'token', null)
  let option = {
    method: 'GET',
    url: api,
    headers: {
      'x-access-token': accessToken
    }
  }
  if (post) {
    if (formData) {
      option = {
        method: 'POST',
        url: api,
        headers: {
          'x-access-token': accessToken
        },
        data
      }
    } else {
      option = {
        method: 'POST',
        url: api,
        headers: {
          'content-type': 'application/json',
          'x-access-token': accessToken
        },
        data
      }
    }
  }
  // console.log('option', option)
  const res = await axios(option)
  // console.log('res api', res)
  if (res.status === 200) return res.data
  const error = new Error(res.statusText)
  throw error
}

