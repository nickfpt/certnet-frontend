// export const DOMAIN = 'https://35.229.160.90'
export const DOMAIN = 'https://nguyenlc1993.me'
// export const DOMAIN = 'https://localhost:3001'

// template management
export const getAllTemplateById = `${DOMAIN}/api/v1/issuer/templates`
export const createNewTemplate = `${DOMAIN}/api/v1/issuer/template`
export const deleteTemplate = templateId => `${DOMAIN}/api/v1/issuer/deleteTemplate/${templateId}`
export const updateTemplate = templateId => `${DOMAIN}/api/v1/issuer/updateTemplate/${templateId}`


// batch management
export const createNewBatch = `${DOMAIN}/api/v1/issuer/batch`
export const getAllBatchById = `${DOMAIN}/api/v1/issuer/batches`
export const getCertsByBatchId = batchId => `${DOMAIN}/api/v1/issuer/batch/${batchId}`

// revoke
export const revokeBatch = batchId => `${DOMAIN}/api/v1/issuer/revokeBatch/${batchId}`
export const revokeCert = certId => `${DOMAIN}/api/v1/issuer/revokeCertificate/${certId}`

// issue management
export const createUnsignedCert = `${DOMAIN}/api/v1/issuer/createUnsignedCert`
export const signBatch = `${DOMAIN}/api/v1/issuer/signBatch`
export const buildMerkleTree = batchId => `${DOMAIN}/api/v1/issuer/buildMerkleTree/${batchId}`

// recipient
export const getAllCertsByRecipientId = `${DOMAIN}/api/v1/recipient`
export const getAllLinkByRecipientId = `${DOMAIN}/api/v1/recipient/getToken`
export const deleteLink = token => `${DOMAIN}/api/v1/recipient/disableToken/${token}`
export const generateShareLink = `${DOMAIN}/api/v1/recipient/generateShareLink`
export const generateShareLinkForGuest = `${DOMAIN}/generateShareLinkForGuest`
export const getCertById = certId => `${DOMAIN}/recipient/getCertById/${certId}`

// public
export const getIssuers = `${DOMAIN}/issuers`
export const getIssuerById = id => `${DOMAIN}/issuers/${id}`
export const verifyToken = token => `${DOMAIN}/verify/${token}`

// authen api
export const register = `${DOMAIN}/register`
export const login = `${DOMAIN}/login`
export const resetPass = email => `${DOMAIN}/recoverPassword/${email}`

// admin api
export const getRequestToBeIssuer = flag => `${DOMAIN}/api/v1/admin/getRequestToBeIssuer?showAll=${flag}`
export const getAllIssuers = `${DOMAIN}/api/v1/admin/users`
export const rejectRequest = reqId => `${DOMAIN}/api/v1/admin/rejectRequestToBeIssuer/${reqId}`
export const approveRequest = reqId => `${DOMAIN}/api/v1/admin/acceptRequestToBeIssuer/${reqId}`
export const changeIssuerStatus = (issuerId, value) => `${DOMAIN}/api/v1/admin/changeIssuerStatus/${issuerId}?active=${value}`
export const updateRequest = reqId => `${DOMAIN}/api/v1/admin/updateRequestToBeIssuer/${reqId}`
export const updateIssuer = issuerId => `${DOMAIN}/api/v1/admin/users/${issuerId}`
export const getCurrentUser = `${DOMAIN}/api/v1/profile/me`
export const changePass = `${DOMAIN}/api/v1/profile/changePassword`
export const updateEthAddressRecipient = `${DOMAIN}/api/v1/recipient/updateEthAccount`
export const requestToBeIssuer = `${DOMAIN}/api/v1/issuer/requestToBeIssuer`
export const uploadLogo = `${DOMAIN}/api/v1/admin/uploadImage`
export const addETHAddress = (issuerId) => `${DOMAIN}/api/v1/admin/addETHAddress/${issuerId}`
export const removeETHAddress = (issuerId) => `${DOMAIN}/api/v1/admin/removeETHAddress/${issuerId}`
export const checkAddressInUsed = (address) => `${DOMAIN}/api/v1/admin/checkIfETHAddressIsNotUsed/${address}`
