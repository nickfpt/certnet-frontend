module.exports = {
  BURN_ACCOUNT: '0xdeaddeaddeaddeaddeaddeaddeaddeaddeaddead',
  NETWORK: {
    RINKENY: 'rinkeby',
    MAINNET: 'main',
  },
  IMAGE_URL: 'https://storage.googleapis.com/certnet/logo_partners'
}
