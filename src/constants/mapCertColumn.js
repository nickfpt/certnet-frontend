export const mapRecipientInfo = {
  rollNumber: 'Roll number',
  name: 'Recipient Name',
  email: 'Email',
  dob: 'DoB',
  ethereumAddress: 'Ethereum Address',
  pob: 'PoB',
  gender: 'Gender',
  nationality: 'Nationality',
  race: 'Race',
}
export const mapContent = {
  certName: 'Certificate name',
  major: 'Major',
  graduationYear: 'Graduation Year',
  graduationGrade: 'Graduation Grade',
  studyMode: 'Mode of Study',
  certId: 'Cert Id',
  graduationDecisionId: 'Decision Id',
}
