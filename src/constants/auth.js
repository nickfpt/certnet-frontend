export const ROLES = {
  ADMIN: 'admin',
  ISSUER: 'issuer',
  RECIPIENT: 'recipient'
}
