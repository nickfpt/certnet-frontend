import React from 'react'
import namor from 'namor'
import camelCase from 'lodash/camelCase'
import './index.css'

const range = (len) => {
  const arr = []
  for (let i = 0; i < len; i += 1) {
    arr.push(i)
  }
  return arr
}

const newPerson = index => ({
  no: index,
  subject: namor.generate({ words: 1, numbers: 0 }),
  value: Math.floor(Math.random() * 30),
})

export function makeData(len = 5553) {
  return range(len).map(d => ({
    ...newPerson(d + 1),
  }))
}

export const Tips = () =>
  (<div style={{ textAlign: 'center' }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>)
export function setPageSizeOption(size, arrOption = [10, 20, 25, 50, 100]) {
  const arr = []
  if (arrOption[0] >= size) {
    arr.push(arrOption[0])
  } else {
    for (let index = 0; index < arrOption.length; index += 1) {
      if (arrOption[index] <= size) {
        arr.push(arrOption[index])
      } else {
        arr.push(size)
        break
      }
    }
  }
  return arr
}
export function getContentPath(content) {
  const recipientInfo = [
    'Roll number',
    'Ethereum Address',
    'Recipient Name',
    'DoB',
    'PoB',
    'Email',
    'Gender',
    'Nationality',
    'Race',
    'Decision Id',
    'Cert Id',
    'Certificate name',
    'Major',
    'Graduation Year',
    'Mode of Study',
    'Graduation Grade',

  ]
  const recipientPath = [
    'content.recipientInfo.rollNumber',
    'content.recipientInfo.ethereumAddress',
    'content.recipientInfo.name',
    'content.recipientInfo.dob',
    'content.recipientInfo.pob',
    'content.recipientInfo.email',
    'content.recipientInfo.gender',
    'content.recipientInfo.nationality',
    'content.recipientInfo.race',
    'content.graduationDecisionId',
    'content.certId',
    'content.certName',
    'content.major',
    'content.graduationYear',
    'content.studyMode',
    'content.graduationGrade',
  ]
  for (let index = 0; index < recipientInfo.length; index += 1) {
    if (recipientInfo[index].toLowerCase() === content.toLowerCase()) { return recipientPath[index] }
  }
  const camelString = camelCase(content)
  return `content.additionInfor.${camelString}`
}
