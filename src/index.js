import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'pretty-checkbox/src/pretty-checkbox.scss'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import appReducers from './reducers/reducers'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter } from 'react-router-dom'

const store = createStore(
  appReducers,
  applyMiddleware(thunk),
)
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
)

if (module.hot) {
  module.hot.accept('./App', () => {
    ReactDOM.render(<AppContainer>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AppContainer>, document.getElementById('root'))
  })
}

serviceWorker.unregister()
