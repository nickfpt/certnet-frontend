import get from 'lodash/get'
import {
  FETCHING_ISSUER, FETCHING_ISSUER_SUCCESS, UPDATE_ISSUER_SUCCESS
} from '../actions/AdminIssuerAction'


const initialState = {
  isFetching: false,
  issuers: [],
  errMsg: '',
}

export default function adminIssuerReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_ISSUER:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_ISSUER_SUCCESS:
      result = JSON.parse(action.data)
      const okFlag = get(result, ['ok'], false)
      if (okFlag) {
        return {
          ...state,
          isFetching: false,
          issuers: get(result, ['issuers'], []),
        }
      }
      return {
        ...state,
        isFetching: false,
        errMsg: get(result, ['error'], ''),
      }
    case UPDATE_ISSUER_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        updateResultFlag: get(result, ['ok'], false),
      }
    default:
      return state
  }
}
