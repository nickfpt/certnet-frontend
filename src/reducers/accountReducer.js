import get from 'lodash/get'
import {
  FETCHING_CURRENT_USER, FETCHING_CURRENT_USER_SUCCESS, CHANGE_PASS_SUCCESS, UPDATE_ETH_SUCCESS, REQUEST_TO_BE_ISSUER_SUCCESS
} from '../actions/AccountAction'


const initialState = {
  isFetching: false,
  user: {},
  resultFlag: false,
  updateFlag: false,
  requestFlag: false,
  error: ''
}

export default function accountReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_CURRENT_USER:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_CURRENT_USER_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        isFetching: false,
        user: result,
      }
    case CHANGE_PASS_SUCCESS:
      result = JSON.parse(action.data)
      const resultFlag = get(result, 'ok', false)
      if (resultFlag) {
        return {
          ...state,
          resultFlag,
        }
      }
      return {
        ...state,
        resultFlag,
        error: get(result, 'error', '')
      }
    case UPDATE_ETH_SUCCESS:
      result = JSON.parse(action.data)
      const updateFlag = get(result, 'ok', false)
      return {
        ...state,
        updateFlag,
      }
    case REQUEST_TO_BE_ISSUER_SUCCESS:
      result = JSON.parse(action.data)
      const requestFlag = get(result, 'ok', false)
      console.log(requestFlag)
      return {
        ...state,
        requestFlag,
      }

    default:
      return state
  }
}
