import {
  INITIALIZE_GENERAL_INFOR, CONFIRM_DATA_BATCH, FETCHING_DATA_TEMPLATE, FETCHING_DATA_TEMPLATE_SUCCESS, POSTING_CREATE_BATCH_SUCCESS,
  CREATE_UNSIGNED_CERT_SUCCESS, SET_ACTIVE_STEP, SAVE_VISUAL_FIELDS, RESET_STATE
} from '../actions/IssueBatchAction'

const initialState = {
  title: '',
  description: '',
  tags: [],
  template: '',
  header: [],
  data: [],
  templateAll: [],
  titleFlag: false,
  titleError: '',
  batchId: '',
  activeStep: 0,
  issueFlag: false,
  unsignedCertError: '',
  visualFields: [],
  visualTplId: 1,
  rawVisualField: []
}

export default function issueBatchReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case SET_ACTIVE_STEP:
      return {
        ...state,
        activeStep: action.step,
      }
    case FETCHING_DATA_TEMPLATE:
      return {
        ...state,
        isFetching: true,
      }
    case SAVE_VISUAL_FIELDS:
      const { data } = action
      const { visualTplId, rawVisualField } = data
      delete data.visualTplId
      delete data.rawVisualField
      return {
        ...state,
        visualTplId,
        rawVisualField,
        visualFields: data
      }
    case CONFIRM_DATA_BATCH:
      const rowData = []
      for (let index = 0; index < action.data.length; index += 1) {
        rowData.push(Object.values(action.data[index]))
      }
      return {
        ...state,
        data: rowData,
      }
    case FETCHING_DATA_TEMPLATE_SUCCESS:
      const myObject = JSON.parse(action.data)
      const tempAll = []
      for (let index = 0; index < myObject.length; index += 1) {
        const temp = {}
        temp.id = myObject[index]._id
        temp.label = myObject[index].name
        temp.value = Object.keys(myObject[index].content)
        tempAll.push(temp)
      }
      return {
        ...state,
        isFetching: false,
        templateAll: tempAll,
      }
    case POSTING_CREATE_BATCH_SUCCESS:
      result = JSON.parse(action.data)
      if (result.ok === true) {
        return {
          ...state,
          titleFlag: true,
          batchId: result.batchId,
        }
      }
      return {
        ...state,
        titleFlag: false,
        titleError: result.error,
      }

    case CREATE_UNSIGNED_CERT_SUCCESS:
      result = JSON.parse(action.data)
      if (result.ok === true) {
        return {
          ...state,
          unsignedCertFlag: true,
        }
      }
      return {
        ...state,
        unsignedCertFlag: false,
        unsignedCertError: result.error,
      }
    case INITIALIZE_GENERAL_INFOR:
      result = action.data
      return {
        ...state,
        data: result.data,
        description: result.description,
        header: result.header,
        title: result.title,
        tags: result.tags,
        template: result.template,
        useVisualFlag: result.useVisualFlag,
      }
    case RESET_STATE:
      return initialState
    default:
      return state
  }
}
