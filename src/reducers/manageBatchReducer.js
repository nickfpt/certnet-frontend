import {
  FETCHING_BATCH_DATA, FETCHING_BATCH_DATA_SUCCESS, GET_LIST_CERTS_BY_BATCH_ID_SUCCESS,
} from '../actions/ManageBatchAction'

const initialState = {
  batchAll: [],
  isFetching: false,
  listCerts: {},
}

export default function issueBatchReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHING_BATCH_DATA:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_BATCH_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        batchAll: JSON.parse(action.data),
      }
    case GET_LIST_CERTS_BY_BATCH_ID_SUCCESS:
      return {
        ...state,
        listCerts: JSON.parse(action.data),
      }
    default:
      return state
  }
}
