import { combineReducers } from 'redux'
import issueBatchReducer from './issueBatchReducer'
import manageTemplateReducer from './manageTemplateReducer'
import manageBatchReducer from './manageBatchReducer'
import manageCertificateReducer from './manageCertificateReducer'
import manageLinkReducer from './manageLinkReducer'
import quickShareReducer from './quickShareReducer'
import verifyTokenReducer from './verifyTokenReducer'
import issuersReducer from './issuersReducer'
import authenReducer from './authenReducer'
import adminRequestReducer from './adminRequestReducer'
import adminIssuerReducer from './adminIssuerReducer'
import accountReducer from './accountReducer'

const appReducers = combineReducers({
  issueBatchReducer,
  manageTemplateReducer,
  manageBatchReducer,
  manageCertificateReducer,
  manageLinkReducer,
  quickShareReducer,
  verifyTokenReducer,
  issuersReducer,
  authenReducer,
  adminRequestReducer,
  adminIssuerReducer,
  accountReducer
})

export default appReducers
