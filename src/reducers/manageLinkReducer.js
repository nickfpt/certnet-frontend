import {
  FETCHING_ALL_LINKS, FETCHING_ALL_LINKS_SUCCESS, DELETE_LINK_SUCCESS,
} from '../actions/ManageLinkAction'

const initialState = {
  allLinks: {},
  isFetching: false,
  resultDelete: false,
  errMsgDelete: 'false',

}

export default function manageLinkReducer(state = initialState, action) {
  // let result = {};
  // let batchAll = [];
  switch (action.type) {
    case FETCHING_ALL_LINKS:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_ALL_LINKS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        allLinks: JSON.parse(action.data).tokens,
      }
    case DELETE_LINK_SUCCESS:
      const result = JSON.parse(action.data)
      console.log('DELETE_LINK_SUCCESS')
      console.log(result)
      console.log('END!!!!!!!!!!!!!!!!!!!')
      return {
        ...state,
        resultDelete: result.ok,
        errMsgDelete: result.error,
      }
    default:
      return state
  }
}
