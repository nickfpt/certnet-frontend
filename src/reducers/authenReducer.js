import get from 'lodash/get'
import {
  CREATE_USER_RESULT, LOGIN_RESULT, LOGOUT, RESET_RESULT
} from '../actions/AuthenAction'


const initialState = {
  createFlag: false,
  okLoginFlag: false,
  resetFlag: false,
  errMsg: '',
  userData: {},
  tokenData: {},
  errCode: ''
}

export default function authenReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case CREATE_USER_RESULT:
      result = JSON.parse(action.data)
      return {
        ...state,
        createFlag: get(result, ['ok'], false),
        errMsg: get(result, ['error'], ''),
      }
    case LOGIN_RESULT:
      result = JSON.parse(action.data)
      return {
        ...state,
        loginFlag: get(result, ['ok'], false),
        errMsg: get(result, ['error'], ''),
        errCode: get(result, ['errorCode'], ''),
        userData: get(result, ['userData'], {}),
        tokenData: get(result, ['tokenData'], {}),
      }
    case RESET_RESULT:
      result = JSON.parse(action.data)
      return {
        ...state,
        resetFlag: get(result, ['ok'], false),
        errCode: get(result, ['errorCode'], ''),
      }
    case LOGOUT:
      return {
        ...state,
        loginFlag: false,
        errMsg: '',
        userData: {},
        tokenData: {},
      }
    default:
      return state
  }
}
