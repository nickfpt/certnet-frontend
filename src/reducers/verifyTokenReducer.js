import {
  FETCHING_TOKEN, FETCHING_TOKEN_SUCCESS,
} from '../actions/VerifyAction'

const initialState = {
  isFetching: false,
  validFlag: false,
  errResult: '',
  resultsData: [],
  shareLinkData: {},
  errMsg: '',
}

export default function verifyToken(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_TOKEN:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_TOKEN_SUCCESS:
      result = JSON.parse(action.data)
      if (result.valid) {
        return {
          ...state,
          validFlag: true,
          isFetching: false,
          resultsData: result.results,
        }
      }
      if (result.errorCode === 1) {
        return {
          ...state,
          isFetching: false,
          validFlag: false,
          errMsg: result.error,
          errorCode: 1,
        }
      }
      return {
        ...state,
        isFetching: false,
        validFlag: false,
        resultsData: result.results,
      }


    default:
      return state
  }
}
