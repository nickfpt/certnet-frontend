import {
  FETCHING_ALL_CERTIFICATE, FETCHING_ALL_CERTIFICATE_SUCCESS, GENERATE_SHARE_LINK_SUCCESS,
} from '../actions/ManageCertificateAction'

const initialState = {
  allCerts: {},
  isFetching: false,
  listCerts: {},
  shareLinkData: {},
}

export default function manageCertificateReducer(state = initialState, action) {
  // let result = {};
  // let batchAll = [];
  switch (action.type) {
    case FETCHING_ALL_CERTIFICATE:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_ALL_CERTIFICATE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        allCerts: JSON.parse(action.data).certificates,
      }
    case GENERATE_SHARE_LINK_SUCCESS:
      return {
        ...state,
        isFetching: false,
        shareLinkData: JSON.parse(action.data),
      }
    default:
      return state
  }
}
