import {
  FETCHING_CERT, FETCHING_CERT_SUCCESS, GENERATE_QUICK_SHARE_LINK_SUCCESS,
} from '../actions/QuickShareAction'
import get from 'lodash/get'

const initialState = {
  isFetching: false,
  resultFlag: false,
  revokeFlag: true,
  errResult: '',
  resultData: {},
  shareLinkData: {},

}

export default function manageLinkReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_CERT:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_CERT_SUCCESS:
      result = JSON.parse(action.data)
      if (result.ok) {
        return {
          ...state,
          resultFlag: true,
          isFetching: false,
          resultData: result,
          revokeFlag: get(result, ['certificate', 'metadata', 'revoked'], true),
        }
      }
      return {
        ...state,
        isFetching: false,
        resultFlag: false,
      }

    case GENERATE_QUICK_SHARE_LINK_SUCCESS:
      return {
        ...state,
        isFetching: false,
        shareLinkData: JSON.parse(action.data),
      }
    default:
      return state
  }
}
