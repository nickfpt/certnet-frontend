import {
  FETCHING_ISSUERS, FETCHING_ISSUERS_SUCCESS, FETCHING_INDIVIDUAL_ISSUER, FETCHING_INDIVIDUAL_ISSUER_SUCCESS,
} from '../actions/IssuersAction'

const initialState = {
  isFetching: false,
  resultFlag: false,
  errResult: '',
  issuersArr: [],

}

export default function issuersReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_ISSUERS:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_ISSUERS_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        resultFlag: result.ok,
        errMsg: result.error,
        issuersArr: result.issuers,
        isFetching: false,
      }
    case FETCHING_INDIVIDUAL_ISSUER:
      return {
        ...state,
        isFetchingIndividual: true,
      }
    case FETCHING_INDIVIDUAL_ISSUER_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        resultIndividualFlag: result.ok,
        errIndividualMsg: result.error,
        issuerData: result.issuers,
        isFetchingIndividual: false,
      }
    default:
      return state
  }
}
