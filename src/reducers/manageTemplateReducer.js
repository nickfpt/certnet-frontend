import {
  FETCHING_DATA_TEMPLATE, FETCHING_DATA_TEMPLATE_SUCCESS,
} from '../actions/IssueBatchAction'
import {
  CREATE_NEW_TEMPLATE_SUCCESS, DELETE_TEMPLATE_SUCCESS, FETCH_TEMPLATE_DATA_AGAIN_SUCCESS, UPDATE_TEMPLATE_SUCCESS,
} from '../actions/ManageTemplateAction'

const initialState = {
  templateAll: [],
  isFetching: true,
}

export default function manageTemplateReducer(state = initialState, action) {
  let result = {}
  const tempAll = []
  switch (action.type) {
    case FETCHING_DATA_TEMPLATE:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_DATA_TEMPLATE_SUCCESS:
      result = JSON.parse(action.data)
      for (let index = 0; index < result.length; index += 1) {
        const temp = {}
        temp.id = result[index]._id
        temp.description = result[index].description
        temp.name = result[index].name
        temp.value = Object.keys(result[index].content)
        tempAll.push(temp)
      }
      return {
        ...state,
        isFetching: false,
        templateAll: tempAll,
      }
    case FETCH_TEMPLATE_DATA_AGAIN_SUCCESS:
      result = JSON.parse(action.data)
      for (let index = 0; index < result.length; index += 1) {
        const temp = {}
        temp.id = result[index]._id
        temp.description = result[index].description
        temp.name = result[index].name
        temp.value = Object.keys(result[index].content)
        tempAll.push(temp)
      }
      return {
        ...state,
        templateAll: tempAll,
      }
    case CREATE_NEW_TEMPLATE_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        resultCreatNewTemplate: result.ok,
        errMsgCreatNewTemplate: result.error,
      }
    case UPDATE_TEMPLATE_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        resultUpdateTemplate: result.ok,
        errMsgUpdateNewTemplate: result.error,
      }
    case DELETE_TEMPLATE_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        resultDeleteTemplate: result.ok,
        errMsgDeleteNewTemplate: result.error,
      }
    default:
      return state
  }
}
