import get from 'lodash/get'
import {
  FETCHING_REQUEST, FETCHING_REQUEST_SUCCESS, UPDATE_REQUEST_SUCCESS
} from '../actions/AdminRequestAction'


const initialState = {
  isFetching: false,
  requests: [],
  errMsg: '',
  updateResultFlag: false
}

export default function adminRequestReducer(state = initialState, action) {
  let result = {}
  switch (action.type) {
    case FETCHING_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case FETCHING_REQUEST_SUCCESS:
      result = JSON.parse(action.data)
      const okFlag = get(result, ['ok'], false)
      if (okFlag) {
        return {
          ...state,
          isFetching: false,
          requests: get(result, ['requests'], []),
        }
      }
      return {
        ...state,
        isFetching: false,
        errMsg: get(result, ['error'], ''),
      }
    case UPDATE_REQUEST_SUCCESS:
      result = JSON.parse(action.data)
      return {
        ...state,
        updateResultFlag: get(result, ['ok'], false),
      }

    default:
      return state
  }
}
