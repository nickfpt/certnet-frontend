/* eslint-disable react/no-array-index-key */

import React, { Component } from 'react'
import jwt from 'jsonwebtoken'
import Cookies from 'universal-cookie'
import get from 'lodash/get'
import { Route, Switch, Redirect } from 'react-router-dom'
import IssuerPage from './components/IssuerPage/IssuerPage'
import RecipientPage from './components/RecipientPage/RecipientPage'
import Home from './components/HomePage/Home'
import Account from './components/AccountProfile/Account'
import QuickSharePage from './components/QuickSharePage/QuickShare'
import PartnersPage from './components/PartnersPage/PartnersPage'
import Admin from './components/AdminPage/Admin'
import IndividualPartnerPage from './components/PartnersPage/IndividualPartnerPage'
import Header from './components/layout/Header'
import Footer from './components/layout/Footer'
import { ROLES } from './constants/auth'
import { withRouter } from 'react-router-dom'
import NotFound from './components/NotFound'


export const PrivateRoute = ({ roleValue: role, component: Comp, token, path, tokenData, ...rest }) => (
  <Route
    {...rest}
    tokenData={tokenData}
    path={path}
    render={(props) => {
      const roles = get(token, 'roles', null)
      if (roles !== null) {
        // any role can access
        if (role === null) {
          return <Comp {...props} tokenData={tokenData} />
        } else if (roles.includes(role)) {
          return <Comp {...props} />
        }
      }
      return <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    }}
  />

)

class App extends Component {
  updateToken = () => {
    this.setState({
      reload: 'reload'
    })
  }
  showContentMenu = (routes) => {
    let result = null
    if (routes.length > 0) {
      result = routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />

      ))
    }
    return result
  }


  render() {
    const cookies = new Cookies()
    const tokenData = cookies.get('token')
    let token = null
    try {
      token = jwt.verify(tokenData.token, process.env.JWT_SECRET)
    } catch (ex) {
      token = null
    }
    console.log(token)
    this.state = {
      token
    }
    return (
      <div className="wrapper-body">
        <div className="wrapper">
          <Header updateToken={this.updateToken} tokenData={tokenData} pathname={this.props.location.pathname} />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/home" exact component={Home} />
            <Route path="/home/:token" component={Home} />
            <PrivateRoute path="/admin" component={Admin} roleValue={ROLES.ADMIN} token={token} />
            <PrivateRoute path="/issuer" component={IssuerPage} roleValue={ROLES.ISSUER} token={token} />
            <PrivateRoute path="/account" component={Account} roleValue={null} token={token} tokenData={tokenData} />
            <PrivateRoute path="/recipient" component={RecipientPage} roleValue={ROLES.RECIPIENT} token={token} />
            <Route path="/quickShare" exact component={QuickSharePage} />
            <Route path="/quickShare/:certId" component={QuickSharePage} />
            <Route path="/partners" exact component={PartnersPage} />
            <Route path="/partners/:issuerId" component={IndividualPartnerPage} />
            <Route path="" component={NotFound} />
          </Switch>
        </div>
        <Footer />
      </div >
    )
  }
}

export default withRouter(App)
