import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_CERT = 'FETCHING_CERT'
export const FETCHING_CERT_SUCCESS = 'FETCHING_CERT_SUCCESS'
export const GENERATE_QUICK_SHARE_LINK_SUCCESS = 'GENERATE_QUICK_SHARE_LINK_SUCCESS'

export function fetchCert() {
  return {
    type: FETCHING_CERT,
  }
}
export function getCertByIdSuccess(data) {
  return {
    type: FETCHING_CERT_SUCCESS,
    data,
  }
}
export function generateQuickShareLinkSuccess(data) {
  return {
    type: GENERATE_QUICK_SHARE_LINK_SUCCESS,
    data,
  }
}

export function getCertById(certId) {
  return async (dispatch) => {
    dispatch(fetchCert())
    try {
      const res = await callAPI.fetch(endpoints.getCertById(certId))
      dispatch(getCertByIdSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function generateQuickShareLink(certData) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.generateShareLinkForGuest, true, certData)
      dispatch(generateQuickShareLinkSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
