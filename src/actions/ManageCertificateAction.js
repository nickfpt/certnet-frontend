import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_ALL_CERTIFICATE = 'FETCHING_ALL_CERTIFICATE'
export const FETCHING_ALL_CERTIFICATE_SUCCESS = 'FETCHING_ALL_CERTIFICATE_SUCCESS'
export const GENERATE_SHARE_LINK_SUCCESS = 'GENERATE_SHARE_LINK_SUCCESS'


export function getDataAllCert() {
  return {
    type: FETCHING_ALL_CERTIFICATE,
  }
}
export function getDataAllCertSuccess(data) {
  return {
    type: FETCHING_ALL_CERTIFICATE_SUCCESS,
    data,
  }
}
export function generateShareLinkSuccess(data) {
  return {
    type: GENERATE_SHARE_LINK_SUCCESS,
    data,
  }
}


export function fetchDataAllCert() {
  return async (dispatch) => {
    dispatch(getDataAllCert())
    try {
      const res = await callAPI.fetch(endpoints.getAllCertsByRecipientId)
      dispatch(getDataAllCertSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}

export function generateShareLink(certData) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.generateShareLink, true, certData)
      dispatch(generateShareLinkSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
