import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_BATCH_DATA_SUCCESS = 'FETCHING_BATCH_DATA_SUCCESS'
export const FETCHING_BATCH_DATA = 'FETCHING_BATCH_DATA'
export const GET_LIST_CERTS_BY_BATCH_ID_SUCCESS = 'GET_LIST_CERTS_BY_BATCH_ID_SUCCESS'

export function getBatchData() {
  return {
    type: FETCHING_BATCH_DATA,
  }
}
export function getBatchDataSuccess(data) {
  return {
    type: FETCHING_BATCH_DATA_SUCCESS,
    data,
  }
}
export function getListCertsByBatchIdSuccess(data) {
  return {
    type: GET_LIST_CERTS_BY_BATCH_ID_SUCCESS,
    data,
  }
}
export function fetchBatchData() {
  return async (dispatch) => {
    dispatch(getBatchData())
    try {
      const res = await callAPI.fetch(endpoints.getAllBatchById)
      dispatch(getBatchDataSuccess(JSON.stringify(res.batches)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function getListCertsByBatchId(batchId) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.getCertsByBatchId(batchId))
      dispatch(getListCertsByBatchIdSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
