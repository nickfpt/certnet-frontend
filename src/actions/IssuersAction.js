import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_ISSUERS = 'FETCHING_ISSUERS'
export const FETCHING_ISSUERS_SUCCESS = 'FETCHING_ISSUERS_SUCCESS'
export const FETCHING_INDIVIDUAL_ISSUER = 'FETCHING_INDIVIDUAL_ISSUER'
export const FETCHING_INDIVIDUAL_ISSUER_SUCCESS = 'FETCHING_INDIVIDUAL_ISSUER_SUCCESS'
export function fetchIssuers() {
  return {
    type: FETCHING_ISSUERS,
  }
}
export function fetchIssuersSuccess(data) {
  return {
    type: FETCHING_ISSUERS_SUCCESS,
    data,
  }
}

export function fetchIndividaulIssuer() {
  return {
    type: FETCHING_INDIVIDUAL_ISSUER,
  }
}
export function fetchIndividaulIssuerSuccess(data) {
  return {
    type: FETCHING_INDIVIDUAL_ISSUER_SUCCESS,
    data,
  }
}

export function getIssuer() {
  return async (dispatch) => {
    dispatch(fetchIssuers())
    try {
      const res = await callAPI.fetch(endpoints.getIssuers)
      dispatch(fetchIssuersSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function getIndividualIssuer(issuerId) {
  return async (dispatch) => {
    dispatch(fetchIndividaulIssuer())
    try {
      const res = await callAPI.fetch(endpoints.getIssuerById(issuerId))
      dispatch(fetchIndividaulIssuerSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
