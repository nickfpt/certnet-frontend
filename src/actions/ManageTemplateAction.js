import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'
import { getDataSuccess } from 'src/actions/IssueBatchAction'

export const CREATE_NEW_TEMPLATE_SUCCESS = 'CREATE_NEW_TEMPLATE_SUCCESS'
export const DELETE_TEMPLATE_SUCCESS = 'DELETE_TEMPLATE_SUCCESS'
export const UPDATE_TEMPLATE_SUCCESS = 'UPDATE_TEMPLATE_SUCCESS'
export const FETCH_TEMPLATE_DATA_AGAIN_SUCCESS = 'FETCH_TEMPLATE_DATA_AGAIN_SUCCESS'


export function postCreateTemplateSuccess(data) {
  return {
    type: CREATE_NEW_TEMPLATE_SUCCESS,
    data,
  }
}
export function deleteTemplateSuccess(data) {
  return {
    type: DELETE_TEMPLATE_SUCCESS,
    data,
  }
}
export function updateTemplateSuccess(data) {
  return {
    type: UPDATE_TEMPLATE_SUCCESS,
    data,
  }
}
export function fetchTemplateDataAgainSucess(data) {
  return {
    type: FETCH_TEMPLATE_DATA_AGAIN_SUCCESS,
    data,
  }
}
export function createNewTemplate(templateData) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.createNewTemplate, true, templateData)
      dispatch(postCreateTemplateSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function deleteTemplate(templateid) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.deleteTemplate(templateid))
      dispatch(postCreateTemplateSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function updateTemplate(templateid, templateData) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.updateTemplate(templateid), true, templateData)
      dispatch(updateTemplateSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function fetchTemplateDataAgain() {
  return async (dispatch) => {
    try {
      const response = await callAPI.fetch(endpoints.getAllTemplateById)
      dispatch(getDataSuccess(JSON.stringify(response.templates)))
    } catch (err) {
      console.log(err)
    }
  }
}
