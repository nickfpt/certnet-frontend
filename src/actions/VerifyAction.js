import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_TOKEN = 'FETCHING_TOKEN'
export const FETCHING_TOKEN_SUCCESS = 'FETCHING_TOKEN_SUCCESS'


export function fetchToken() {
  return {
    type: FETCHING_TOKEN,
  }
}
export function fetchTokenSuccess(data) {
  return {
    type: FETCHING_TOKEN_SUCCESS,
    data,
  }
}

export function verifyToken(token) {
  return async (dispatch) => {
    dispatch(fetchToken())
    try {
      const res = await callAPI.fetch(endpoints.verifyToken(token))
      dispatch(fetchTokenSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
