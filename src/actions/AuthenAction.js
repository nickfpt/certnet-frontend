import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const CREATE_USER_RESULT = 'CREATE_USER_RESULT'
export const LOGIN_RESULT = 'LOGIN_RESULT'
export const RESET_RESULT = 'RESET_RESULT'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const LOGOUT = 'LOGOUT'
export function createUserResult(data) {
  return {
    type: CREATE_USER_RESULT,
    data,
  }
}
export function loginResult(data) {
  return {
    type: LOGIN_RESULT,
    data,
  }
}
export function resetResult(data) {
  return {
    type: RESET_RESULT,
    data,
  }
}
export function logout() {
  return {
    type: LOGOUT,
  }
}
export function createUser(user) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.register, true, user)
      dispatch(createUserResult(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function login(user) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.login, true, user)
      dispatch(loginResult(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function resetPass(email) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.resetPass(email))
      dispatch(resetResult(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
