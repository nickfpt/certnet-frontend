import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_ALL_LINKS = 'FETCHING_ALL_LINKS'
export const FETCHING_ALL_LINKS_SUCCESS = 'FETCHING_ALL_LINKS_SUCCESS'
export const DELETE_LINK_SUCCESS = 'DELETE_LINK_SUCCESS'


export function fetchingAllLinks() {
  return {
    type: FETCHING_ALL_LINKS,
  }
}
export function fetchingAllLinksSuccess(data) {
  return {
    type: FETCHING_ALL_LINKS_SUCCESS,
    data,
  }
}
export function deleteLinkSuccess(data) {
  return {
    type: DELETE_LINK_SUCCESS,
    data,
  }
}


export function fetchDataAllLink() {
  return async (dispatch) => {
    dispatch(fetchingAllLinks())
    try {
      const res = await callAPI.fetch(endpoints.getAllLinkByRecipientId)
      dispatch(fetchingAllLinksSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function fetchDataAllLinkAgain() {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.getAllLinkByRecipientId)
      dispatch(fetchingAllLinksSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function deleteLink(token) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.deleteLink(token))
      await dispatch(deleteLinkSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
