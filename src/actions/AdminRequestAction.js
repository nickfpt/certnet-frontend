import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_REQUEST = 'FETCHING_REQUEST'
export const RE_FETCHING_REQUEST = 'RE_FETCHING_REQUEST'
export const FETCHING_REQUEST_SUCCESS = 'FETCHING_REQUEST_SUCCESS'
export const UPDATE_REQUEST_SUCCESS = 'UPDATE_REQUEST_SUCCESS'


export function fetchingRequest() {
  return {
    type: FETCHING_REQUEST,
  }
}
export function fetchingRequestSuccess(data) {
  return {
    type: FETCHING_REQUEST_SUCCESS,
    data,
  }
}
export function reFetchingRequest(data) {
  return {
    type: RE_FETCHING_REQUEST,
    data,
  }
}
export function updateRequestSuccess(data) {
  return {
    type: UPDATE_REQUEST_SUCCESS,
    data,
  }
}
export function getRequestToBeIssuer(flag) {
  return async (dispatch) => {
    dispatch(fetchingRequest())
    try {
      const res = await callAPI.fetch(endpoints.getRequestToBeIssuer(flag))
      dispatch(fetchingRequestSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function reGetRequestToBeIssuer(flag) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.getRequestToBeIssuer(flag))
      dispatch(fetchingRequestSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}

export function rejectRequest(reqId) {
  return async () => {
    try {
      await callAPI.fetch(endpoints.rejectRequest(reqId))
    } catch (err) {
      console.log(err)
    }
  }
}
export function approveRequest(reqId) {
  return async () => {
    try {
      await callAPI.fetch(endpoints.approveRequest(reqId))
    } catch (err) {
      console.log(err)
    }
  }
}
export function updateRequest(reqId, data) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.updateRequest(reqId), true, data)
      dispatch(updateRequestSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}

export function uploadLogo(data) {
  return async () => {
    try {
      await callAPI.fetch(endpoints.uploadLogo, true, data, true)
    } catch (err) {
      console.log(err)
    }
  }
}
