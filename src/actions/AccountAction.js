import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_CURRENT_USER = 'FETCHING_CURRENT_USER'
export const FETCHING_CURRENT_USER_SUCCESS = 'FETCHING_CURRENT_USER_SUCCESS'
export const CHANGE_PASS_SUCCESS = 'CHANGE_PASS_SUCCESS'
export const UPDATE_ETH_SUCCESS = 'UPDATE_ETH_SUCCESS'
export const REQUEST_TO_BE_ISSUER_SUCCESS = 'REQUEST_TO_BE_ISSUER_SUCCESS'


export function fetchingCurrentUser() {
  return {
    type: FETCHING_CURRENT_USER,
  }
}
export function fetchingCurrentUserSuccess(data) {
  return {
    type: FETCHING_CURRENT_USER_SUCCESS,
    data,
  }
}
export function changePassSuccess(data) {
  return {
    type: CHANGE_PASS_SUCCESS,
    data,
  }
}
export function updateEthSuccess(data) {
  return {
    type: UPDATE_ETH_SUCCESS,
    data,
  }
}
export function requestToBeIssuerSuccess(data) {
  return {
    type: REQUEST_TO_BE_ISSUER_SUCCESS,
    data,
  }
}

export function getCurrentUser() {
  return async (dispatch) => {
    dispatch(fetchingCurrentUser())
    try {
      const res = await callAPI.fetch(endpoints.getCurrentUser)
      dispatch(fetchingCurrentUserSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function changePassword(data) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.changePass, true, data)
      dispatch(changePassSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function updateEthRecipient(data) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.updateEthAddressRecipient, true, data)
      dispatch(updateEthSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function requestToBeIssuer(data) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.requestToBeIssuer, true, data)
      dispatch(requestToBeIssuerSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
