import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const FETCHING_ISSUER = 'FETCHING_ISSUER'
export const FETCHING_ISSUER_SUCCESS = 'FETCHING_ISSUER_SUCCESS'
export const UPDATE_ISSUER_SUCCESS = 'UPDATE_ISSUER_SUCCESS'

export function fetchingIssuer() {
  return {
    type: FETCHING_ISSUER,
  }
}
export function fetchingIssuerSuccess(data) {
  return {
    type: FETCHING_ISSUER_SUCCESS,
    data,
  }
}

export function getAllIssuers() {
  return async (dispatch) => {
    dispatch(fetchingIssuer())
    try {
      const res = await callAPI.fetch(endpoints.getAllIssuers)
      dispatch(fetchingIssuerSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function updateIssuerSuccess(data) {
  return {
    type: UPDATE_ISSUER_SUCCESS,
    data,
  }
}
export function reGetAllIssuers() {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.getAllIssuers)
      dispatch(fetchingIssuerSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function changeIssuerStatus(issuerId, value) {
  return async () => {
    try {
      await callAPI.fetch(endpoints.changeIssuerStatus(issuerId, !value))
    } catch (err) {
      console.log(err)
    }
  }
}

export function updateIssuer(issuerId, data) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.updateIssuer(issuerId), true, data)
      console.log(res)
      dispatch(updateIssuerSuccess(JSON.stringify(res)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function uploadLogo(data) {
  return async () => {
    try {
      await callAPI.fetch(endpoints.uploadLogo, true, data, true)
    } catch (err) {
      console.log(err)
    }
  }
}
