import * as callAPI from './../constants/CallAPI'
import * as endpoints from './../constants/endpoints'

export const INITIALIZE_GENERAL_INFOR = 'INITIALIZE_GENERAL_INFOR'
export const CONFIRM_DATA_BATCH = 'CONFIRM_DATA_BATCH'
export const CREATE_UNSIGNED_CERT_SUCCESS = 'CREATE_UNSIGNED_CERT_SUCCESS'
export const FETCHING_DATA_TEMPLATE = 'FETCHING_DATA_TEMPLATE'
export const FETCHING_DATA_TEMPLATE_SUCCESS = 'FETCHING_DATA_TEMPLATE_SUCCESS'
export const POSTING_CREATE_BATCH_SUCCESS = 'POSTING_CREATE_BATCH_SUCCESS'
export const SET_ACTIVE_STEP = 'SET_ACTIVE_STEP'
export const SAVE_VISUAL_FIELDS = 'SAVE_VISUAL_FIELDS'
export const RESET_STATE = 'RESET_STATE'

export function initializeInfor(data) {
  return {
    type: INITIALIZE_GENERAL_INFOR,
    data,
  }
}
export function saveVisualField(data) {
  return {
    type: SAVE_VISUAL_FIELDS,
    data,
  }
}
export function setActiveStep(step) {
  return {
    type: SET_ACTIVE_STEP,
    step,
  }
}

export function getData() {
  return {
    type: FETCHING_DATA_TEMPLATE,
  }
}
export function getDataSuccess(data) {
  return {
    type: FETCHING_DATA_TEMPLATE_SUCCESS,
    data,
  }
}
export function postCreateBatchSuccess(data) {
  return {
    type: POSTING_CREATE_BATCH_SUCCESS,
    data,
  }
}
export function createUnsignedCertSuccess(data) {
  return {
    type: CREATE_UNSIGNED_CERT_SUCCESS,
    data,
  }
}
export function resetState() {
  return {
    type: RESET_STATE,
  }
}
export function fetchData() {
  return async (dispatch) => {
    dispatch(getData())
    try {
      const response = await callAPI.fetch(endpoints.getAllTemplateById)
      dispatch(getDataSuccess(JSON.stringify(response.templates)))
    } catch (err) {
      console.log(err)
    }
  }
}
export function createNewBatch(batchInfo) {
  return async (dispatch) => {
    try {
      const response = await callAPI.fetch(endpoints.createNewBatch, true, {
        batch: {
          title: batchInfo.title,
          tags: batchInfo.tags,
          description: batchInfo.description,
          header: batchInfo.header,
        },
      })
      dispatch(postCreateBatchSuccess(JSON.stringify(response)))
    } catch (err) {
      console.log(err)
    }
  }
}

export function createUnsignedCert(batchInfo, updatedData) {
  return async (dispatch) => {
    try {
      const res = await callAPI.fetch(endpoints.createUnsignedCert, true, {
        batchId: batchInfo.batchId,
        templateId: batchInfo.template.id,
        header: batchInfo.header,
        data: updatedData,
      })
      dispatch(createUnsignedCertSuccess(JSON.stringify(res)))
      return res
    } catch (err) {
      console.log(err)
      return []
    }
  }
}

export function signBatch(signature, flag) {
  return async () => {
    try {
      let res = {}
      console.log('signature: ', signature)
      console.log('flag: ', flag)

      if (flag) {
        res = await callAPI.fetch(endpoints.signBatch, true, {
          batchId: signature.batchId,
          transactionId: signature.transactionId,
          sender: signature.sender,
          map: signature.map,
          visualTemplateId: signature.visualTemplateId
        })
      } else {
        res = await callAPI.fetch(endpoints.signBatch, true, {
          batchId: signature.batchId,
          transactionId: signature.transactionId,
          sender: signature.sender
        })
      }

      return res
    } catch (err) {
      console.log(err)
      return []
    }
  }
}
