import { web3 } from '../services/web3'

const address = '0xee2fe69fd01cef5ff57eeef1f4ef0d3301bec530'
const ABI = [
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "name": "issuers",
    "outputs": [
      {
        "name": "issuerAddress",
        "type": "address"
      },
      {
        "name": "email",
        "type": "string"
      },
      {
        "name": "createdTimestamp",
        "type": "uint256"
      },
      {
        "name": "locked",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "owner",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "bytes32"
      }
    ],
    "name": "revokedCertificates",
    "outputs": [
      {
        "name": "certificateHash",
        "type": "bytes32"
      },
      {
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "name": "revokedTimestamp",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "bytes32"
      }
    ],
    "name": "issuedBatches",
    "outputs": [
      {
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "name": "createdTimestamp",
        "type": "uint256"
      },
      {
        "name": "issuerAddress",
        "type": "address"
      },
      {
        "name": "revoked",
        "type": "bool"
      },
      {
        "name": "revokedTimestamp",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "issuerAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "createdTimestamp",
        "type": "uint256"
      }
    ],
    "name": "IssuerAdded",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "issuerAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "updatedTimestamp",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "locked",
        "type": "bool"
      }
    ],
    "name": "IssuerLockStatusUpdated",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "indexed": false,
        "name": "createdTimestamp",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "CertificateBatchIssued",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "indexed": false,
        "name": "revokedTimestamp",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "CertificateBatchRevoked",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "certificateHash",
        "type": "bytes32"
      },
      {
        "indexed": false,
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "indexed": false,
        "name": "revokedTimestamp",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "CertificateRevoked",
    "type": "event"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "issuerAddress",
        "type": "address"
      },
      {
        "name": "email",
        "type": "string"
      }
    ],
    "name": "addIssuer",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "removeIssuer",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "lockIssuer",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "issuerAddress",
        "type": "address"
      }
    ],
    "name": "unlockIssuer",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "merkleRootHash",
        "type": "bytes32"
      }
    ],
    "name": "issueBatch",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "merkleRootHash",
        "type": "bytes32"
      }
    ],
    "name": "revokeBatch",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "certificateHash",
        "type": "bytes32"
      },
      {
        "name": "merkleRootHash",
        "type": "bytes32"
      },
      {
        "name": "proof",
        "type": "bytes"
      }
    ],
    "name": "revokeSingleCertificate",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [],
    "name": "destroy",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  }
]

export default new web3.eth.Contract(ABI, address)
