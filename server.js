const express = require('express')

const app = express()

app.use('/static', express.static(`${__dirname}/build`))
app.use('/staticCSS', express.static(`${__dirname}/src`))


app.set('views', `${__dirname}/views`)
app.set('view engine', 'pug')

app.get('*', (req, res) => {
  res.render('index')
})

app.listen(3000, () => {
  console.log('listening to this joint on port 3000')
})
