FROM node:8.11.2

ENV PORT 3000
ENV JWT_SECRET lovethewayyoulie
EXPOSE 3000

WORKDIR /opt/app

ADD package.json yarn.lock /tmp/

# Install packages
RUN cd /tmp && yarn && yarn cache clean
RUN mkdir -p /opt/app && cd /opt/app && ln -s /tmp/node_modules

ADD . /opt/app
RUN npm run build

CMD [ "npm", "start" ]
